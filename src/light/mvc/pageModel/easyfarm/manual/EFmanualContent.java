package light.mvc.pageModel.easyfarm.manual;

import light.mvc.model.base.IdEntity;
import light.mvc.model.easyfarm.manual.TEFmanualContent;

public class EFmanualContent extends IdEntity implements java.io.Serializable{
	private Long id;
	private Long cid;
	private String content;
	private String title;
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCid() {
		return cid;
	}
	public void setCid(Long cid) {
		this.cid = cid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	
	
}
