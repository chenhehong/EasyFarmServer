package light.mvc.pageModel.easyfarm;

import java.util.Date;

public class EFResource implements java.io.Serializable {

	private Long id;
	private String description; 
	private String address;
	private Date creatTime;
	private String type;
	private String filepath;
	private String thumbFilepath;
	private Long dataId;
	private String dataBusinessMatter; //下乡事由

	public String getThumbFilepath() {
		return thumbFilepath;
	}

	public void setThumbFilepath(String thumbFilepath) {
		this.thumbFilepath = thumbFilepath;
	}

	public EFResource() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public Long getDataId() {
		return dataId;
	}

	public void setDataId(Long dataId) {
		this.dataId = dataId;
	}

	public String getDataBusinessMatter() {
		return dataBusinessMatter;
	}

	public void setDataBusinessMatter(String dataBusinessMatter) {
		this.dataBusinessMatter = dataBusinessMatter;
	}


}