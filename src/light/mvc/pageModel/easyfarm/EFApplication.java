package light.mvc.pageModel.easyfarm;

import java.util.Date;

public class EFApplication implements java.io.Serializable {

	private Long id;
	private String applyMan; 
	private Date applyDate;
	private String businessPersonnel;
	private String businessPhone;
	private String businessArea;
	private String businessAddress;
	private Date businessDate;
	private String businessMatter;
	private Date returnDate;
	private String businessLinkMan;
	private String businessLinkPhone;
	private int departmentCheck;
	private int finalCheck;
	private String departmentOpinion;
	private String finalOpinion;
	private int serverDates;

	private Long memberId;
	private String memberLoginName;

	public EFApplication() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApplyMan() {
		return applyMan;
	}

	public void setApplyMan(String applyMan) {
		this.applyMan = applyMan;
	}

	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public String getBusinessPersonnel() {
		return businessPersonnel;
	}

	public void setBusinessPersonnel(String businessPersonnel) {
		this.businessPersonnel = businessPersonnel;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getBusinessArea() {
		return businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public String getBusinessMatter() {
		return businessMatter;
	}

	public void setBusinessMatter(String businessMatter) {
		this.businessMatter = businessMatter;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getBusinessLinkMan() {
		return businessLinkMan;
	}

	public void setBusinessLinkMan(String businessLinkMan) {
		this.businessLinkMan = businessLinkMan;
	}

	public String getBusinessLinkPhone() {
		return businessLinkPhone;
	}

	public void setBusinessLinkPhone(String businessLinkPhone) {
		this.businessLinkPhone = businessLinkPhone;
	}

	public int getDepartmentCheck() {
		return departmentCheck;
	}

	public void setDepartmentCheck(int departmentCheck) {
		this.departmentCheck = departmentCheck;
	}

	public int getFinalCheck() {
		return finalCheck;
	}

	public void setFinalCheck(int finalCheck) {
		this.finalCheck = finalCheck;
	}
	

	public String getDepartmentOpinion() {
		return departmentOpinion;
	}

	public void setDepartmentOpinion(String departmentOpinion) {
		this.departmentOpinion = departmentOpinion;
	}

	public String getFinalOpinion() {
		return finalOpinion;
	}

	public void setFinalOpinion(String finalOpinion) {
		this.finalOpinion = finalOpinion;
	}

	public int getServerDates() {
		return serverDates;
	}

	public void setServerDates(int serverDates) {
		this.serverDates = serverDates;
	}

	public Long getMemberId() {
		return memberId;
	}

	public void setMemberId(Long id) {
		this.memberId = id;
	}

	public String getMemberLoginName() {
		return memberLoginName;
	}

	public void setMemberLoginName(String memberLoginName) {
		this.memberLoginName = memberLoginName;
	}

}