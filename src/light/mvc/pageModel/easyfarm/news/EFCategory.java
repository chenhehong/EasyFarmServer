package light.mvc.pageModel.easyfarm.news;
import java.util.Date;


public class EFCategory  implements java.io.Serializable{
	
	private Long id;

	private String categoryName;
	private String categoryDesc;
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryDesc() {
		return categoryDesc;
	}
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

}
