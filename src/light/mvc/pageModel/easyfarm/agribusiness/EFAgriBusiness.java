package light.mvc.pageModel.easyfarm.agribusiness;

import java.util.Date;

public class EFAgriBusiness implements java.io.Serializable{
	private Long id;
	private Date createTime;
	private Long autherId;
	private String autherType;
	private String agribusinessSite;
	private String agribusinessIntroduce;
	private int status;
	private String companyName;
	private String address;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getAutherId() {
		return autherId;
	}
	public void setAutherId(Long autherId) {
		this.autherId = autherId;
	}
	public String getAutherType() {
		return autherType;
	}
	public void setAutherType(String autherType) {
		this.autherType = autherType;
	}
	public String getAgribusinessSite() {
		return agribusinessSite;
	}
	public void setAgribusinessSite(String agribusinessSite) {
		this.agribusinessSite = agribusinessSite;
	}
	public String getAgribusinessIntroduce() {
		return agribusinessIntroduce;
	}
	public void setAgribusinessIntroduce(String agribusinessIntroduce) {
		this.agribusinessIntroduce = agribusinessIntroduce;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	

}
