package light.mvc.service.easyfarm;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.EFResource;

public interface EFResourceServiceI {

	public List<EFResource> dataGrid(EFResource resource,PageFilter ph);

	public Long count(EFResource resource, PageFilter ph);

	public void delete(Long id,HttpServletRequest request);

	public EFResource get(Long id);
	
}
