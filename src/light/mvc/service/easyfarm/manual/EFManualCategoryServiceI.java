package light.mvc.service.easyfarm.manual;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.model.easyfarm.manual.TEFmanualCategory;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.manual.EFmanualCategory;

public interface EFManualCategoryServiceI {
	public void add(EFmanualCategory category);
	
	public List<EFmanualCategory> dataGrid(EFmanualCategory category,PageFilter ph);
	
	public List<TEFmanualCategory> getAllData();
	
	public Long count(EFmanualCategory category,PageFilter ph);
		
	public void delete(Long id,HttpServletRequest request);

	public EFmanualCategory get(Long id);	
	
	public void edit(EFmanualCategory data);
}
