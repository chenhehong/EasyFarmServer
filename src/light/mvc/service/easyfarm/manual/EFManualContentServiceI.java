package light.mvc.service.easyfarm.manual;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.model.easyfarm.manual.TEFmanualContent;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.manual.EFmanualContent;

public interface EFManualContentServiceI {
	public void add(EFmanualContent category);
	
	public List<EFmanualContent> dataGrid(EFmanualContent category,PageFilter ph);
	
	public List<TEFmanualContent> getAllData();
	
	public Long count(EFmanualContent category,PageFilter ph);
		
	public void delete(Long id,HttpServletRequest request);

	public EFmanualContent get(Long id);	
	
	public void edit(EFmanualContent data);
}
