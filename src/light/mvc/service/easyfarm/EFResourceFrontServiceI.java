package light.mvc.service.easyfarm;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.pageModel.easyfarm.EFResource;

public interface EFResourceFrontServiceI {

	public List<EFResource> dataGrid(SessionInfo sessionInfo,EFResource resource,PageFilter ph);

	public Long count(SessionInfo sessionInfo,EFResource resource, PageFilter ph);

	public void delete(Long id,HttpServletRequest request);

	public void add(EFResource resource);

	public EFResource get(Long id);
	
}
