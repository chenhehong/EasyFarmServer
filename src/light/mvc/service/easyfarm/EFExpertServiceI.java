package light.mvc.service.easyfarm;

import java.util.List;

import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFExpert;
import light.mvc.pageModel.sys.User;

public interface EFExpertServiceI {

	public List<EFExpert> dataGrid(EFExpert expert,PageFilter ph);

	public Long count(EFExpert expert, PageFilter ph);

	public void add(EFExpert expert);

	public void delete(Long id);

	public void pass(Long id);

	public void edit(EFExpert expert);

	public EFExpert get(Long id);
	
	public EFExpert getByLoginName(EFExpert expert);

	public EFExpert login(EFExpert expert);
	
	public boolean editUserPwd(SessionInfo sessionInfo, String oldPwd, String pwd);
}
