package light.mvc.service.easyfarm.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jdk.nashorn.internal.ir.ContinueNode;
import light.mvc.dao.BaseDaoI;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.model.easyfarm.TEFapplication;
import light.mvc.model.easyfarm.TEFexpert;
import light.mvc.model.sys.Torganization;
import light.mvc.model.sys.Tresource;
import light.mvc.model.sys.Trole;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.pageModel.sys.User;
import light.mvc.service.base.ServiceException;
import light.mvc.service.easyfarm.EFDataDepartmentServiceI;
import light.mvc.service.sys.UserServiceI;
import light.mvc.utils.MD5Util;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EFDataDepartmentServiceImpl implements EFDataDepartmentServiceI {

	@Autowired
	private BaseDaoI<TEFapplication> dataDao;
	
	@Autowired
	private BaseDaoI<TEFexpert> memberDao;

	@Override
	public void delete(Long id) {
		TEFapplication t = dataDao.get(TEFapplication.class, id);
		dataDao.delete(t);
	}

	@Override
	public void edit(EFApplication data) {
		TEFapplication t = dataDao.get(TEFapplication.class,data.getId());
		t.setDepartmentCheck(data.getDepartmentCheck());
		t.setDepartmentOpinion(data.getDepartmentOpinion());
		dataDao.update(t);
	}

	
	@Override
	public EFApplication get(Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TEFapplication t = dataDao.get("from TEFapplication t  where t.id = :id", params);
		EFApplication u = new EFApplication();
		BeanUtils.copyProperties(t, u);
		
		if(t.getMember()!=null){
			u.setMemberId(t.getMember().getId());;
			u.setMemberLoginName(t.getMember().getLoginName());;
		}
		
		return u;
	}

	@Override
	public List<EFApplication> dataGrid(EFApplication data, PageFilter ph) {
		List<EFApplication> ul = new ArrayList<EFApplication>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFapplication t ";
		List<TEFapplication> l = dataDao.find(hql + whereHql(data, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFapplication t : l) {
			EFApplication u = new EFApplication();
			BeanUtils.copyProperties(t, u);
			if (t.getMember() != null) {
				u.setMemberId(t.getMember().getId());
				u.setMemberLoginName(t.getMember().getLoginName());
			}
			ul.add(u);
		}
		return ul;
	}

	@Override
	public Long count(EFApplication data, PageFilter ph) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFapplication t ";
		return dataDao.count("select count(*) " + hql + whereHql(data, params), params);
	}

	private String whereHql(EFApplication data, Map<String, Object> params) {
		String hql = "";
		if (data != null) {
			hql += " where 1=1 ";
			if (data.getApplyMan() != null) {
				hql += " and t.applyMan like :applyMan";
				params.put("applyMan", "%%" + data.getApplyMan() + "%%");
			}
			hql += " and t.departmentCheck ="+data.getDepartmentCheck();
		}
		return hql;
	}
	
	@Override
	public List<EFApplication> manageDataGrid(EFApplication data, PageFilter ph) {
		List<EFApplication> ul = new ArrayList<EFApplication>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFapplication t ";
		List<TEFapplication> l = dataDao.find(hql + manageWhereHql(data, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFapplication t : l) {
			EFApplication u = new EFApplication();
			BeanUtils.copyProperties(t, u);
			if (t.getMember() != null) {
				u.setMemberId(t.getMember().getId());
				u.setMemberLoginName(t.getMember().getLoginName());
			}
			ul.add(u);
		}
		return ul;
	}

	@Override
	public Long manageCount(EFApplication data, PageFilter ph) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFapplication t ";
		return dataDao.count("select count(*) " + hql + manageWhereHql(data, params), params);
	}

//	获取申请状态为通过与不通过的数据
	private String manageWhereHql(EFApplication data, Map<String, Object> params) {
		String hql = "";
		if (data != null) {
			hql += " where 1=1 ";
			if (data.getApplyMan() != null) {
				hql += " and t.applyMan like :applyMan";
				params.put("applyMan", "%%" + data.getApplyMan() + "%%");
			}
			hql += " and (t.departmentCheck = 1 OR t.departmentCheck=-1)";
		}
		return hql;
	}

	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}

	@Override
	public void pass(Long id) {
		TEFapplication t = dataDao.get(TEFapplication.class,id);
		t.setDepartmentCheck(1);
		dataDao.update(t);
	}

}
