package light.mvc.service.easyfarm.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import light.mvc.dao.BaseDaoI;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.model.easyfarm.TEFapplication;
import light.mvc.model.easyfarm.TEFexpert;
import light.mvc.model.easyfarm.TEFresource;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFResource;
import light.mvc.service.easyfarm.EFResourceFrontServiceI;
import light.mvc.service.easyfarm.EFResourceServiceI;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EFResourceFrontServiceImpl implements EFResourceFrontServiceI {

	@Autowired
	private BaseDaoI<TEFresource> resourceDao;
	
	@Autowired
	private BaseDaoI<TEFapplication> dataDao;

	@Override
	public void delete(Long id,HttpServletRequest request) {
		TEFresource t = resourceDao.get(TEFresource.class, id);
		resourceDao.delete(t);
		resourceDao.deleteFile(t.getFilepath(),request);
//		如果资源是图片，删除缩略图
		resourceDao.deleteFile(t.getThumbFilepath(),request);
	}
	
	@Override
	public EFResource get(Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TEFresource t = resourceDao.get("from Txxresource t  where t.id = :id", params);
		EFResource u = new EFResource();
		BeanUtils.copyProperties(t, u);
		if(t.getData()!=null){
			u.setDataId(t.getData().getId());
			u.setDataBusinessMatter(t.getData().getBusinessMatter());
		}
		return u;
	}

	@Override
	public List<EFResource> dataGrid(SessionInfo sessionInfo,EFResource resource, PageFilter ph) {
		List<EFResource> ul = new ArrayList<EFResource>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Txxresource t ";
		List<TEFresource> l = resourceDao.find(hql + whereHql(sessionInfo,resource, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFresource t : l) {
			EFResource u = new EFResource();
			BeanUtils.copyProperties(t, u);
			if(t.getData()!=null){
				u.setDataId(t.getData().getId());
				u.setDataBusinessMatter(t.getData().getBusinessMatter());
			}
			ul.add(u);
		}
		return ul;
	}

	@Override
	public Long count(SessionInfo sessionInfo,EFResource resource, PageFilter ph) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Txxresource t ";
		return resourceDao.count("select count(*) " + hql + whereHql(sessionInfo,resource, params), params);
	}

	private String whereHql(SessionInfo sessionInfo,EFResource resource, Map<String, Object> params) {
		long memberId = sessionInfo.getId();
		String hql = "";
//		联表查询
//		hql +=" join fetch t.data data";
		hql += " where 1=1 ";
		hql += " and t.data.member.id = "+memberId;
		if (resource != null) {
			if (resource.getDataId() != null&&resource.getDataId()!=0) {
				hql += " and t.data.id = "+resource.getDataId();
			}
		}
		return hql;
	}

	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}

	@Override
	public void add(EFResource resource) {
		// TODO Auto-generated method stub
		TEFresource r = new TEFresource();
		r.setData(dataDao.get(TEFapplication.class, resource.getDataId()));
		r.setDescription(resource.getDescription());
		r.setAddress(resource.getAddress());
		r.setFilepath(resource.getFilepath());
		r.setThumbFilepath(resource.getThumbFilepath());
		r.setCreatTime(new Date());
		r.setType(resource.getType());
		resourceDao.save(r);
	}

}
