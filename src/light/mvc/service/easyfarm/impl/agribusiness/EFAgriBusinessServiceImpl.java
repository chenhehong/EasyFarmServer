package light.mvc.service.easyfarm.impl.agribusiness;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import light.mvc.dao.BaseDaoI;
import light.mvc.model.easyfarm.agribusiness.TEFAgriBusiness;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.agribusiness.EFAgriBusiness;
import light.mvc.service.easyfarm.agribusiness.EFAgriBusinessServiceI;

@Service
public class EFAgriBusinessServiceImpl implements EFAgriBusinessServiceI {

	@Autowired
	private BaseDaoI<TEFAgriBusiness> agriBDao;
	
	@Override
	public void add(EFAgriBusiness m) {
		// TODO Auto-generated method stub
		TEFAgriBusiness t = new TEFAgriBusiness();
		BeanUtils.copyProperties(m,t);
		agriBDao.save(t);
	}

	@Override
	public List<EFAgriBusiness> dataGrid(EFAgriBusiness agriB, PageFilter ph) {
		// TODO Auto-generated method stub
		
		List<EFAgriBusiness> ul = new ArrayList<EFAgriBusiness>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFAgriBusiness t ";
		List<TEFAgriBusiness> l = agriBDao.find(hql + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFAgriBusiness t : l) {
			EFAgriBusiness u = new EFAgriBusiness();
			BeanUtils.copyProperties(t, u);
			ul.add(u);
		}
		return ul;
	}

	@Override
	public Long count(EFAgriBusiness agriB, PageFilter ph) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFAgriBusiness t ";
		return agriBDao.count("select count(*) " + hql + whereHql(agriB, params), params);
	}

	@Override
	public void delete(Long id, HttpServletRequest request) {
		// TODO Auto-generated method stub
		TEFAgriBusiness t= agriBDao.get(TEFAgriBusiness.class,id);
		agriBDao.delete(t);
		
	}

	@Override
	public EFAgriBusiness get(Long id) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TEFAgriBusiness t = agriBDao.get("from TEFAgriBusiness t  where t.id = :id", params);
		EFAgriBusiness u = new EFAgriBusiness();
		BeanUtils.copyProperties(t, u);
		return u;
	}

	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	
	private String whereHql(EFAgriBusiness agriB, Map<String, Object> params) {//待修改
		String hql = "";
		if (agriB != null) {
			hql += " where 1=1 ";
		}

		return hql;
	}

	@Override
	public void edit(EFAgriBusiness data) {
		// TODO Auto-generated method stub
		TEFAgriBusiness t=agriBDao.get(TEFAgriBusiness.class,data.getId());
		agriBDao.update(t);
	}
		

}
