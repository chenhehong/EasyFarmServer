package light.mvc.service.easyfarm.impl.manual;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import light.mvc.dao.BaseDaoI;
import light.mvc.model.easyfarm.manual.TEFmanualCategory;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.manual.EFmanualCategory;
import light.mvc.service.easyfarm.manual.EFManualCategoryServiceI;

@Service
public class EFManualCategoryServiceImpl implements EFManualCategoryServiceI {

	@Autowired
	private BaseDaoI<TEFmanualCategory> categoryDao;
	
	@Override
	public void add(EFmanualCategory category) {
		// TODO Auto-generated method stub
		TEFmanualCategory t = new TEFmanualCategory();
		BeanUtils.copyProperties(category,t);
		categoryDao.save(t);

	}

	@Override
	public List<EFmanualCategory> dataGrid(EFmanualCategory category,
			PageFilter ph) {
		// TODO Auto-generated method stub
		List<EFmanualCategory> ul = new ArrayList<EFmanualCategory>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFmanualCategory t ";
		List<TEFmanualCategory> l = categoryDao.find(hql + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFmanualCategory t : l) {
			EFmanualCategory u = new EFmanualCategory();
			BeanUtils.copyProperties(t, u);
			ul.add(u);
		}
		return ul;
	}

	@Override
	public List<TEFmanualCategory> getAllData() {
		// TODO Auto-generated method stub
		
		String hql = " from TEFmanualCategory t ";
		List<TEFmanualCategory> l = categoryDao.find(hql);
		return l;
	}

	@Override
	public Long count(EFmanualCategory category, PageFilter ph) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFmanualCategory t ";
		return categoryDao.count("select count(*) " + hql + whereHql(category, params), params);
	}

	@Override
	public void delete(Long id, HttpServletRequest request) {
		// TODO Auto-generated method stub
		TEFmanualCategory t= categoryDao.get(TEFmanualCategory.class,id);
		categoryDao.delete(t);
	}

	@Override
	public EFmanualCategory get(Long id) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TEFmanualCategory t = categoryDao.get("from TEFmanualCategory t  where t.id = :id", params);
		EFmanualCategory u = new EFmanualCategory();
		BeanUtils.copyProperties(t, u);
		return u;
	}

	@Override
	public void edit(EFmanualCategory data) {
		// TODO Auto-generated method stub
		TEFmanualCategory t=categoryDao.get(TEFmanualCategory.class, data.getId());
		categoryDao.update(t);
	}

	
	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	
	private String whereHql(EFmanualCategory category, Map<String, Object> params) {//待修改
		String hql = "";
		if (category != null) {
			hql += " where 1=1 ";
		}

		return hql;
	}
	
	
	
}
