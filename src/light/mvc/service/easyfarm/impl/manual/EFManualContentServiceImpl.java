package light.mvc.service.easyfarm.impl.manual;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import light.mvc.dao.BaseDaoI;
import light.mvc.model.easyfarm.manual.TEFmanualContent;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.manual.EFmanualContent;
import light.mvc.service.easyfarm.manual.EFManualContentServiceI;


@Service
public class EFManualContentServiceImpl implements EFManualContentServiceI {

	@Autowired
	private BaseDaoI<TEFmanualContent> manualDao;
	
	@Override
	public void add(EFmanualContent m) {
		// TODO Auto-generated method stub
		TEFmanualContent t = new TEFmanualContent();
		BeanUtils.copyProperties(m,t);
		manualDao.save(t);
	}

	@Override
	public List<EFmanualContent> dataGrid(EFmanualContent news, PageFilter ph) {
		// TODO Auto-generated method stub
		
		List<EFmanualContent> ul = new ArrayList<EFmanualContent>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFmanualContent t ";
		List<TEFmanualContent> l = manualDao.find(hql + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFmanualContent t : l) {
			EFmanualContent u = new EFmanualContent();
			BeanUtils.copyProperties(t, u);
			ul.add(u);
		}
		return ul;
	}

	@Override
	public Long count(EFmanualContent news, PageFilter ph) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFmanualContent t ";
		return manualDao.count("select count(*) " + hql + whereHql(news, params), params);
	}

	@Override
	public void delete(Long id, HttpServletRequest request) {
		// TODO Auto-generated method stub
		TEFmanualContent t= manualDao.get(TEFmanualContent.class,id);
		manualDao.delete(t);
		
	}

	@Override
	public EFmanualContent get(Long id) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TEFmanualContent t = manualDao.get("from TEFmanualContent t  where t.id = :id", params);
		EFmanualContent u = new EFmanualContent();
		BeanUtils.copyProperties(t, u);
		return u;
	}

	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	
	private String whereHql(EFmanualContent news, Map<String, Object> params) {//待修改
		String hql = "";
		if (news != null) {
			hql += " where 1=1 ";
		}

		return hql;
	}

	@Override
	public void edit(EFmanualContent data) {
		// TODO Auto-generated method stub
		TEFmanualContent t=manualDao.get(TEFmanualContent.class,data.getId());
		manualDao.update(t);
	}

	@Override
	public List<TEFmanualContent> getAllData() {
		// TODO Auto-generated method stub
		String hql = " from TEFmanualContent t ";
		List<TEFmanualContent> l=manualDao.find(hql);
		return l;
	}
		
}
