package light.mvc.service.easyfarm.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import light.mvc.dao.BaseDaoI;
import light.mvc.model.easyfarm.TEFexpert;
import light.mvc.model.sys.Tresource;
import light.mvc.model.sys.Trole;
import light.mvc.model.sys.Tuser;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.base.Tree;
import light.mvc.pageModel.easyfarm.EFExpert;
import light.mvc.pageModel.sys.Role;
import light.mvc.pageModel.sys.User;
import light.mvc.service.easyfarm.EFExpertServiceI;
import light.mvc.utils.MD5Util;

@Service
public class EFExpertServiceImpl implements EFExpertServiceI{

	@Autowired
	private BaseDaoI<TEFexpert> expertDao;

	@Override
	public void add(EFExpert m) {
		TEFexpert t = new TEFexpert();
		BeanUtils.copyProperties(m,t);
		expertDao.save(t);
	}

	@Override
	public void delete(Long id) {
		TEFexpert t = expertDao.get(TEFexpert.class, id);
		expertDao.delete(t);
	}

	@Override
	public void edit(EFExpert m) {
		TEFexpert t = expertDao.get(TEFexpert.class, m.getId());
		BeanUtils.copyProperties(m,t);
		expertDao.update(t);
	}

	@Override
	public EFExpert get(Long id) {
		TEFexpert t = expertDao.get(TEFexpert.class, id);
		EFExpert m = new EFExpert();
		BeanUtils.copyProperties(t,m);
		return m;
	}

	@Override
	public List<EFExpert> dataGrid(EFExpert expert, PageFilter ph) {
		List<EFExpert> ul = new ArrayList<EFExpert>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFexpert t ";
		List<TEFexpert> l = expertDao.find(hql + whereHql(expert, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFexpert t : l) {
			EFExpert u = new EFExpert();
			BeanUtils.copyProperties(t, u);
			ul.add(u);
		}
		return ul;
	}

	@Override
	public Long count(EFExpert expert, PageFilter ph) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFexpert t ";
		return expertDao.count("select count(*) " + hql + whereHql(expert, params), params);
	}

	private String whereHql(EFExpert expert, Map<String, Object> params) {
		String hql = "";
		if (expert != null) {
			hql += " where 1=1 ";
//			搜索loginName或realName时用模糊查询
			if (expert.getLoginName() != null) {
				hql += " and t.loginName like :loginName";
				params.put("loginName", "%%" + expert.getLoginName() + "%%");
			}
			if (expert.getRealName() != null) {
				hql += " and t.realName like :realName";
				params.put("realName", "%%" + expert.getRealName() + "%%");
			}
			hql += " and t.status = :status";
			params.put("status", expert.getStatus());
		}
		return hql;
	}

	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}

	@Override
	public EFExpert getByLoginName(EFExpert expert) {
		TEFexpert t = expertDao.get("from TEFexpert t  where t.loginName = '"+expert.getLoginName()+"'");
		EFExpert u = new EFExpert();
		if(t!=null){
			BeanUtils.copyProperties(t, u);
		}else{
			return null;
		}
		return u;
	}

	@Override
	public void pass(Long id) {
		TEFexpert t = expertDao.get(TEFexpert.class, id);
		t.setStatus(1);
		expertDao.update(t);
	}

	@Override
	public EFExpert login(EFExpert expert) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("loginName", expert.getLoginName());
		params.put("password", MD5Util.md5(expert.getPassword()));
		TEFexpert t = expertDao.get("from TEFexpert t where t.loginName = :loginName and t.password = :password and t.status = 1", params);
		if (t != null) {
			EFExpert u = new EFExpert();
//			对象拷贝
			BeanUtils.copyProperties(t, u);
			return u;
		}
		return null;
	}

	@Override
	public boolean editUserPwd(SessionInfo sessionInfo, String oldPwd,
			String pwd) {
		TEFexpert u = expertDao.get(TEFexpert.class, sessionInfo.getId());
		if (u.getPassword().equalsIgnoreCase(MD5Util.md5(oldPwd))) {// 说明原密码输入正确
			u.setPassword(MD5Util.md5(pwd));
			return true;
		}
		return false;
	}

}
