package light.mvc.service.easyfarm.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import light.mvc.dao.BaseDaoI;
import light.mvc.model.easyfarm.TEFapplication;
import light.mvc.model.easyfarm.TEFexpert;
import light.mvc.model.easyfarm.TEFresource;
import light.mvc.model.sys.Torganization;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.pageModel.easyfarm.EFResource;
import light.mvc.service.easyfarm.EFDataFrontServiceI;
import light.mvc.service.easyfarm.EFResourceFrontServiceI;

import org.omg.PortableServer.Servant;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EFDataFrontServiceImpl implements EFDataFrontServiceI {

	@Autowired
	private BaseDaoI<TEFapplication> dataDao;
	
	@Autowired
	private BaseDaoI<TEFexpert> memberDao;
	
	@Autowired
	private BaseDaoI<TEFresource> resourceDao;

	@Override
	public void delete(Long id,HttpServletRequest request) {
		TEFapplication t = dataDao.get(TEFapplication.class, id);
		if ((t.getResources() != null) && (t.getResources().size() > 0)) {
			for (TEFresource r : t.getResources()) {
				resourceDao.delete(r);
				resourceDao.deleteFile(r.getFilepath(), request);
				resourceDao.deleteFile(r.getThumbFilepath(), request);
			}
		}
		dataDao.delete(t);
	}

	@Override
	public void edit(EFApplication data) {
		TEFapplication t = dataDao.get(TEFapplication.class,data.getId());
		t.setBusinessDate(data.getBusinessDate());
		t.setReturnDate(data.getReturnDate());
		t.setDepartmentCheck(data.getDepartmentCheck());
		dataDao.update(t);
	}

	@Override
	public List<EFApplication> dataGrid(EFApplication data, PageFilter ph) {
		List<EFApplication> ul = new ArrayList<EFApplication>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFapplication t ";
		List<TEFapplication> l = dataDao.find(hql + whereHql(data, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFapplication t : l) {
			EFApplication u = new EFApplication();
			BeanUtils.copyProperties(t, u);
			if (t.getMember() != null) {
				u.setMemberId(t.getMember().getId());
				u.setMemberLoginName(t.getMember().getLoginName());
			}
			ul.add(u);
		}
		return ul;
	}
	
//	可以上传资源的申请数据
	@Override
	public List<EFApplication> dataGridEnableUploadRes(EFApplication data, PageFilter ph) {
		List<EFApplication> ul = new ArrayList<EFApplication>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFapplication t ";
		List<TEFapplication> l = dataDao.find(hql + whereHqlEnableUploadRes(data, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFapplication t : l) {
			EFApplication u = new EFApplication();
			BeanUtils.copyProperties(t, u);
			if (t.getMember() != null) {
				u.setMemberId(t.getMember().getId());
				u.setMemberLoginName(t.getMember().getLoginName());
			}
			ul.add(u);
		}
		return ul;
	}

	//serverDates大于0的申请
	@Override
	public List<EFApplication> dataGridHasRes(EFApplication data, PageFilter ph) {
		List<EFApplication> ul = new ArrayList<EFApplication>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFapplication t ";
		List<TEFapplication> l = dataDao.find(hql + whereHqlHasRes(data, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFapplication t : l) {
			EFApplication u = new EFApplication();
			BeanUtils.copyProperties(t, u);
			if (t.getMember() != null) {
				u.setMemberId(t.getMember().getId());
				u.setMemberLoginName(t.getMember().getLoginName());
			}
			ul.add(u);
		}
		return ul;
	}

	@Override
	public Long count(EFApplication data, PageFilter ph) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFapplication t ";
		return dataDao.count("select count(*) " + hql + whereHql(data, params), params);
	}

	private String whereHql(EFApplication data, Map<String, Object> params) {
		String hql = "";
		hql += " where 1=1 ";
		if (data != null) {
			hql += " and t.member.id = "+data.getMemberId();
			if (data.getApplyMan() != null) {
				hql += " and t.applyMan like :applyMan";
				params.put("applyMan", "%%" + data.getApplyMan() + "%%");
			}
		}
		return hql;
	}
	
	private String whereHqlEnableUploadRes(EFApplication data, Map<String, Object> params) {
		String hql = "";
		hql += " where 1=1 ";
		if (data != null) {
			hql += " and t.member.id = "+data.getMemberId();
		}
		hql += " and t.departmentCheck=1 ";
		hql += " and t.finalCheck=1 ";
		Date today = new Date();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String todayString = sdf.format(today);
		try {
			Date todayFormat = sdf.parse(todayString);
			hql += " and t.businessDate<=:todayFormat";
			hql += " and t.returnDate>=:todayFormat";
			params.put("todayFormat", todayFormat);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hql;
	}
	
	private String whereHqlHasRes(EFApplication data, Map<String, Object> params) {
		String hql = "";
		hql += " where 1=1 ";
		if (data != null) {
			hql += " and t.member.id = "+data.getMemberId();
		}
		hql += " and t.serverDates>0 ";
		return hql;
	}

	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}

	@Override
	public void add(EFApplication data) {
		TEFapplication t = new TEFapplication();
		BeanUtils.copyProperties(data, t);
		t.setApplyDate(new Date());
		t.setDepartmentCheck(0);
		t.setFinalCheck(0);
		t.setServerDates(0);
		t.setMember(memberDao.get(TEFexpert.class, data.getMemberId()));
		dataDao.save(t);
	}
	
	@Override
	public EFApplication get(Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TEFapplication t = dataDao.get("from TEFapplication t  where t.id = :id", params);
		EFApplication u = new EFApplication();
		BeanUtils.copyProperties(t, u);
		
		if(t.getMember()!=null){
			u.setMemberId(t.getMember().getId());;
			u.setMemberLoginName(t.getMember().getLoginName());;
		}
		
		return u;
	}

	@Override
	public void updateServerDates(Long id) {
		ArrayList<String> serverDateArrayList = new ArrayList<String>();
		List<TEFresource> l = resourceDao.find(" from Txxresource t where t.data.id = "+id);
		for (TEFresource t : l) {
			EFResource u = new EFResource();
			BeanUtils.copyProperties(t, u);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
		    String date = sdf.format(u.getCreatTime());
		    if (!serverDateArrayList.contains(date)) {
				serverDateArrayList.add(date);
			}
		}
		TEFapplication t = dataDao.get(TEFapplication.class,id);
		t.setServerDates(serverDateArrayList.size());
		dataDao.update(t);
	}

	@Override
	public List<EFApplication> dataHasNotRes() {
		// TODO Auto-generated method stub
		List<EFApplication> ul = new ArrayList<EFApplication>();
		String hql = " from TEFapplication t where 1=1 and t.departmentCheck=0";
		List<TEFapplication> l = dataDao.find(hql);
		for (TEFapplication t : l) {
			EFApplication u = new EFApplication();
			BeanUtils.copyProperties(t, u);
			if (t.getMember() != null) {
				u.setMemberId(t.getMember().getId());
				u.setMemberLoginName(t.getMember().getLoginName());
			}
			ul.add(u);
		}
		return ul;
	}

}
