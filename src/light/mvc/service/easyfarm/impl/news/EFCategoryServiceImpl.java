package light.mvc.service.easyfarm.impl.news;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import light.mvc.dao.BaseDaoI;
import light.mvc.model.easyfarm.news.TEFcategory;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.news.EFCategory;
import light.mvc.service.easyfarm.news.*;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EFCategoryServiceImpl implements EFCategoryServiceI{

	@Autowired
	private BaseDaoI<TEFcategory> categoryDao;
	
	@Override
	public void add(EFCategory m) {
		// TODO Auto-generated method stub
		TEFcategory t= new TEFcategory();
		BeanUtils.copyProperties(m,t);
		categoryDao.save(t);
	}

	@Override
	public List<EFCategory> dataGrid(EFCategory category, PageFilter ph) {
		// TODO Auto-generated method stub
		
		List<EFCategory> ul = new ArrayList<EFCategory>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFcategory t ";
		List<TEFcategory> l = categoryDao.find(hql + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFcategory t : l) {
			EFCategory u = new EFCategory();
			BeanUtils.copyProperties(t, u);
			ul.add(u);
		}
		return ul;
	}

	@Override
	public Long count(EFCategory category, PageFilter ph) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFcategory t ";
		return categoryDao.count("select count(*) " + hql + whereHql(category, params), params);
	}

	@Override
	public void delete(Long id, HttpServletRequest request) {
		// TODO Auto-generated method stub
		TEFcategory t= categoryDao.get(TEFcategory.class,id);
		categoryDao.delete(t);
		
	}

	@Override
	public EFCategory get(Long id) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TEFcategory t = categoryDao.get("from TEFcategory t  where t.id = :id", params);
		EFCategory u = new EFCategory();
		BeanUtils.copyProperties(t, u);
		return u;
	}

	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	
	private String whereHql(EFCategory category, Map<String, Object> params) {//待修改
		String hql = "";
		if (category != null) {
			hql += " where 1=1 ";
		}

		return hql;
	}

	@Override
	public void edit(EFCategory data) {
		// TODO Auto-generated method stub
		TEFcategory t=categoryDao.get(TEFcategory.class, data.getId());
		categoryDao.update(t);
	}

	@Override
	public List<TEFcategory> getAllData() {
		// TODO Auto-generated method stub
		
		String hql = " from TEFcategory t ";
		List<TEFcategory> l = categoryDao.find(hql);
		return l;
	}
		


}
