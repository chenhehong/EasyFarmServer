package light.mvc.service.easyfarm.impl.news;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import light.mvc.dao.BaseDaoI;
import light.mvc.model.easyfarm.news.TEFnews;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.news.EFNews;
import light.mvc.service.easyfarm.news.*;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EFNewsServiceImpl implements EFNewsServiceI{

	@Autowired
	private BaseDaoI<TEFnews> newsDao;
	
	@Override
	public void add(EFNews m) {
		// TODO Auto-generated method stub
		TEFnews t = new TEFnews();
		BeanUtils.copyProperties(m,t);
		newsDao.save(t);
	}

	@Override
	public List<EFNews> dataGrid(EFNews news, PageFilter ph) {
		// TODO Auto-generated method stub
		
		List<EFNews> ul = new ArrayList<EFNews>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFnews t ";
		List<TEFnews> l = newsDao.find(hql + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFnews t : l) {
			EFNews u = new EFNews();
			BeanUtils.copyProperties(t, u);
			ul.add(u);
		}
		return ul;
	}

	@Override
	public Long count(EFNews news, PageFilter ph) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from TEFnews t ";
		return newsDao.count("select count(*) " + hql + whereHql(news, params), params);
	}

	@Override
	public void delete(Long id, HttpServletRequest request) {
		// TODO Auto-generated method stub
		TEFnews t= newsDao.get(TEFnews.class,id);
		newsDao.delete(t);
		
	}

	@Override
	public EFNews get(Long id) {
		// TODO Auto-generated method stub
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TEFnews t = newsDao.get("from TEFnews t  where t.id = :id", params);
		EFNews u = new EFNews();
		BeanUtils.copyProperties(t, u);
		return u;
	}

	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}
	
	private String whereHql(EFNews news, Map<String, Object> params) {//待修改
		String hql = "";
		if (news != null) {
			hql += " where 1=1 ";
		}

		return hql;
	}

	@Override
	public void edit(EFNews data) {
		// TODO Auto-generated method stub
		TEFnews t=newsDao.get(TEFnews.class,data.getId());
		newsDao.update(t);
	}
		


}
