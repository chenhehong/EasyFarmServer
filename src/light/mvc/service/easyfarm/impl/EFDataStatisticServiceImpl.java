package light.mvc.service.easyfarm.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javafx.scene.chart.PieChart.Data;

import javax.servlet.http.HttpServletRequest;

import jdk.nashorn.internal.ir.ContinueNode;
import light.mvc.dao.BaseDaoI;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.model.easyfarm.TEFapplication;
import light.mvc.model.easyfarm.TEFexpert;
import light.mvc.model.easyfarm.TEFresource;
import light.mvc.model.sys.Torganization;
import light.mvc.model.sys.Tresource;
import light.mvc.model.sys.Trole;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.pageModel.sys.User;
import light.mvc.service.base.ServiceException;
import light.mvc.service.easyfarm.EFDataDepartmentServiceI;
import light.mvc.service.easyfarm.EFDataFinalServiceI;
import light.mvc.service.easyfarm.EFDataStatisticServiceI;
import light.mvc.service.sys.UserServiceI;
import light.mvc.utils.MD5Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EFDataStatisticServiceImpl implements EFDataStatisticServiceI {

	@Autowired
	private BaseDaoI<TEFapplication> dataDao;
	
	@Autowired
	private BaseDaoI<TEFexpert> memberDao;
	
	@Autowired
	private BaseDaoI<TEFresource> resourceDao;

	@Override
	public void delete(Long id,HttpServletRequest request) {
		TEFapplication t = dataDao.get(TEFapplication.class, id);
		if ((t.getResources() != null) && (t.getResources().size() > 0)) {
			for (TEFresource r : t.getResources()) {
				resourceDao.delete(r);
				resourceDao.deleteFile(r.getFilepath(), request);
				resourceDao.deleteFile(r.getThumbFilepath(), request);
			}
		}
		dataDao.delete(t);
	}

	
	@Override
	public EFApplication get(Long id) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		TEFapplication t = dataDao.get("from Txxapplication t  where t.id = :id", params);
		EFApplication u = new EFApplication();
		BeanUtils.copyProperties(t, u);
		
		if(t.getMember()!=null){
			u.setMemberId(t.getMember().getId());
			u.setMemberLoginName(t.getMember().getLoginName());
		}
		
		return u;
	}

	@Override
	public List<EFApplication> dataGrid(EFApplication data, PageFilter ph) {
		List<EFApplication> ul = new ArrayList<EFApplication>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Txxapplication t";
		List<TEFapplication> l = dataDao.find(hql + whereHql(data, params) + orderHql(ph), params, ph.getPage(), ph.getRows());
		for (TEFapplication t : l) {
			EFApplication u = new EFApplication();
			BeanUtils.copyProperties(t, u);
			if (t.getMember() != null) {
				u.setMemberId(t.getMember().getId());
				u.setMemberLoginName(t.getMember().getLoginName());
			}
			ul.add(u);
		}
		return ul;
	}
	
	

	@Override
	public Long count(EFApplication data, PageFilter ph) {
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Txxapplication t ";
		return dataDao.count("select count(*) " + hql + whereHql(data, params), params);
	}

	private String whereHql(EFApplication data, Map<String, Object> params) {
		String hql = "";
		if (data != null) {
			hql += " where 1=1 ";
			if (data.getApplyMan() != null&&data.getApplyMan().length()!=0) {
				hql += " and t.applyMan = '"+data.getApplyMan()+"'";
			}
			if(data.getBusinessArea()!=null && data.getBusinessArea().length()!=0){
				hql += " and t.businessArea = '"+data.getBusinessArea()+"'";
			}
			if(data.getBusinessDate()!=null && data.getReturnDate()==null){
				hql += " and t.businessDate>=:businessDateFormat";
				params.put("businessDateFormat", data.getBusinessDate());
			}
			if(data.getReturnDate()!=null && data.getBusinessDate()==null){
				hql += " and t.returnDate<=:returnDateFormat";
				params.put("returnDateFormat", data.getReturnDate());
			}
			if(data.getBusinessDate()!=null && data.getReturnDate()!=null){
				hql += " and ((t.businessDate between :businessDateFormat and :returnDateFormat)"
						+ " OR (t.returnDate between :businessDateFormat and :returnDateFormat))";
				params.put("businessDateFormat", data.getBusinessDate());
				params.put("returnDateFormat", data.getReturnDate());
			}
			hql += " and t.finalCheck = 1";
			hql += " and t.departmentCheck = 1";
			hql += " and t.serverDates > 0";
		}
		return hql;
	}

	private String orderHql(PageFilter ph) {
		String orderString = "";
		if ((ph.getSort() != null) && (ph.getOrder() != null)) {
			orderString = " order by t." + ph.getSort() + " " + ph.getOrder();
		}
		return orderString;
	}

	@Override
	public List<Map<String, Object>> createExcelRecord(JSONArray jsonIds) {
        List<Map<String, Object>> listmap = new ArrayList<Map<String, Object>>();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("sheetName", "sheet1");
        listmap.add(map);
        for (int j = 0; j < jsonIds.length(); j++) {
			try {
				EFApplication data = get(jsonIds.getLong(j));
	            Map<String, Object> mapValue = new HashMap<String, Object>();
	            mapValue.put("id", data.getId());
	            mapValue.put("memberLoginName", data.getMemberLoginName());
	            mapValue.put("applyMan", data.getApplyMan());
	            mapValue.put("applyDate", data.getApplyDate());
	            mapValue.put("businessPersonnel", data.getBusinessPersonnel());
	            mapValue.put("businessPhone", data.getBusinessPhone());
	            mapValue.put("businessAddress", data.getBusinessArea()+data.getBusinessAddress());
	            mapValue.put("businessDate", data.getBusinessDate());
	            mapValue.put("businessMatter", data.getBusinessMatter());
	            mapValue.put("returnDate", data.getReturnDate());
	            mapValue.put("businessLinkMan", data.getBusinessLinkMan());
	            mapValue.put("businessLinkPhone", data.getBusinessLinkPhone());
	            mapValue.put("serverDates", data.getServerDates());
	            listmap.add(mapValue);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return listmap;
	}


	@Override
	public List<EFApplication> dataGridApplyManGroup() {
		List<EFApplication> ul = new ArrayList<EFApplication>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Txxapplication t";
		hql += " where 1=1 ";
		hql += " and t.finalCheck = 1";
		hql += " and t.departmentCheck = 1";
		hql += " and t.serverDates > 0 group by applyMan";
		List<TEFapplication> l = dataDao.find(hql);
		for (TEFapplication t : l) {
			EFApplication u = new EFApplication();
			BeanUtils.copyProperties(t, u);
			if (t.getMember() != null) {
				u.setMemberId(t.getMember().getId());
				u.setMemberLoginName(t.getMember().getLoginName());
			}
			ul.add(u);
		}
		return ul;
	}


	@Override
	public List<EFApplication> dataGridBusinessAreaGroup() {
		List<EFApplication> ul = new ArrayList<EFApplication>();
		Map<String, Object> params = new HashMap<String, Object>();
		String hql = " from Txxapplication t";
		hql += " where 1=1 ";
		hql += " and t.finalCheck = 1";
		hql += " and t.departmentCheck = 1";
		hql += " and t.serverDates > 0 group by businessArea";
		List<TEFapplication> l = dataDao.find(hql);
		for (TEFapplication t : l) {
			EFApplication u = new EFApplication();
			BeanUtils.copyProperties(t, u);
			if (t.getMember() != null) {
				u.setMemberId(t.getMember().getId());
				u.setMemberLoginName(t.getMember().getLoginName());
			}
			ul.add(u);
		}
		return ul;
	}
	
}
