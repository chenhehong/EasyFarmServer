package light.mvc.service.easyfarm.news;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.news.EFNews;

public interface EFNewsServiceI {
	
	public void add(EFNews xxnews);
	
	public List<EFNews> dataGrid(EFNews news,PageFilter ph);
	
	public Long count(EFNews news,PageFilter ph);
		
	public void delete(Long id,HttpServletRequest request);

	public EFNews get(Long id);	

	public void edit(EFNews data);
}
