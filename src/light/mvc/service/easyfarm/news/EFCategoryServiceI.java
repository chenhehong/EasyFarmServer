package light.mvc.service.easyfarm.news;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.model.easyfarm.news.TEFcategory;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.news.EFCategory;

public interface EFCategoryServiceI {
	
	public void add(EFCategory category);
	
	public List<EFCategory> dataGrid(EFCategory category,PageFilter ph);
	
	public List<TEFcategory> getAllData();
	
	public Long count(EFCategory category,PageFilter ph);
		
	public void delete(Long id,HttpServletRequest request);

	public EFCategory get(Long id);	
	
	public void edit(EFCategory data);
	
	

}
