package light.mvc.service.easyfarm;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;

import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.EFApplication;

public interface EFDataStatisticServiceI {

	public List<EFApplication> dataGrid(EFApplication data,PageFilter ph);

	public Long count(EFApplication data, PageFilter ph);

	public void delete(Long id,HttpServletRequest request);

	public EFApplication get(Long id);
	
	public List<Map<String, Object>> createExcelRecord(JSONArray jsonIds);
	
	public List<EFApplication> dataGridApplyManGroup();
	
	public List<EFApplication> dataGridBusinessAreaGroup();
}
