package light.mvc.service.easyfarm.agribusiness;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.agribusiness.EFAgriBusiness;

public interface EFAgriBusinessServiceI {
	public void add(EFAgriBusiness agriB);
	
	public List<EFAgriBusiness> dataGrid(EFAgriBusiness news,PageFilter ph);
	
	public Long count(EFAgriBusiness news,PageFilter ph);
		
	public void delete(Long id,HttpServletRequest request);

	public EFAgriBusiness get(Long id);	

	public void edit(EFAgriBusiness data);
}
