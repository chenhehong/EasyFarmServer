package light.mvc.service.easyfarm;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.pageModel.easyfarm.EFExpert;
import light.mvc.pageModel.easyfarm.EFResource;

public interface EFDataFrontServiceI {

	public List<EFApplication> dataGrid(EFApplication data,PageFilter ph);
	
	public List<EFApplication> dataGridEnableUploadRes(EFApplication data,PageFilter ph);//可以上传资源的申请
	
	public List<EFApplication> dataGridHasRes(EFApplication data,PageFilter ph);//serverDates大于0的申请
	
	public List<EFApplication> dataHasNotRes();//还未审核的

	public Long count(EFApplication data, PageFilter ph);

	public void delete(Long id,HttpServletRequest request);

	public void edit(EFApplication data);

	public void add(EFApplication data);
	
	public EFApplication get(Long id);
	
	public void updateServerDates(Long id);
}
