package light.mvc.service.easyfarm;

import java.util.List;

import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.EFApplication;

public interface EFDataFinalServiceI {

	public List<EFApplication> dataGrid(EFApplication data,PageFilter ph);

	public Long count(EFApplication data, PageFilter ph);
	
	public List<EFApplication> manageDataGrid(EFApplication data,PageFilter ph);

	public Long manageCount(EFApplication data, PageFilter ph);

	public void delete(Long id);

	public void pass(Long id);

	public void edit(EFApplication data);

	public EFApplication get(Long id);
	
}
