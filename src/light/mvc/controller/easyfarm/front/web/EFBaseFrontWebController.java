package light.mvc.controller.easyfarm.front.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.service.easyfarm.EFDataFrontServiceI;
@Controller
@RequestMapping("/front/web/base")
public class EFBaseFrontWebController extends BaseController{

	
	@Autowired 
	private EFDataFrontServiceI dataService;
	
	@RequestMapping("/index")
	public String index(HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		request.setAttribute("sessionInfo", sessionInfo); 
			return "/front/index";
	
	}
	
	@RequestMapping("/getLoginPage")
	public String getLoginPage(HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		request.setAttribute("sessionInfo", sessionInfo); 
		if ((sessionInfo != null) && (sessionInfo.getId() != null)) {
			return "/front/index";
		}
		return "/front/login";
			
	}
	
	@RequestMapping("/getArticleList")
	public String getArticleList() {
		return "/front/articlelist";
			
	}
	
	@RequestMapping("/getArticle")
	public String getArticle() {
		return "/front/article";
			
	}
	
	
	@RequestMapping("/getInformationList")
	public String getInformationList() {
		return "/front/informationlist";
			
	}
	@RequestMapping("/getQuestionList")
	public String getQuestionList() {
		return "/front/questionlist";
			
	}
	@RequestMapping("/getQuestionPage")
	public String getQuestionPage() {
		return "/front/questionpage";
			
	}
	
	@RequestMapping("/getPersonalCenter")
	public String getPersonalCenter(HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		request.setAttribute("sessionInfo", sessionInfo); 
		return "/front/personalcenter";
			
	}
	@RequestMapping("/getKnowledgeData")
	public String getKnowledgeData() {
		
		return "/front/knowledgedata";
			
	}
	
	@RequestMapping("/getPerformanceList")
	public String getPerformanceList() {
		
		return "/front/performancelist";
			
	}
	@RequestMapping("/getCountrysideList")
	public String getCountrysideList(HttpServletRequest request) {
		List<EFApplication> ul=dataService.dataHasNotRes();
		request.setAttribute("efApplication", ul);
		return "/front/countrysidelist";
			
	}
	
	@RequestMapping("/getCommpanyList")
	public String getCommpanyList() {
		
		return "/front/commpanylist";
			
	}
	
	@RequestMapping("/getRegister")
	public String getRegister() {
		
		return "/front/register";
			
	}
	
	
	@RequestMapping("/getPersonalQuestionList")
	public String getPersonalQuestionList() {
		
		return "/front/personalquestionlist";
			
	}
	
}
