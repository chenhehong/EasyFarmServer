package light.mvc.controller.easyfarm.front.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.pageModel.easyfarm.EFExpert;
import light.mvc.service.easyfarm.EFDataFrontServiceI;
import light.mvc.service.easyfarm.EFExpertServiceI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/front/web/expert")
public class EFExpertFrontWebController extends BaseController{
	@Autowired 
	private EFExpertServiceI memberService;
	
	@Autowired 
	private EFDataFrontServiceI dataService;

	@ResponseBody
	@RequestMapping("/login")
	public Json login(EFExpert member, HttpSession session) {
		Json j = new Json();
		//System.out.println(member.getLoginName()+"pass"+member.getPassword());
		EFExpert sysuser = memberService.login(member);
		if (sysuser != null) {
			j.setSuccess(true);
			j.setMsg("登陆成功！");

			SessionInfo sessionInfo = new SessionInfo();
			sessionInfo.setId(sysuser.getId());
			sessionInfo.setLoginname(sysuser.getLoginName());
			sessionInfo.setName(sysuser.getRealName());
			session.setAttribute(GlobalConstant.SESSION_INFO_EXPERT, sessionInfo);
			sessionInfo.setSessionId(session.getId());
			j.setObj(sessionInfo);
		} else {
			j.setMsg("用户名或密码错误！");
		}
		return j;
		
	}
	

	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		
		if (session != null) {
			session.invalidate();
		}
		
		return "/front/index";
	}
	
	@RequestMapping("/getApplycountryside")
	public String getApplycountryside() {
		
		return "/front/applycountryside";
			
	}
	
	@RequestMapping("/getApplyCommpany")
	public String getApplyCommpany() {
		
		return "/front/applycommpany";
			
	}
	
	@RequestMapping("/getAskQuestionPage")
	public String getAskQuestionPage() {
		
		return "/front/askquestionpage";
			
	}
	

	@ResponseBody
	@RequestMapping("/addApplyCountryside")
	public Json addApplyCountryside(HttpServletRequest request,EFApplication data) {
		
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		data.setMemberId(sessionInfo.getId());
		data.setApplyMan(sessionInfo.getLoginname());		
		Json j = new Json();
		try {
			dataService.add(data);
			j.setSuccess(true);
			j.setMsg("提交下乡申请成功！");
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}	
	
	
}
