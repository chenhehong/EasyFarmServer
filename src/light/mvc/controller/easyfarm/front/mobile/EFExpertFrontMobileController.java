package light.mvc.controller.easyfarm.front.mobile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFExpert;
import light.mvc.pageModel.sys.User;
import light.mvc.service.easyfarm.EFExpertServiceI;
import light.mvc.utils.MD5Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/front/mobile/expert")
public class EFExpertFrontMobileController extends BaseController{

	@Autowired 
	private EFExpertServiceI memberService;
	
	@RequestMapping("/editUserPwd")
	@ResponseBody
	public Json editUserPwd(HttpServletRequest request,String oldPwd, String pwd) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		Json j = new Json();
		try {
			if(memberService.editUserPwd(sessionInfo, oldPwd, pwd)){
				j.setSuccess(true);
				j.setMsg("密码修改成功！");
			}else{
				j.setSuccess(false);
				j.setMsg("原密码输入错误！");
			}
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/register")
	@ResponseBody
	public Json register(EFExpert member) {
		Json j = new Json();
		EFExpert u = memberService.getByLoginName(member);
		if(u!=null){
			j.setMsg("用户名已存在!");
		}else{
			try {
				member.setStatus(0);
				member.setPassword(MD5Util.md5(member.getPassword()));
				memberService.add(member);
				j.setSuccess(true);
				j.setMsg("提交注册成功，请等待审核！");
			} catch (Exception e) {
				j.setMsg(e.getMessage());
			}
			
		}
		return j;
	}
	
	@ResponseBody
	@RequestMapping("/login")
	public Json login(EFExpert member, HttpSession session) {
		Json j = new Json();
		EFExpert sysuser = memberService.login(member);
		if (sysuser != null) {
			j.setSuccess(true);
			j.setMsg("登陆成功！");

			SessionInfo sessionInfo = new SessionInfo();
			sessionInfo.setId(sysuser.getId());
			sessionInfo.setLoginname(sysuser.getLoginName());
			sessionInfo.setName(sysuser.getRealName());
			session.setAttribute(GlobalConstant.SESSION_INFO_EXPERT, sessionInfo);
			sessionInfo.setSessionId(session.getId());
			j.setObj(sessionInfo);
		} else {
			j.setMsg("用户名或密码错误！");
		}
		return j;
	}

}
