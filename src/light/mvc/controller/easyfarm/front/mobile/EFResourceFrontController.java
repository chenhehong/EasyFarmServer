package light.mvc.controller.easyfarm.front.mobile;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.pageModel.easyfarm.EFResource;
import light.mvc.service.easyfarm.EFDataFrontServiceI;
import light.mvc.service.easyfarm.EFResourceFrontServiceI;
import light.mvc.utils.MyImageUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;


@Controller
@RequestMapping("/front/mobile/xxresource") 
public class EFResourceFrontController extends BaseController{

	
	@Autowired 
	private EFResourceFrontServiceI resourceService;
	
	@Autowired
	private EFDataFrontServiceI dataService;

	@RequestMapping("/dataGrid")
	@ResponseBody
	public Json dataGrid(HttpServletRequest request,EFResource resource, PageFilter ph) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		Json j = new Json();
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(resourceService.dataGrid(sessionInfo,resource, ph));
		grid.setTotal(resourceService.count(sessionInfo,resource, ph));
		j.setSuccess(true);
		j.setObj(grid);
		return j;
	}

	@RequestMapping("/delete")
	@ResponseBody
//	ids为jsonArray
	public Json delete(String ids,HttpServletRequest request) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				long id = jsonIds.getLong(i);
//				验证权限
				EFResource r = resourceService.get(id);
				EFApplication d = dataService.get(r.getDataId());
				if (d.getMemberId()!=sessionInfo.getId()) {
					j.setMsg("非法操作");
					return j;
				}
				resourceService.delete(jsonIds.getLong(i),request);
//				更新服务天数
				dataService.updateServerDates(r.getDataId());
			}
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Json add(HttpServletRequest request,EFResource resource,@RequestParam("file") CommonsMultipartFile file) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);	
		Json j = new Json();
//		权限验证
		EFApplication data = dataService.get(resource.getDataId());
		if ((long)data.getMemberId()!=(long)sessionInfo.getId()) {
			j.setMsg("非法操作");
			return j;
		}
//		判断是否是该申请审核通过
		if (data.getDepartmentCheck()==0||data.getFinalCheck()==0) {
			j.setMsg("该申请的审核尚未通过，不能上传！");
			return j;
		}
//		判断资源对应的时间段正确
		Date now = new Date(); 
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd"); 
		try {
			Date today = sdf.parse(sdf.format(now));
			if (today.before(data.getBusinessDate())||today.after(data.getReturnDate())) {
				j.setMsg("该资源不在下乡服务的时间段内，不能上传！");
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			j.setMsg(e1.getMessage());
			return j;
		}
//		判断资源的地点是否正确，后期和手机端对接再做
		if (!data.getBusinessArea().equals(resource.getAddress())) {
			j.setMsg("该资源不在下乡服务的服务范围内，不能上传！");
			return j;
		}
//		 获取上传的图片的文件名
        String uploadFileName = file.getOriginalFilename();
//      获取图片的扩展名
        String extensionName = uploadFileName.substring(uploadFileName.lastIndexOf(".") + 1);
//      生成保存的图片文件名
        String prefixName = System.currentTimeMillis()+"";
        String fileName = prefixName+"."+extensionName;
		String filePath=GlobalConstant.uploadFolder+"/"+fileName;//文件路径名
		String realFilepath = request.getSession().getServletContext().getRealPath("/")+filePath;
        String videoFilepath = request.getSession().getServletContext().getRealPath("/")+GlobalConstant.uploadThumbFolder+"/video.png";//视频的图片文件路径名
        File newFile=new File(realFilepath);
        String thumbFilepath = GlobalConstant.uploadThumbFolder+"/"+prefixName+".jpg";//缩略图文件路径名
        String thumbRealFilepath = request.getSession().getServletContext().getRealPath("/")+thumbFilepath;
		try {
//          通过CommonsMultipartFile的方法直接写文件
	        file.transferTo(newFile);
//			保存文件路径
	        resource.setFilepath(filePath);
//	                        如果是图片，生成缩略图并保存
	        if (resource.getType().equals(GlobalConstant.PICTURETYPE)) {
	        	MyImageUtil.Tosmallerpic(realFilepath,thumbRealFilepath, 64,64,0.8F);
			}else if (resource.getType().equals(GlobalConstant.VIDEOTYPE)) {
	        	MyImageUtil.Tosmallerpic(videoFilepath,thumbRealFilepath, 64,64,0.8F);
			}
	        resource.setThumbFilepath(thumbFilepath);
			resourceService.add(resource);
//			更新服务天数
			dataService.updateServerDates(resource.getDataId());
			j.setSuccess(true);
			j.setMsg("添加成功！");
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}	

}
