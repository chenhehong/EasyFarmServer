package light.mvc.controller.easyfarm.front.mobile;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.base.SessionInfo;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.service.easyfarm.EFDataFrontServiceI;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/front/mobile/xxdata")
public class EFDataFrontController extends BaseController{

	@Autowired 
	private EFDataFrontServiceI dataService;
	

	@RequestMapping("/dataGrid")
	@ResponseBody
	public Json dataGrid(HttpServletRequest request,EFApplication data, PageFilter ph) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		data.setMemberId(sessionInfo.getId());
		Json j = new Json();
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(dataService.dataGrid(data, ph));
		grid.setTotal(dataService.count(data, ph));
		j.setSuccess(true);
		j.setObj(grid);
		return j;
	}
	
	@RequestMapping("/dataGridEnableUploadRes")
	@ResponseBody
	public Json dataGridEnableUploadRes(HttpServletRequest request,EFApplication data, PageFilter ph) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		data.setMemberId(sessionInfo.getId());
		Json j = new Json();
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(dataService.dataGridEnableUploadRes(data, ph));
		j.setSuccess(true);
		j.setObj(grid);
		return j;
	}
	
	@RequestMapping("/dataGridHasRes")
	@ResponseBody
	public Json dataGridHasRes(HttpServletRequest request,EFApplication data, PageFilter ph) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		data.setMemberId(sessionInfo.getId());
		Json j = new Json();
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(dataService.dataGridHasRes(data, ph));
		j.setSuccess(true);
		j.setObj(grid);
		return j;
	}

	@RequestMapping("/delete")
	@ResponseBody
//	ids为jsonArray
	public Json delete(HttpServletRequest request,String ids) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				long id = jsonIds.getLong(i);
//				验证权限
				EFApplication d = dataService.get(id);
				if (d.getMemberId()!=sessionInfo.getId()) {
					j.setMsg("非法操作");
					return j;
				}
				if(d.getDepartmentCheck()==1&&d.getFinalCheck()==1){
					j.setMsg("非法操作");
					return j;
				}
				dataService.delete(id,request);
			}
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	

	@RequestMapping("/edit")
	@ResponseBody
	public Json edit(HttpServletRequest request,EFApplication data) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		Json j = new Json();
//		验证权限
		EFApplication d = dataService.get(data.getId());
		if (d.getMemberId()!=sessionInfo.getId()) {
			j.setMsg("非法操作");
			return j;
		}
		try {
				dataService.edit(data);
				j.setSuccess(true);
				j.setMsg("编辑成功！");
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Json add(HttpServletRequest request,EFApplication data) {
		SessionInfo sessionInfo = (SessionInfo) request.getSession().getAttribute(GlobalConstant.SESSION_INFO_EXPERT);
		data.setMemberId(sessionInfo.getId());
		Json j = new Json();
		try {
			dataService.add(data);
			j.setSuccess(true);
			j.setMsg("提交下乡申请成功！");
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}	

	@RequestMapping("/getProvinces")
	@ResponseBody
	public Json getProvinces(HttpServletRequest request) {
		Json j = new Json();
		String proPath=request.getSession().getServletContext().getRealPath("/");
		String provinceFilePath = proPath+"/jsonDatas/provinces.json";
		BufferedReader reader = null;
		String jsonStr = "";
		try {
			FileInputStream fileInputStream = new FileInputStream(provinceFilePath);
			InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
			reader = new BufferedReader(inputStreamReader);
			String tempString = null;
			while((tempString = reader.readLine()) != null){
				jsonStr += tempString;
			}
			reader.close();
			j.setObj(jsonStr);
			j.setSuccess(true);
			j.setMsg("成功获取各省信息");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			j.setMsg(e1.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/getCitys")
	@ResponseBody
	public Json getCitys(HttpServletRequest request,int ProID) {
		Json j = new Json();
		String proPath=request.getSession().getServletContext().getRealPath("/");
		String cityFilePath = proPath+"/jsonDatas/citys.json";
		BufferedReader reader = null;
		String jsonStr = "";
		try {
			FileInputStream fileInputStream = new FileInputStream(cityFilePath);
			InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
			reader = new BufferedReader(inputStreamReader);
			String tempString = null;
			while((tempString = reader.readLine()) != null){
				jsonStr += tempString;
			}
			reader.close();
			// 解析数据
	        JSONArray jsonArray = new JSONArray(jsonStr);
//	                        定义存放符合的城市的json数组
	        JSONArray jsonCitysArray = new JSONArray();
	        int length = jsonArray.length();
            int i = 0;
            int continuous = 0;// 这个变量用于判断是否连续几次没有找到，如果是，则该省信息全部找到了
            boolean isFind = false;
            while (i < length) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int fGuid = jsonObject.getInt("ProID");
                if (fGuid==ProID) {
                    isFind = true;
                    jsonCitysArray.put(jsonObject);
                } else {
                    if (isFind == true) {
                        continuous += 1;
                        if (continuous > 5) {
                            break;
                        }
                    }
                }
                i++;
            }
			j.setObj(jsonCitysArray.toString());
			j.setSuccess(true);
			j.setMsg("成功获取城市信息");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			j.setMsg(e1.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/getCountys")
	@ResponseBody
	public Json getCountys(HttpServletRequest request,int CityID) {
		Json j = new Json();
		String proPath=request.getSession().getServletContext().getRealPath("/");
		String countyFilePath = proPath+"/jsonDatas/countys.json";
		BufferedReader reader = null;
		String jsonStr = "";
		try {
			FileInputStream fileInputStream = new FileInputStream(countyFilePath);
			InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");
			reader = new BufferedReader(inputStreamReader);
			String tempString = null;
			while((tempString = reader.readLine()) != null){
				jsonStr += tempString;
			}
			reader.close();
			// 解析数据
	        JSONArray jsonArray = new JSONArray(jsonStr);
//	                        定义存放符合的县的json数组
	        JSONArray jsonCountysArray = new JSONArray();
	        int length = jsonArray.length();
            int i = 0;
            int continuous = 0;// 这个变量用于判断是否连续几次没有找到，如果是，则该省信息全部找到了
            boolean isFind = false;
            while (i < length) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int fGuid = jsonObject.getInt("CityID");
                if (fGuid==CityID) {
                    isFind = true;
                    jsonCountysArray.put(jsonObject);
                } else {
                    if (isFind == true) {
                        continuous += 1;
                        if (continuous > 5) {
                            break;
                        }
                    }
                }
                i++;
            }
			j.setObj(jsonCountysArray.toString());
			j.setSuccess(true);
			j.setMsg("成功获取县信息");
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
			j.setMsg(e1.getMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			j.setMsg(e.getMessage());
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			j.setMsg(e.getMessage());
		}
		return j;
	}
}
