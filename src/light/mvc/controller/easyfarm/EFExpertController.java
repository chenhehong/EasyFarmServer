package light.mvc.controller.easyfarm;

import javax.servlet.http.HttpServletRequest;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.EFExpert;
import light.mvc.service.easyfarm.EFExpertServiceI;
import light.mvc.utils.MD5Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/expert")
public class EFExpertController extends BaseController{

	@Autowired 
	private EFExpertServiceI expertService;
	
	@RequestMapping("/manager")
	public String manager() {
		return "/admin/easyfarm/expert";
	}

	@RequestMapping("/dataGrid")
	@ResponseBody
	public Grid dataGrid(EFExpert expert, PageFilter ph) {
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(expertService.dataGrid(expert, ph));
		grid.setTotal(expertService.count(expert, ph));
		return grid;
	}
	
	@RequestMapping("/addPage")
	public String addPage() {
		return "/admin/easyfarm/expertAdd";
	}

	@RequestMapping("/add")
	@ResponseBody
	public Json add(EFExpert expert) {
		Json j = new Json();
		EFExpert m = expertService.getByLoginName(expert);
		if(m!=null){
			j.setMsg("用户名已存在!");
		}else{
			try {
				expert.setPassword(MD5Util.md5(expert.getPassword()));
				expertService.add(expert);
				j.setSuccess(true);
				j.setMsg("添加成功！");
			} catch (Exception e) {
				j.setMsg(e.getMessage());
			}
		}
		return j;
	}

	@RequestMapping("/delete")
	@ResponseBody
//	ids为jsonArray
	public Json delete(String ids) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				expertService.delete(jsonIds.getLong(i));
			}
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/editPage")
	public String editPage(HttpServletRequest request, Long id) {
		EFExpert m = expertService.get(id);
		request.setAttribute("expert", m); 
		request.setAttribute("statusList",GlobalConstant.statuslist);
		return "/admin/easyfarm/expertEdit";
	}

	@RequestMapping("/edit")
	@ResponseBody
	public Json edit(EFExpert expert) {
		Json j = new Json();
		EFExpert m = expertService.getByLoginName(expert);
		if(m!=null&&m.getId()!=expert.getId()){
			j.setMsg("用户名已存在!");
		}else{
			try {
				expert.setPassword(MD5Util.md5(expert.getPassword()));
				expertService.edit(expert);
				j.setSuccess(true);
				j.setMsg("编辑成功！");
			} catch (Exception e) {
				j.setMsg(e.getMessage());
			}
		}
		return j;
	}
	
	@RequestMapping("/verifyPage")
	public String verifyPage() {
		return "/admin/easyfarm/expertVerify";
	}

	@RequestMapping("/pass")
	@ResponseBody
	public Json pass(String ids) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				expertService.pass(jsonIds.getLong(i));
			}
			j.setMsg("通过审核成功！");
			j.setSuccess(true);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}

}
