package light.mvc.controller.easyfarm;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.service.easyfarm.EFDataStatisticServiceI;
import light.mvc.utils.DateTimeUtil;
import light.mvc.utils.ExcelUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/xxdata_statistic")
public class EFDataStatisticController extends BaseController{

	@Autowired 
	private EFDataStatisticServiceI dataService;
	
	@RequestMapping("/manager")
	public String manager(HttpServletRequest request) {
//		设置筛选栏菜单
		List<EFApplication> dataApplyManList = dataService.dataGridApplyManGroup();
		List<EFApplication> dataBusinessAreaList = dataService.dataGridBusinessAreaGroup();
		HashMap<String, String> applyManMap = new HashMap<String, String>();
		HashMap<String, String> businessAreaMap = new HashMap<String, String>();
		for (int i = 0; i < dataApplyManList.size(); i++) {
			EFApplication applyManData = dataApplyManList.get(i);
			applyManMap.put(applyManData.getApplyMan(), applyManData.getApplyMan());
		}
		applyManMap.put("1", "所有申请人");
		for (int i = 0; i < dataBusinessAreaList.size(); i++) {
			EFApplication businessAreaData = dataBusinessAreaList.get(i);
			businessAreaMap.put(businessAreaData.getBusinessArea(),businessAreaData.getBusinessArea());
		}
		businessAreaMap.put("1", "所有区域");
		String today = DateTimeUtil.getCurrentDateStr("yyyy-MM-dd");
		String preYearDate = DateTimeUtil.getPreDateStr(365, "yyyy-MM-dd");
		request.setAttribute("today", today);
		request.setAttribute("preYearDate", preYearDate);
		request.setAttribute("dataApplyManList",applyManMap);
		request.setAttribute("dataBusinessAreaList",businessAreaMap);
		return "/admin/xiaxiang/dataStatistic";
	}

	@RequestMapping("/dataGrid")
	@ResponseBody
	public Grid dataGrid(EFApplication data, PageFilter ph) {
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(dataService.dataGrid(data, ph));
		grid.setTotal(dataService.count(data, ph));
		return grid;
	}
	
//	获取列表菜单的选项：申请人、下乡区域
	@RequestMapping("/getListMenu")
	@ResponseBody
	public Json getListMenu() {
		Json j = new Json();
		List<Grid> list = new ArrayList<Grid>();
		Grid gridApplyMan = new Grid();
		gridApplyMan.setRows(dataService.dataGridApplyManGroup());
		Grid gridBusinessArea = new Grid();
		gridBusinessArea.setRows(dataService.dataGridBusinessAreaGroup());
		list.add(gridApplyMan);
		list.add(gridBusinessArea);
		j.setSuccess(true);
		j.setObj(list);
		return j;
	}
	
	/**
	 * 手机端
	 */
	@RequestMapping("/dataGridMobile")
	@ResponseBody
	public Json dataGridMobile(EFApplication data, PageFilter ph) {
		Json j = new Json();
//		spring自动将参数注入到ph对象中
		Grid grid = dataGrid(data, ph);
		j.setSuccess(true);
		j.setObj(grid);
		return j;
	}

	@RequestMapping("/delete")
	@ResponseBody
//	ids为jsonArray
	public Json delete(String ids,HttpServletRequest request) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				dataService.delete(jsonIds.getLong(i),request);
			}
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/exportExcel")
	@ResponseBody
//	ids为jsonArray，导出excel
	public String  exportExecl(String ids,HttpServletRequest request,HttpServletResponse response) {
//		ids = "['1']";
		JSONArray jsonIds;
		Json j = new Json();
		String fileName = System.currentTimeMillis()+"";
		try {
			jsonIds = new JSONArray(ids);
			List<Map<String,Object>> list=dataService.createExcelRecord(jsonIds);
	        String columnNames[]={"ID","所属用户","申请人","申请时间","下乡人员","下乡人员电话","下乡地点",
	        		              "下乡时间","下乡事由","返回时间","服务联系人","服务联系人电话","服务天数"};//列名
	        String keys[]={"id","memberLoginName","applyMan","applyDate","businessPersonnel",
	        			   "businessPhone","businessAddress","businessDate","businessMatter",
	        			   "returnDate","businessLinkMan","businessLinkPhone","serverDates"};//map中的key
	        ByteArrayOutputStream os = new ByteArrayOutputStream();
	        ExcelUtil.createWorkBook(list,keys,columnNames).write(os);
	        byte[] content = os.toByteArray();
	        InputStream is = new ByteArrayInputStream(content);
	        // 设置response参数，可以打开下载页面
	        response.reset();
	        response.setContentType("application/vnd.ms-excel;charset=utf-8");
	        response.setHeader("Content-Disposition", "attachment;filename="+ new String((fileName + ".xls").getBytes(), "iso-8859-1"));
	        ServletOutputStream out = response.getOutputStream();
	        BufferedInputStream bis = null;
	        BufferedOutputStream bos = null;
	        bis = new BufferedInputStream(is);
            bos = new BufferedOutputStream(out);
            byte[] buff = new byte[2048];
            int bytesRead;
            // Simple read/write loop.
            while (-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
            if (bis != null)
                bis.close();
            if (bos != null)
                bos.close();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/detailPage")
	public String detailPage(HttpServletRequest request, Long id) {
		EFApplication d = dataService.get(id);
		request.setAttribute("data", d);
		request.setAttribute("statusList",GlobalConstant.statuslist);
		return "/admin/xiaxiang/dataStatisticDetail";
	}

}
