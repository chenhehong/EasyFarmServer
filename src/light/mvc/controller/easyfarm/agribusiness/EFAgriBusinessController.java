package light.mvc.controller.easyfarm.agribusiness;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.controller.base.BaseController;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;


import light.mvc.pageModel.easyfarm.agribusiness.EFAgriBusiness;
import light.mvc.service.easyfarm.agribusiness.EFAgriBusinessServiceI;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/agribusiness")
public class EFAgriBusinessController  extends BaseController{

	@Autowired 
	private EFAgriBusinessServiceI agriBService;
	
	
	@RequestMapping("/getPage")
	public String get_news_page() {
		
		return "/admin/easyfarm/agribusiness/content";
	}
	
	@RequestMapping("/dataGrid")
	@ResponseBody
	public  Grid data_grid(EFAgriBusiness content, PageFilter ph) {
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(agriBService.dataGrid(content, ph));
		grid.setTotal(agriBService.count(content, ph));
		return grid;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Json delete(String ids,HttpServletRequest request) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				long id = jsonIds.getLong(i);
				agriBService.delete(id,request);
			}
			
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/addPage")
	public String addPage(HttpServletRequest request) {
		return "/admin/easyfarm/agribusiness/contentAdd";
	}
	

	
	@RequestMapping("/add")
	@ResponseBody
	public Json add(EFAgriBusiness nobj) {
		//System.out.println("addnews");
		Json j = new Json();
			try {
				agriBService.add(nobj);
				j.setSuccess(true);
				j.setMsg("添加成功！");
			} catch (Exception e) {
				j.setMsg(e.getMessage());
			}
		
		return j;
	}
}
