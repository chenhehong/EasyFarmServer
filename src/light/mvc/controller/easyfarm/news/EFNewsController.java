package light.mvc.controller.easyfarm.news;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.model.easyfarm.news.TEFcategory;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.news.EFNews;
import light.mvc.service.easyfarm.news.EFCategoryServiceI;
import light.mvc.service.easyfarm.news.EFNewsServiceI;
import light.mvc.utils.MD5Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/news")
public class EFNewsController extends BaseController{

	@Autowired 
	private EFNewsServiceI newsService;
	
	@Autowired 
	private EFCategoryServiceI categoryService;
	
	@RequestMapping("/newsPage")
	public String get_news_page() {
		
		return "/admin/easyfarm/news/news";
	}
	
	@RequestMapping("/dataGrid")
	@ResponseBody
	public  Grid data_grid(EFNews news, PageFilter ph) {
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(newsService.dataGrid(news, ph));
		grid.setTotal(newsService.count(news, ph));
		return grid;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Json delete(String ids,HttpServletRequest request) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				long id = jsonIds.getLong(i);
				newsService.delete(id,request);
			}
			
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/addPage")
	public String addPage(HttpServletRequest request) {
		List<TEFcategory> category =categoryService.getAllData();
		request.setAttribute("category", category);
		return "/admin/easyfarm/news/newsAdd";
	}
	

	
	@RequestMapping("/add")
	@ResponseBody
	public Json add(EFNews nobj) {
		//System.out.println("addnews");
		Json j = new Json();
			try {
				newsService.add(nobj);
				j.setSuccess(true);
				j.setMsg("添加成功！");
			} catch (Exception e) {
				j.setMsg(e.getMessage());
			}
		
		return j;
	}


}
