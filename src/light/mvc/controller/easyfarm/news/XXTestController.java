package light.mvc.controller.easyfarm.news;


import javax.servlet.http.HttpServletRequest;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.news.EFCategory;
import light.mvc.service.easyfarm.news.EFCategoryServiceI;
import light.mvc.utils.MD5Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/test")
public class XXTestController extends BaseController{

	@Autowired 
	private EFCategoryServiceI Service;
	
	@RequestMapping("/testPage")
	public String getPage() {
		return "/admin/news/category";
	}
	
	@RequestMapping("/dataGrid")
	@ResponseBody
	public  Grid dataGrid(EFCategory category, PageFilter ph) {
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(Service.dataGrid(category, ph));
		grid.setTotal(Service.count(category, ph));
		return grid;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Json delete(String ids,HttpServletRequest request) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				long id = jsonIds.getLong(i);
				Service.delete(id,request);
			}
			
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/addPage")
	public String addPage() {
		return "/admin/news/newsAdd";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Json add(EFCategory nobj) {
		Json j = new Json();
			try {
				Service.add(nobj);
				j.setSuccess(true);
				j.setMsg("添加成功！");
			} catch (Exception e) {
				j.setMsg(e.getMessage());
			}
		
		return j;
	}
	
}
