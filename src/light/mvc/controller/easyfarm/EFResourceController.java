package light.mvc.controller.easyfarm;

import javax.servlet.http.HttpServletRequest;

import light.mvc.controller.base.BaseController;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.pageModel.easyfarm.EFResource;
import light.mvc.service.easyfarm.EFDataFrontServiceI;
import light.mvc.service.easyfarm.EFResourceServiceI;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/xxresource") 
public class EFResourceController extends BaseController{

	
	@Autowired 
	private EFResourceServiceI resourceService;
	
	@Autowired
	private EFDataFrontServiceI dataService;
	
	@RequestMapping("/manager")
	public String manager() {
		return "/admin/xiaxiang/resource";
	}

	@RequestMapping("/dataGrid")
	@ResponseBody
	public Grid dataGrid(EFResource resource, PageFilter ph) {
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(resourceService.dataGrid(resource, ph));
		grid.setTotal(resourceService.count(resource, ph));
		return grid;
	}
	
	/**
	 * 手机端
	 */
	@RequestMapping("/dataGridMobile")
	@ResponseBody
	public Json dataGridMobile(EFResource resource, PageFilter ph) {
		Json j = new Json();
//		spring自动将参数注入到ph对象中
		Grid grid = dataGrid(resource, ph);
		j.setSuccess(true);
		j.setObj(grid);
		return j;
	}

	@RequestMapping("/delete")
	@ResponseBody
//	ids为jsonArray
	public Json delete(String ids,HttpServletRequest request) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				long id = jsonIds.getLong(i);
//				获取dataId
				EFResource r = resourceService.get(id);
				long dataId = r.getDataId();
//				删除资源
				resourceService.delete(id,request);
//				更新服务天数
				dataService.updateServerDates(dataId);
			}
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			j.setMsg(e1.getMessage());
		}catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/detailPage")
	public String detailPage(HttpServletRequest request, Long id) {
		EFResource r = resourceService.get(id);
		request.setAttribute("resource", r); 
		return "/admin/xiaxiang/resourceDetail";
	}

}
