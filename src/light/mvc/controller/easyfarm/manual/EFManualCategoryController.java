package light.mvc.controller.easyfarm.manual;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.model.easyfarm.manual.TEFmanualCategory;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.manual.EFmanualCategory;
import light.mvc.service.easyfarm.manual.EFManualCategoryServiceI;
import light.mvc.service.easyfarm.news.EFCategoryServiceI;
import light.mvc.utils.MD5Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/manualCategory")
public class EFManualCategoryController extends BaseController{

	@Autowired 
	private EFManualCategoryServiceI categoryService;
	
	@RequestMapping("/getPage")
	public String categoryPage() {
		return "/admin/easyfarm/manual/category";
	}
	
	@RequestMapping("/dataGrid")
	@ResponseBody
	public  Grid dataGrid(EFmanualCategory category, PageFilter ph) {
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(categoryService.dataGrid(category, ph));
		grid.setTotal(categoryService.count(category, ph));
		return grid;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Json delete(String ids,HttpServletRequest request) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				long id = jsonIds.getLong(i);
				categoryService.delete(id,request);
			}
			
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/addPage")
	public String addPage(HttpServletRequest request) {
		List<TEFmanualCategory> category =categoryService.getAllData();
		request.setAttribute("category", category);
		return "/admin/easyfarm/manual/categoryAdd";
	}
	
	@RequestMapping("/add")
	@ResponseBody
	public Json add(EFmanualCategory nobj) {
		System.out.println("hello");
		Json j = new Json();
			try {
				categoryService.add(nobj);
				j.setSuccess(true);
				j.setMsg("添加成功！");
			} catch (Exception e) {
				j.setMsg(e.getMessage());
			}
		
		return j;
	}
	
}