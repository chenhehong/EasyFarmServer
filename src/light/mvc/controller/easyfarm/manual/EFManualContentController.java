package light.mvc.controller.easyfarm.manual;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import light.mvc.controller.base.BaseController;
import light.mvc.model.easyfarm.manual.TEFmanualCategory;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.manual.EFmanualContent;
import light.mvc.service.easyfarm.manual.EFManualCategoryServiceI;
import light.mvc.service.easyfarm.manual.EFManualContentServiceI;
@Controller
@RequestMapping("/manualContent")
public class EFManualContentController extends BaseController{

	@Autowired 
	private EFManualContentServiceI contentService;
	
	@Autowired 
	private EFManualCategoryServiceI categoryService;
	
	@RequestMapping("/getPage")
	public String get_news_page() {
		
		return "/admin/easyfarm/manual/manual";
	}
	
	@RequestMapping("/dataGrid")
	@ResponseBody
	public  Grid data_grid(EFmanualContent content, PageFilter ph) {
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(contentService.dataGrid(content, ph));
		grid.setTotal(contentService.count(content, ph));
		return grid;
	}
	
	@RequestMapping("/delete")
	@ResponseBody
	public Json delete(String ids,HttpServletRequest request) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				long id = jsonIds.getLong(i);
				contentService.delete(id,request);
			}
			
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/addPage")
	public String addPage(HttpServletRequest request) {
		List<TEFmanualCategory> category =categoryService.getAllData();
		request.setAttribute("category", category);
		return "/admin/easyfarm/manual/manualAdd";
	}
	

	
	@RequestMapping("/add")
	@ResponseBody
	public Json add(EFmanualContent nobj) {
		//System.out.println("addnews");
		Json j = new Json();
			try {
				contentService.add(nobj);
				j.setSuccess(true);
				j.setMsg("添加成功！");
			} catch (Exception e) {
				j.setMsg(e.getMessage());
			}
		
		return j;
	}
}
