package light.mvc.controller.easyfarm;

import javax.servlet.http.HttpServletRequest;

import light.mvc.controller.base.BaseController;
import light.mvc.framework.constant.GlobalConstant;
import light.mvc.pageModel.base.Grid;
import light.mvc.pageModel.base.Json;
import light.mvc.pageModel.base.PageFilter;
import light.mvc.pageModel.easyfarm.EFApplication;
import light.mvc.service.easyfarm.EFDataFinalServiceI;

import org.json.JSONArray;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/xxdata_final")
public class EFDataFinalController extends BaseController{

	@Autowired 
	private EFDataFinalServiceI dataService;
	
	@RequestMapping("/manager")
	public String manager(HttpServletRequest request) {
		request.setAttribute("statusList",GlobalConstant.statuslist);
		return "/admin/xiaxiang/dataFinal";
	}

	@RequestMapping("/dataGrid")
	@ResponseBody
	public Grid dataGrid(EFApplication data, PageFilter ph) {
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(dataService.dataGrid(data, ph));
		grid.setTotal(dataService.count(data, ph));
		return grid;
	}
	
	/**
	 * 手机端
	 */
	@RequestMapping("/dataGridMobile")
	@ResponseBody
	public Json dataGridMobile(EFApplication data, PageFilter ph) {
		Json j = new Json();
//		spring自动将参数注入到ph对象中
		Grid grid = dataGrid(data, ph);
		j.setSuccess(true);
		j.setObj(grid);
		return j;
	}
	
	/**
	 * 手机端请求的状态为通过与不通过的数据
	 */
	@RequestMapping("/manageDataGridMobile")
	@ResponseBody
	public Json manageDataGridMobile(EFApplication data, PageFilter ph) {
		Json j = new Json();
//		spring自动将参数注入到ph对象中
		Grid grid = new Grid();
		grid.setRows(dataService.manageDataGrid(data, ph));
		grid.setTotal(dataService.manageCount(data, ph));
		j.setSuccess(true);
		j.setObj(grid);
		return j;
	}

	@RequestMapping("/delete")
	@ResponseBody
//	ids为jsonArray
	public Json delete(String ids) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				EFApplication data = dataService.get(jsonIds.getLong(i));
				if (data.getServerDates()!=0) {
					j.setMsg("id号为"+jsonIds.getLong(i)+"的申请已经进行下乡服务了，你无权删除该数据！");
					j.setSuccess(false);
					return j;
				}
				dataService.delete(jsonIds.getLong(i));
			}
			j.setMsg("删除成功！");
			j.setSuccess(true);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}
	
	@RequestMapping("/editPage")
	public String editPage(HttpServletRequest request, Long id) {
		EFApplication d = dataService.get(id);
		request.setAttribute("data", d); 
		request.setAttribute("statusList",GlobalConstant.statuslist);
		return "/admin/xiaxiang/dataFinalEdit";
	}

	@RequestMapping("/edit")
	@ResponseBody
	public Json edit(EFApplication data) {
		Json j = new Json();
		EFApplication d = dataService.get(data.getId());
		if (d.getServerDates()!=0) {
			j.setMsg("非法操作");
			j.setSuccess(false);
			return j;
		}
		try {
				dataService.edit(data);
				j.setSuccess(true);
				j.setMsg("编辑成功！");
			} catch (Exception e) {
				j.setMsg(e.getMessage());
			}
		return j;
	}
	
	@RequestMapping("/verifyPage")
	public String verifyPage() {
		return "/admin/xiaxiang/dataFinalVerify";
	}

	@RequestMapping("/pass")
	@ResponseBody
	public Json pass(String ids) {
		JSONArray jsonIds;
		Json j = new Json();
		try {
			jsonIds = new JSONArray(ids);
			for (int i = 0; i < jsonIds.length(); i++) {
				dataService.pass(jsonIds.getLong(i));
			}
			j.setMsg("通过审核成功！");
			j.setSuccess(true);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}catch (Exception e) {
			j.setMsg(e.getMessage());
		}
		return j;
	}

}
