package light.mvc.model.easyfarm;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import light.mvc.model.base.IdEntity;
import light.mvc.model.sys.Torganization;
import light.mvc.model.sys.Tresource;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "ef_villageservice")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TEFapplication extends IdEntity implements java.io.Serializable {

	private String applyMan; 
	private Date applyDate;
	private String businessPersonnel;
	private String businessPhone;
	private String businessArea;
	private String businessAddress;
	private Date businessDate;
	private String businessMatter;
	private Date returnDate;
	private String businessLinkMan;
	private String businessLinkPhone;
	private int departmentCheck;
	private int finalCheck;
	private String departmentOpinion;
	private String finalOpinion;
	private int serverDates;
	private TEFexpert member;
	private Set<TEFresource> xxresources = new HashSet<TEFresource>(0);
	

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "data")
	public Set<TEFresource> getResources() {
		return xxresources;
	}

	public void setResources(Set<TEFresource> resources) {
		this.xxresources = resources;
	}


	public TEFapplication() {
		super();
	}

	public TEFapplication(String applyMan, Date applyDate, String businessPersonnel,
			String businessPhone, String businessArea, String businessAddress,
			Date businessDate, String businessMatter, Date returnDate,
			String businessLinkMan, String businessLinkPhone,
			int departmentCheck, int finalCheck, int serverDates,
			TEFexpert member) {
		super();
		this.applyMan = applyMan;
		this.applyDate = applyDate;
		this.businessPersonnel = businessPersonnel;
		this.businessPhone = businessPhone;
		this.businessArea = businessArea;
		this.businessAddress = businessAddress;
		this.businessDate = businessDate;
		this.businessMatter = businessMatter;
		this.returnDate = returnDate;
		this.businessLinkMan = businessLinkMan;
		this.businessLinkPhone = businessLinkPhone;
		this.departmentCheck = departmentCheck;
		this.finalCheck = finalCheck;
		this.serverDates = serverDates;
		this.member = member;
	}

	public String getApplyMan() {
		return applyMan;
	}

	public void setApplyMan(String applyMan) {
		this.applyMan = applyMan;
	}

	@Temporal(TemporalType.DATE)
	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	public String getBusinessPersonnel() {
		return businessPersonnel;
	}

	public void setBusinessPersonnel(String businessPersonnel) {
		this.businessPersonnel = businessPersonnel;
	}

	public String getBusinessPhone() {
		return businessPhone;
	}

	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	public String getBusinessArea() {
		return businessArea;
	}

	public void setBusinessArea(String businessArea) {
		this.businessArea = businessArea;
	}

	public String getBusinessAddress() {
		return businessAddress;
	}

	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	@Temporal(TemporalType.DATE)
	public Date getBusinessDate() {
		return businessDate;
	}

	public void setBusinessDate(Date businessDate) {
		this.businessDate = businessDate;
	}

	public String getBusinessMatter() {
		return businessMatter;
	}

	public void setBusinessMatter(String businessMatter) {
		this.businessMatter = businessMatter;
	}

	@Temporal(TemporalType.DATE)
	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getBusinessLinkMan() {
		return businessLinkMan;
	}

	public void setBusinessLinkMan(String businessLinkMan) {
		this.businessLinkMan = businessLinkMan;
	}

	public String getBusinessLinkPhone() {
		return businessLinkPhone;
	}

	public void setBusinessLinkPhone(String businessLinkPhone) {
		this.businessLinkPhone = businessLinkPhone;
	}

	public int getDepartmentCheck() {
		return departmentCheck;
	}

	public void setDepartmentCheck(int departmentCheck) {
		this.departmentCheck = departmentCheck;
	}

	public int getFinalCheck() {
		return finalCheck;
	}

	public void setFinalCheck(int finalCheck) {
		this.finalCheck = finalCheck;
	}

	public String getDepartmentOpinion() {
		return departmentOpinion;
	}

	public void setDepartmentOpinion(String departmentOpinion) {
		this.departmentOpinion = departmentOpinion;
	}

	public String getFinalOpinion() {
		return finalOpinion;
	}

	public void setFinalOpinion(String finalOpinion) {
		this.finalOpinion = finalOpinion;
	}
	
	public int getServerDates() {
		return serverDates;
	}

	public void setServerDates(int serverDates) {
		this.serverDates = serverDates;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "expertId")
	public TEFexpert getMember() {
		return member;
	}

	public void setMember(TEFexpert member) {
		this.member = member;
	}

}