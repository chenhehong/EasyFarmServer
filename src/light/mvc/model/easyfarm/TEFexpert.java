package light.mvc.model.easyfarm;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import light.mvc.model.base.IdEntity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "ef_expert")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TEFexpert extends IdEntity implements java.io.Serializable{
	
	private String loginName;
	private String password;
	private String realName;
	private String department;
	private String phoneNumber;
	private String techType;
	private int status;
	private Set<TEFapplication> villageServices = new HashSet<TEFapplication>(0);
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "member")
	public Set<TEFapplication> getVillageServices() {
		return villageServices;
	}
	
	public void setVillageServices(Set<TEFapplication> villageServices) {
		this.villageServices = villageServices;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getTechType() {
		return techType;
	}

	public void setTechType(String techType) {
		this.techType = techType;
	}

	public int getStatus() { 
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
}
