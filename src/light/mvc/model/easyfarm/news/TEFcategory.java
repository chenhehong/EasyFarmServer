package light.mvc.model.easyfarm.news;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import light.mvc.model.base.IdEntity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


@Entity
@Table(name = "ef_news_category")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TEFcategory extends IdEntity implements java.io.Serializable{
	
	private String categoryName;
	private String categoryDesc;
	private TEFcategory data;
	
	
	public TEFcategory()
	{
		super();
	}
	
	public TEFcategory(String categoryName,String categoryDesc,TEFcategory data)
	{
		super();
		this.categoryDesc=categoryDesc;
		this.categoryName=categoryName;
		this.data=data;
	}
	
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryDesc() {
		return categoryDesc;
	}
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}
	
	

}
