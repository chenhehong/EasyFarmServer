package light.mvc.model.easyfarm.news;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import light.mvc.model.base.IdEntity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "ef_news_content")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TEFnews extends IdEntity implements java.io.Serializable{

	private String title;
	private String content;
	private Date createTime;
	private String author;
	private int userId;
	private int categoryId;
	private TEFnews data;
	
	
	public TEFnews()
	{
		super();
	}
	
	public TEFnews(String title, String content,Date createTime, String author,int userId, String category, int categoryId, String remark, TEFnews data)
	{
		super();
		this.title=title;
		this.content=content;
		this.createTime=createTime;
		this.author=author;
		this.userId=userId;
		this.categoryId=categoryId;
		this.data=data;
	}
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
}
