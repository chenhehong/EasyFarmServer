package light.mvc.model.easyfarm.agribusiness;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import light.mvc.model.base.IdEntity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "ef_agribusiness_info")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TEFAgriBusiness extends IdEntity implements java.io.Serializable {
	
	private Date createTime;
	private Long autherId;
	private String autherType;
	private String agribusinessSite;
	private String agribusinessIntroduce;
	private int status;
	private String companyName;
	private String address;
	
	private TEFAgriBusiness data;
	public TEFAgriBusiness() {
		super();
		// TODO Auto-generated constructor stub
	}



	public TEFAgriBusiness(Date createTime, Long autherId, String autherType,
			String agribusinessSite, String agribusinessIntroduce, int status,
			String companyName, String address, TEFAgriBusiness data) {
		super();
		this.createTime = createTime;
		this.autherId = autherId;
		this.autherType = autherType;
		this.agribusinessSite = agribusinessSite;
		this.agribusinessIntroduce = agribusinessIntroduce;
		this.status = status;
		this.companyName = companyName;
		this.address = address;
		this.data = data;
	}



	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Long getAutherId() {
		return autherId;
	}
	public void setAutherId(Long autherId) {
		this.autherId = autherId;
	}
	public String getAutherType() {
		return autherType;
	}
	public void setAutherType(String autherType) {
		this.autherType = autherType;
	}
	public String getAgribusinessSite() {
		return agribusinessSite;
	}
	public void setAgribusinessSite(String agribusinessSite) {
		this.agribusinessSite = agribusinessSite;
	}
	public String getAgribusinessIntroduce() {
		return agribusinessIntroduce;
	}
	public void setAgribusinessIntroduce(String agribusinessIntroduce) {
		this.agribusinessIntroduce = agribusinessIntroduce;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	

}
