package light.mvc.model.easyfarm.manual;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import light.mvc.model.base.IdEntity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


@Entity
@Table(name = "ef_manual_content")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TEFmanualContent extends IdEntity implements java.io.Serializable{
	
	private Long cid;
	private String content;
	private String title;
	private TEFmanualContent data;
	
	public TEFmanualContent() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TEFmanualContent(Long cid, String content, String title,
			TEFmanualContent data) {
		super();
		this.cid = cid;
		this.content = content;
		this.title = title;
		this.data = data;
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	
	

}
