package light.mvc.model.easyfarm.manual;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import light.mvc.model.base.IdEntity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;


@Entity
@Table(name = "ef_manual_category")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TEFmanualCategory extends IdEntity implements java.io.Serializable{
	
	private Long pid;
	private String name;
	private TEFmanualCategory data;
	
	public TEFmanualCategory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TEFmanualCategory(Long pid, String name, TEFmanualCategory data) {
		super();
		this.pid = pid;
		this.name = name;
		this.data = data;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	

}
