package light.mvc.model.easyfarm;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import light.mvc.model.base.IdEntity;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "xx_resource")
@DynamicInsert(true)
@DynamicUpdate(true)
public class TEFresource extends IdEntity implements java.io.Serializable {

	private String description; 
	private String address;
	private Date creatTime;
	private String type;
	private String filepath;
	private String thumbFilepath;
	private TEFapplication data;

	public TEFresource() {
		super();
	}

	public TEFresource(String description, String address, Date creatTime,
			String type, String filepath,String thumbFilepath, TEFapplication data) {
		super();
		this.description = description;
		this.address = address;
		this.creatTime = creatTime;
		this.type = type;
		this.filepath = filepath;
		this.thumbFilepath = thumbFilepath;
		this.data = data;
	}

	public String getThumbFilepath() {
		return thumbFilepath;
	}

	public void setThumbFilepath(String thumbFilepath) {
		this.thumbFilepath = thumbFilepath;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatTime() {
		return creatTime;
	}

	public void setCreatTime(Date creatTime) {
		this.creatTime = creatTime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dataId")
	public TEFapplication getData() {
		return data;
	}

	public void setData(TEFapplication data) {
		this.data = data;
	}

}