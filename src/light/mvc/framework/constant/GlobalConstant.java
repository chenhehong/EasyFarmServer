package light.mvc.framework.constant;

import java.util.HashMap;
import java.util.Map;

public class GlobalConstant {
	
//	专家和普通用户类型字符串
	public static final String EXPERT = "expert";//专家
	public static final String MEMBER = "member";//普通用户

//	系统各种类型的用户session字段设置
	public static final String SESSION_INFO_ADMIN = "sessionInfoAdmin";//管理员session常量
	public static final String SESSION_INFO_EXPERT = "sessionInfoExpert";//专家用户session常量
	public static final String SESSION_INFO_MEMBER = "sessionInfoMember";//专家用户session常量

	public static final Integer ENABLE = 0; // 启用
	public static final Integer DISABLE = 1; // 禁用
	
	
	public static final Integer DEFAULT = 0; // 默认 
	public static final Integer NOT_DEFAULT = 1; // 非默认
	
	public static final Map sexlist = new HashMap(){{ put("0", "男");  put("1", "女");} };
	public static final Map statelist = new HashMap(){{ put("0", "启用");  put("1", "停用");} };
	public static final Map statuslist = new HashMap(){{ put("0", "待审核");  put("1", "通过"); put("-1", "不通过");} };
	
	public static final String uploadFolder = "/uploadfile";
	public static final String uploadThumbFolder = "/uploadfile/thumb";
	
//	xx_resource表中资源type的类别定义
	public static final String PICTURETYPE = "picture";
    public static final String VIDEOTYPE = "video";
	
}
