<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	$(function() {

		$('#expertAddForm').form({
			url : '${pageContext.request.contextPath}/expert/add',
			onSubmit : function() {
				progressLoad();
				var isValid = $(this).form('validate');
				if (!isValid) {
					progressClose();
				}
				return isValid;
			},
			success : function(result) {
				progressClose();
				result = $.parseJSON(result);
				if (result.success) {
					parent.$.modalDialog.openner_dataGrid.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_dataGrid这个对象，是因为expert.jsp页面预定义好了
					parent.$.modalDialog.handler.dialog('close');
				} else {
					parent.$.messager.alert('错误', result.msg, 'error');
				}
			}
		});
	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false" >
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 3px;" >
		<form id="expertAddForm" method="post">
			<table class="grid">
				<tr>
					<td>登录名称</td>
					<td><input name="loginName" type="text" placeholder="请输入登录名称" class="easyui-validatebox span2" data-options="required:true" value=""></td>
				</tr>
				<tr>
					<td>登录密码</td>
					<td><input name="password" type="text" placeholder="请输入密码" class="easyui-validatebox span2" data-options="required:true" value=""></td>
				</tr>
				<tr>
					<td>真实姓名</td>
					<td><input name="realName" type="text"></td>
				</tr>
				<tr>
					<td>单位</td>
					<td><input name="department" type="text"></td>
				</tr>
				<tr>
					<td>联系电话</td>
					<td><input name="phoneNumber" type="text"></td>
				</tr>
				<tr>
					<td>技术类型</td>
					<td><input name="techType" type="text"></td>
				</tr>
			</table>
		</form>
	</div>
</div>