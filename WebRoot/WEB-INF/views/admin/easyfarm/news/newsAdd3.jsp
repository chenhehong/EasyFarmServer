<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>

<head>
<jsp:include page="../../../inc.jsp"></jsp:include>
<script>
//$('#xheditor').xheditor();
</script>
<meta http-equiv="X-UA-Compatible" content="edge" />
<c:if test="${fn:contains(sessionInfoAdmin.resourceList, '/news/newsDetail')}">
	<script type="text/javascript">
		$.canDetail = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfoAdmin.resourceList, '/news/edit')}">
	<script type="text/javascript">
		$.canEdit = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfoAdmin.resourceList, '/news/delete')}">
	<script type="text/javascript">
		$.canDelete = true;
	</script>
</c:if>
<title>新闻管理</title>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<div class="unit">
		<textarea class="xheditor" name="description" rows="6" cols="100">full(完全) 默认方式</textarea>
	</div>
</body>
</html>
