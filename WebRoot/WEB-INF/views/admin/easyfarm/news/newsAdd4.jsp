<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<jsp:include page="../../../inc.jsp"></jsp:include>
<script type="text/javascript">
	

	$(function() {
		
		$('#newsAddForm').form({
			url : '${ctx}/news/add2',
			onSubmit : function() {
				progressLoad();
				var isValid = $(this).form('validate');
				if (!isValid) {
					progressClose();
				}
				return isValid;
			},
			success : function(result) {
				progressClose();
				result = $.parseJSON(result);
				if (result.success) {
					parent.$.modalDialog.openner_dataGrid.datagrid('reload');//
					parent.$.modalDialog.handler.dialog('close');
				}else{
					parent.$.messager.alert('提示', result.msg, 'warning');
				}
			}
		});
		
	});
</script>
<div style="padding: 3px;">
	<form id="newsAddForm" method="post">
	<div class="table">
		<table class="grid">
			<tr>
				<td>新闻题目</td>
				<td><input name="title" type="text" placeholder="请输入新闻题目" class="easyui-validatebox" data-options="required:true" style="width: 100%; height: 29px;" ></td>
			</tr>
			<tr>
				<td>所属类别</td>
				<td><input name="categoryId" type="text" placeholder="请输入栏目名称" class="easyui-validatebox" data-options="required:true" style="width: 100%; height: 29px;" ></td>
			</tr>
			  
			   		 
		
		</table>
	</div>
	<div>
	  <textarea class="xheditor" name="content" rows="6" cols="100" style="width: 100%;">请输入内容</textarea>
	</div>
	</form>
</div>
