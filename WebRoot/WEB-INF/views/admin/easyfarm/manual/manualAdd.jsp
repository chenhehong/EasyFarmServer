<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<jsp:include page="../../../inc.jsp"></jsp:include>
<script type="text/javascript">
	

	$(function() {
		
		$('#newsAddForm').form({
			url : '${ctx}/manualContent/add',
			onSubmit : function() {
				progressLoad();
				var isValid = $(this).form('validate');
				if (!isValid) {
					progressClose();
				}
				return isValid;
			},
			success : function(result) {
				progressClose();
				result = $.parseJSON(result);
				if (result.success) {
					parent.$.messager.alert('提示', result.msg, 'warning');
				}else{
					parent.$.messager.alert('提示', result.msg, 'warning');
				}
			}
		});
		
	});
	
		function addFun() {
					$('#newsAddForm').submit();				
	}
</script>
<div style="padding: 3px;">
	<form id="newsAddForm" method="post">
	<div class="table">
		<table class="grid">
			<tr>
				<td>新闻题目</td>
				<td><input name="title" type="text" placeholder="请输入新闻题目" class="easyui-validatebox" data-options="required:true" style="width: 80%; height: 29px;" ></td>
			</tr>
			
			<tr>
				<td>所属类别</td>
				<td><select name="cid" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
				<c:if test="${!empty category}">
				  <c:forEach items="${category}" var="n">   
				    <option value="${n.id}">${n.name}</option>
				  </c:forEach>
				
				</c:if>
				</select></td>
			</tr>
						<tr>
			<td>发布时间</td>
				<td><input name="createTime" type="text"  class="easyui-validatebox" readonly="readonly" value=" <%=new java.util.Date().toLocaleString( )%>" data-options="required:true" style="width: 80%; height: 29px;" ></td>
			</tr>
			<tr>
			<td>发布人</td>
			<td><input name="author" type="text"  class="easyui-validatebox" readonly="readonly" value="${sessionInfoAdmin.loginname}"  style=" height: 29px;" ></td>
			
			</tr>
			<td>发布人工号</td>
			<td>
			<input name="userId" type="text"  class="easyui-validatebox" readonly="readonly" value="${sessionInfoAdmin.id}"  style=" height: 29px;" >
			</td>
			</tr>
			  
			   		 
		
		</table>
	</div>
	<div>
	  <textarea class="xheditor" name="content" rows="6" cols="100" style="width: 100%;">请输入内容</textarea>
	</div>
	</form>
</div>
	<div id="toolbar">	    
			<a onclick="addFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon_add'">发布</a>

	</div>