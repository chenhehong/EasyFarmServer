<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<script type="text/javascript">
	$(function() {
		
		$('#dataEditForm').form({
			url : '${ctx}/xxdata_department/edit',
			onSubmit : function() {
				progressLoad();
				var isValid = $(this).form('validate');
				if (!isValid) {
					progressClose();
				}
				return isValid;
			},
			success : function(result) {
				progressClose();
				result = $.parseJSON(result);
				if (result.success) {
					parent.$.modalDialog.openner_dataGrid.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_dataGrid这个对象，是因为member.jsp页面预定义好了
					parent.$.modalDialog.openner_dataGrid.datagrid('clearSelections');
					parent.$.modalDialog.handler.dialog('close');
				} else {
					parent.$.messager.alert('错误', result.msg, 'error');
				}
			}
		});
	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 3px;">
		<form id="dataEditForm" method="post">
			<table class="grid">
				<tr>
					<td>申请用户</td>
					<td><input name="id" type="hidden"  value="${data.id}">
					<input name="memberLoginName" type="text" value="${data.memberLoginName}" readonly="readonly"></td>
					<td>申请人姓名</td>
					<td><input name="applyMan" type="text" value="${data.applyMan}" readonly="readonly"></td>
				</tr>
				<tr>
					<td>申请日期</td>
					<td><input name="applyDate" type="text" value="${data.applyDate}" readonly="readonly"></td>
					<td>下乡人员姓名</td>
					<td><input name="businessPersonnel" type="text" value="${data.businessPersonnel}" readonly="readonly"></td>
				</tr>
				<tr>
					<td>下乡人员联系手机</td>
					<td><input name="businessPhone" type="text" value="${data.businessPhone}" readonly="readonly"></td>
					<td>下乡地点</td>
					<td colspan="4"><textarea name="businessArea" rows="" cols="" readonly="readonly">${data.businessArea}${data.businessAddress}</textarea></td>
				</tr>
				<tr>
					<td>下乡事由</td>
					<td colspan="8"><textarea name="businessMatter" rows="" cols="50" readonly="readonly">${data.businessMatter}</textarea></td>
				</tr>
				<tr>
					<td>服务联系人</td>
					<td><input name="businessLinkMan" type="text" value="${data.businessLinkMan}" readonly="readonly"></td>
					<td>服务联系人电话</td>
					<td><input name="businessLinkPhone" type="text" value="${data.businessLinkPhone}" readonly="readonly"></td>
				</tr>
				<tr>
					<td>下乡时间</td>
					<td><input name="businessDate" value="${data.businessDate}" type="text" class="easyui-datebox" readonly="readonly"></td>
					<td>返回时间</td>
					<td><input name="returnDate" type="text" value="${data.returnDate}" class="easyui-datebox" readonly="readonly"></td>
				</tr>
				<tr>
					<td>部门审核状态</td>
					<td><select name="departmentCheck" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'" <c:if test="${data.finalCheck != 0 }">readonly="readonly"</c:if>>
						<c:forEach items="${statusList}" var="statusList">
							<option value="${statusList.key}" <c:if test="${statusList.key == data.departmentCheck}">selected="selected"</c:if>>${statusList.value}</option>
						</c:forEach>
					</select></td>
					<td>分管领导审核状态</td>
					<td><select name="finalCheck" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'" readonly="readonly">
						<c:forEach items="${statusList}" var="statusList">
							<option value="${statusList.key}" <c:if test="${statusList.key == data.finalCheck}">selected="selected"</c:if>>${statusList.value}</option>
						</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>部门审核意见</td>
					<td colspan="8"><textarea name="departmentOpinion" rows="" cols="50" <c:if test="${data.finalCheck != 0 }">readonly="readonly"</c:if>>${data.departmentOpinion}</textarea></td>
				</tr>
				<tr>
					<td>分管领导审核意见</td>
					<td colspan="8"><textarea name="finalOpinion" rows="" cols="50" readonly="readonly">${data.finalOpinion}</textarea></td>
				</tr>
			</table>
		</form>
	</div>
</div>