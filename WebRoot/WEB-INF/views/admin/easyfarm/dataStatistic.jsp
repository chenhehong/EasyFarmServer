<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../../inc.jsp"></jsp:include>
<meta http-equiv="X-UA-Compatible" content="edge" />
<c:if test="${fn:contains(sessionInfo.resourceList, '/xxdata_statistic/detailPage')}">
	<script type="text/javascript">
		$.dataCanDetail = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfo.resourceList, '/xxdata_statistic/delete')}">
	<script type="text/javascript">
		$.dataCanDelete = true;
	</script>
</c:if>

<c:if test="${fn:contains(sessionInfo.resourceList, '/xxresource/detailPage')}">
	<script type="text/javascript">
		$.resourceCanDetail = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfo.resourceList, '/xxresource/delete')}">
	<script type="text/javascript">
		$.resourceCanDelete = true;
	</script>
</c:if>
<title>下乡服务统计</title>
	<script type="text/javascript">
	var dataGridData;
	var dataGridResource;
	$(function() {
		dataGridData = $('#dataGridData').datagrid({
			url : '${ctx}' + '/xxdata_statistic/dataGrid',
			striped : true,
			rownumbers : false,//是否显示行号
			pagination : true,
			singleSelect : false,//允许选择多行
			idField : 'id',
			sortName : 'id',
			sortOrder : 'asc',
			pageSize : 50,
			pageList : [ 10, 20, 30, 40, 50, 100, 200, 300, 400, 500 ],
			frozenColumns : [ [ {
				width : '30',
				field : 'ck',
				checkbox : true
			}, {
				width : '80',
				title : '下乡人员',
				field : 'businessPersonnel',
				sortable : true
			} , {
				width : '120',
				title : '下乡区域',
				field : 'businessArea',
				sortable : true
			}, {
				width : '150',
				title : '下乡时间',
				field : 'businessDate',
				sortable : true
			}, {
				width : '150',
				title : '返回时间',
				field : 'returnDate',
				sortable : true
			}, {
				field : 'action',
				title : '操作',
				width : 160,
				formatter : function(value, row, index) {
					var str = '&nbsp;';
					str += $.formatString('<a href="javascript:void(0)" onclick="showResourceFun(\'{0}\');" >查看资源</a>', row.id);
					str += '&nbsp;&nbsp;|&nbsp;&nbsp;';
					if ($.dataCanDetail) {
						str += $.formatString('<a href="javascript:void(0)" onclick="dataDetailFun(\'{0}\');" >详情</a>', row.id);
					}
					str += '&nbsp;&nbsp;|&nbsp;&nbsp;';
					if ($.dataCanDelete) {
						str += $.formatString('<a href="javascript:void(0)" onclick="dataDeleteFun({0});" >删除</a>', row.id);
					}
					return str;
				}
			} ] ],
			toolbar : '#datatoolbar'
		});
		doDataSearch();
		dataGridResource = $('#dataGridResource').datagrid({
			url : '${ctx}' + '/xxresource/dataGrid',
			striped : true,
			rownumbers : false,//是否显示行号
			pagination : true,
			singleSelect : false,//允许选择多行
			idField : 'id',
			sortName : 'id',
			sortOrder : 'asc',
			pageSize : 50,
			pageList : [ 10, 20, 30, 40, 50, 100, 200, 300, 400, 500 ],
			frozenColumns : [ [ {
				width : '30',
				field : 'ck',
				checkbox : true
			},{
				width : '160',
				title : '上传时间',
				field : 'creatTime',
				sortable : true
			} ,{
				field : 'show',
				title : '查看资源',
				width : 120,
				formatter : function(value, row, index) {
					var str = '&nbsp;';
					str += $.formatString('<a href="${ctx}{0}" target="_blank"><img src="${ctx}{1}" width="45"/></a>', row.filepath,row.thumbFilepath);
					return str;
				}
			}, {
				field : 'action',
				title : '操作',
				width : 100,
				formatter : function(value, row, index) {
					var str = '&nbsp;';
					if ($.resourceCanDetail) {
						str += $.formatString('<a href="javascript:void(0)" onclick="resourceDetailFun(\'{0}\');" >详情</a>', row.id);
					}
					str += '&nbsp;&nbsp;|&nbsp;&nbsp;';
					if ($.resourceCanDelete) {
						str += $.formatString('<a href="javascript:void(0)" onclick="resourceDeleteFun({0});" >删除</a>', row.id);
					}
					return str;
				}
			} ] ],
			toolbar : '#resourcetoolbar'
		});
	});
	
	function dataDeleteFun(id) {
		/* 定义json数组ids */
		var ids = [];
		if (id == undefined) {//点击删除按钮才会触发这个 
			var rows = dataGridData.datagrid('getSelections');
			if (rows.length==0) {  
				parent.$.messager.alert("提示", "请选择要删除的行！", "info");  
	            return;  
	        }else{ 
				for(i=0;i<rows.length;i++)
					ids.push(rows[i].id);
	        }
		} else {//点击操作里面的删除图标会触发这个
			dataGridData.datagrid('unselectAll').datagrid('uncheckAll');
			ids.push(id);
		}
		var jsonIds = JSON.stringify(ids);
		parent.$.messager.confirm('询问', '您是否要删除当前选中的用户？', function(b) {
			if (b) {
					progressLoad();
					$.post('${ctx}/xxdata_statistic/delete', {
						ids : jsonIds
					}, function(result) {
						if (result.success) {
							//清除datagrid之前的选择，防止下次删除时重复删除
							dataGridData.datagrid('clearSelections');
							parent.$.messager.alert('提示', result.msg, 'info');
							dataGridData.datagrid('reload');
							dataGridResource.datagrid('reload');
						}
						progressClose();
					}, 'JSON');
			}
		});
	}
	
	function dataExportExcelFun(id) {
		/* 定义json数组ids */
		var ids = [];
		if (id == undefined) {//点击删除按钮才会触发这个 
			var rows = dataGridData.datagrid('getSelections');
			if (rows.length==0) {  
				parent.$.messager.alert("提示", "请选择要导出的行！", "info");  
	            return;  
	        }else{ 
				for(i=0;i<rows.length;i++)
					ids.push(rows[i].id);
	        }
		} else {//点击操作里面的删除图标会触发这个
			dataGridData.datagrid('unselectAll').datagrid('uncheckAll');
			ids.push(id);
		}
		var jsonIds = JSON.stringify(ids);
		parent.$.messager.confirm('询问', '您是否要导出当前选中的下乡数据到Excel文件？', function(b) {
			if (b) {
				openWindowWithPost('${ctx}/xxdata_statistic/exportExcel','导出Excel','ids',jsonIds);
				dataGridData.datagrid('unselectAll').datagrid('uncheckAll');
			}
		});
	}
	
	//已post的方式打开新窗口
	function openWindowWithPost(url,name,keys,values) 
	{ 
		var newWindow = window.open(url, name); 
		if (!newWindow) 
			return false; 
		var html = ""; 
		html += "<html><head></head><body><form id='formid' method='post' action='" + url + "'>"; 
		if (keys && values) 
		{ 
			html += "<input type='hidden' name='" + keys + "' value='" + values + "'/>";
		} 
		html += "</form><script type='text/javascript'>document.getElementById('formid').submit();"; 
		html += "<\/script></body></html>".toString().replace(/^.+?\*|\\(?=\/)|\*.+?$/gi, ""); 
		newWindow.document.write(html); 
		return newWindow; 
	} 
	
	function dataDetailFun(id) {
		if (id == undefined) {
			var rows = dataGridData.datagrid('getSelections');
			id = rows[0].id;
		} else {
			dataGridData.datagrid('unselectAll').datagrid('uncheckAll');
		}
		parent.$.modalDialog({
			title : '详情',
			width : 500,
			height : 600,
			href : '${ctx}/xxdata_statistic/detailPage?id=' + id
		});
	}
	
	function showResourceFun(id){
		dataGridResource.datagrid('reload', {
            dataId: id
        });
	}
	
	function resourceDeleteFun(id) {
		/* 定义json数组ids */
		var ids = [];
		if (id == undefined) {//点击删除按钮才会触发这个 
			var rows = dataGridResource.datagrid('getSelections');
			if (rows.length==0) {  
				parent.$.messager.alert("提示", "请选择要删除的行！", "info");  
	            return;  
	        }else{ 
				for(i=0;i<rows.length;i++)
					ids.push(rows[i].id);
	        }
		} else {//点击操作里面的删除图标会触发这个
			dataGridResource.datagrid('unselectAll').datagrid('uncheckAll');
			ids.push(id);
		}
		var jsonIds = JSON.stringify(ids);
		parent.$.messager.confirm('询问', '您是否要删除当前选中的资源？', function(b) {
			if (b) {
					progressLoad();
					$.post('${ctx}/xxresource/delete', {
						ids : jsonIds
					}, function(result) {
						if (result.success) {
							//清除datagrid之前的选择，防止下次删除时重复删除
							dataGridResource.datagrid('clearSelections');
							parent.$.messager.alert('提示', result.msg, 'info');
							dataGridResource.datagrid('reload');
						}
						progressClose();
					}, 'JSON');
			}
		});
	}
	
	function resourceDetailFun(id) {
		if (id == undefined) {
			var rows = dataGridResource.datagrid('getSelections');
			id = rows[0].id;
		} else {
			dataGridResource.datagrid('unselectAll').datagrid('uncheckAll');
		}
		parent.$.modalDialog({
			title : '详情',
			width : 500,
			height : 300,
			href : '${ctx}/xxresource/detailPage?id=' + id
		});
	}
	
	function doDataSearch(){
		var selectApplyMan = null;
		var selectBusinessArea = null;
		var businessDate = null;
		var returnDate = null;
		if($('#selectApplyMan').val()!='1'){
			selectApplyMan = $('#selectApplyMan').val();
		}
		if($('#selectBusinessArea').val()!='1'){
			selectBusinessArea = $('#selectBusinessArea').val();
		}
		businessDate = $('#businessDate').datebox('getValue');;
		returnDate = $('#returnDate').datebox('getValue');;
	    $('#dataGridData').datagrid('load',{
	        applyMan: selectApplyMan,
	        businessArea:selectBusinessArea,
	        businessDate:businessDate,
	        returnDate:returnDate
	    }); 
	}
	
	</script>
	
	<style type="text/css">
		 .pagination-info
		 {
		 	float:none;
		 }
	</style>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'west',split:true,border:false" title="统计列表" style="width:700px;">
		<table id="dataGridData" data-options="fit:true,border:false"></table>
	</div>
	<div id="datatoolbar" style="display: none;">
		<div style="margin-left:10px;margin-bottom:7px;margin-top:7px">
			<span>申请人:</span> 
			<select id="selectApplyMan">
				<c:forEach items="${dataApplyManList}" var="dataApplyManList">
					<option value="${dataApplyManList.key}" <c:if test="${dataApplyManList.key == '1'}">selected="selected"</c:if>>${dataApplyManList.value}</option>
				</c:forEach>
			</select>
			<span>&nbsp;&nbsp;&nbsp;下乡区域:</span> 
			<select id="selectBusinessArea">
				<c:forEach items="${dataBusinessAreaList}" var="dataBusinessAreaList">
					<option value="${dataBusinessAreaList.key}" <c:if test="${dataBusinessAreaList.key == '1'}">selected="selected"</c:if>>${dataBusinessAreaList.value}</option>
				</c:forEach>
			</select>
			<span>&nbsp;&nbsp;&nbsp;下乡时间:</span>
			从<input name="businessDate" id="businessDate" value="${preYearDate}" class="easyui-datebox" style="width:100px"> 
			&nbsp;到&nbsp;<input name="returnDate" id="returnDate" value="${today}" class="easyui-datebox" style="width:100px">
			<a href="#" class="easyui-linkbutton" data-options="plain:true" onclick="doDataSearch()">搜索</a>  
		</div>
		<div>
			<c:if test="${fn:contains(sessionInfo.resourceList, '/xxdata_statistic/delete')}">
				<a onclick="dataDeleteFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon_del'">删除</a>
			</c:if>
			<c:if test="${fn:contains(sessionInfo.resourceList, '/xxdata_statistic/exportExcel')}">
				<a onclick="dataExportExcelFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon_resource'">导出Excel</a>
			</c:if>
		</div>
	</div>
	<div data-options="region:'center',fit:true,border:false" title="资源列表">
		<table id="dataGridResource" data-options="fit:true,border:false"></table>
	</div>
	<div id="resourcetoolbar" style="display: none;">
		<c:if test="${fn:contains(sessionInfo.resourceList, '/xxresource/delete')}">
			<a onclick="resourceDeleteFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon_del'">删除</a>
		</c:if>
	</div>
</body>
</html>