<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../../inc.jsp"></jsp:include>
<meta http-equiv="X-UA-Compatible" content="edge" />
<c:if test="${fn:contains(sessionInfo.resourceList, '/xxdata_department/edit')}">
	<script type="text/javascript">
		$.canEdit = true;
	</script>
</c:if>
<c:if test="${fn:contains(sessionInfo.resourceList, '/xxdata_department/delete')}">
	<script type="text/javascript">
		$.canDelete = true;
	</script>
</c:if>
<title>下乡申请管理</title>
	<script type="text/javascript">
	var dataGrid;
	$(function() {
		dataGrid = $('#dataGrid').datagrid({
			url : '${ctx}' + '/xxdata_department/dataGrid',
			striped : true,
			rownumbers : false,//是否显示行号
			pagination : true,
			singleSelect : false,//允许选择多行
			idField : 'id',
			sortName : 'id',
			sortOrder : 'asc',
			pageSize : 50,
			pageList : [ 10, 20, 30, 40, 50, 100, 200, 300, 400, 500 ],
			frozenColumns : [ [ {
				width : '30',
				field : 'ck',
				checkbox : true
			},{
				width : '30',
				title : 'id',
				field : 'id',
				sortable : true
			}, {
				width : '80',
				title : '申请用户',
				field : 'memberLoginName',
				sortable : true
			} , {
				width : '80',
				title : '申请人姓名',
				field : 'applyMan',
				sortable : true
			}, {
				width : '150',
				title : '申请时间',
				field : 'applyDate',
				sortable : true
			}, {
				width : '80',
				title : '联系电话',
				field : 'businessPhone',
				sortable : true
			}, {
				width : '150',
				title : '下乡时间',
				field : 'businessDate',
				sortable : true
			}, {
				width : '150',
				title : '返回时间',
				field : 'returnDate',
				sortable : true
			}, {
				width : '120',
				title : '部门审核状态',
				field : 'departmentCheck',
				sortable : true,
				formatter : function(value, row, index) {
					switch (value) {
					case 0:
						return '待审核';
					case 1:
						return '通过';
					case -1:
						return '不通过';	
					}
				}
			},{
				width : '120',
				title : '分管领导审核状态',
				field : 'finalCheck',
				sortable : true,
				formatter : function(value, row, index) {
					switch (value) {
					case 0:
						return '待审核';
					case 1:
						return '通过';
					case -1:
						return '不通过';	
					}
				}
			},{
				field : 'action',
				title : '操作',
				width : 120,
				formatter : function(value, row, index) {
					var str = '&nbsp;';
					if(row.finalCheck==0){
						if ($.canEdit) {
							str += $.formatString('<a href="javascript:void(0)" onclick="editFun(\'{0}\');" >编辑</a>', row.id);
						}
						str += '&nbsp;&nbsp;|&nbsp;&nbsp;';
						if ($.canDelete) {
							str += $.formatString('<a href="javascript:void(0)" onclick="deleteFun({0});" >删除</a>', row.id);
						}
					}else{
						str += $.formatString('<a href="javascript:void(0)" onclick="detailFun(\'{0}\');" >详情</a>', row.id);
					}
					return str;
				}
			} ] ],
			toolbar : '#toolbar'
		});
		doSearch();
	});
	
	function deleteFun(id) {
		/* 定义json数组ids */
		var ids = [];
		if (id == undefined) {//点击删除按钮才会触发这个 
			var rows = dataGrid.datagrid('getSelections');
			if (rows.length==0) {  
				parent.$.messager.alert("提示", "请选择要删除的行！", "info");  
	            return;  
	        }else{ 
				for(i=0;i<rows.length;i++)
					ids.push(rows[i].id);
	        }
		} else {//点击操作里面的删除图标会触发这个
			dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
			ids.push(id);
		}
		var jsonIds = JSON.stringify(ids);
		parent.$.messager.confirm('询问', '您是否要删除当前选中的用户？', function(b) {
			if (b) {
					progressLoad();
					$.post('${ctx}/xxdata_department/delete', {
						ids : jsonIds
					}, function(result) {
						if (result.success) {
							//清除datagrid之前的选择，防止下次删除时重复删除
							dataGrid.datagrid('clearSelections');
							parent.$.messager.alert('操作成功', result.msg, 'info');
							dataGrid.datagrid('reload');
						}else{
							//清除datagrid之前的选择，防止下次删除时重复删除
							dataGrid.datagrid('clearSelections');
							parent.$.messager.alert('操作失败', result.msg, 'info');
							dataGrid.datagrid('reload');
						}
						progressClose();
					}, 'JSON');
			}
		});
	}
	
	function editFun(id) {
		if (id == undefined) {
			var rows = dataGrid.datagrid('getSelections');
			id = rows[0].id;
		} else {
			dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
		}
		parent.$.modalDialog({
			title : '编辑',
			width : 600,
			height : 500,
			href : '${ctx}/xxdata_department/editPage?id=' + id,
			buttons : [ {
				text : '编辑',
				handler : function() {
					parent.$.modalDialog.openner_dataGrid = dataGrid;//因为添加成功之后，需要刷新这个dataGrid，所以先预定义好
					var f = parent.$.modalDialog.handler.find('#dataEditForm');
					f.submit();
				}
			} ]
		});
	}
	
	function detailFun(id) {
		if (id == undefined) {
			var rows = dataGrid.datagrid('getSelections');
			id = rows[0].id;
		} else {
			dataGrid.datagrid('unselectAll').datagrid('uncheckAll');
		}
		parent.$.modalDialog({
			title : '详情',
			width : 600,
			height : 500,
			href : '${ctx}/xxdata_department/editPage?id=' + id,
			buttons : [ {
				text : '详情',
				handler : function() {
					parent.$.modalDialog.openner_dataGrid = dataGrid;//因为添加成功之后，需要刷新这个dataGrid，所以先预定义好
					var f = parent.$.modalDialog.handler.find('#dataEditForm');
					f.submit();
				}
			} ]
		});
	}
	
	function doSearch(){
	    $('#dataGrid').datagrid('load', {    
	        departmentCheck: $('#selectStatue').val(),
	    }); 
	}
	</script>
</head>
<body class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',fit:true,border:false">
		<table id="dataGrid" data-options="fit:true,border:false"></table>
	</div>
	<div id="toolbar" style="display: none;">
		<div style="margin-left:10px;margin-bottom:7px;margin-top:7px">
			<span>审核状态:</span> 
			<select id="selectStatue">
				<c:forEach items="${statusList}" var="statusList">
					<option value="${statusList.key}" <c:if test="${statusList.key == 1}">selected="selected"</c:if>>${statusList.value}</option>
				</c:forEach>
			</select>
			<a href="#" class="easyui-linkbutton" data-options="plain:true" onclick="doSearch()">搜索</a>  
		</div>
		<div>
			<c:if test="${fn:contains(sessionInfo.resourceList, '/xxdata_department/delete')}">
				<a onclick="deleteFun();" href="javascript:void(0);" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon_del'">删除</a>
			</c:if>
		</div>
	</div>
</body>
</html>