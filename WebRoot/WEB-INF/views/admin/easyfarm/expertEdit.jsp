<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<script type="text/javascript">
	$(function() {
		
		$('#expertEditForm').form({
			url : '${ctx}/expert/edit',
			onSubmit : function() {
				progressLoad();
				var isValid = $(this).form('validate');
				if (!isValid) {
					progressClose();
				}
				return isValid;
			},
			success : function(result) {
				progressClose();
				result = $.parseJSON(result);
				if (result.success) {
					parent.$.modalDialog.openner_dataGrid.datagrid('reload');//之所以能在这里调用到parent.$.modalDialog.openner_dataGrid这个对象，是因为expert.jsp页面预定义好了
					parent.$.modalDialog.openner_dataGrid.datagrid('clearSelections');
					parent.$.modalDialog.handler.dialog('close');
				} else {
					parent.$.messager.alert('错误', result.msg, 'error');
				}
			}
		});
	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 3px;">
		<form id="expertEditForm" method="post">
			<div class="light-info" style="overflow: hidden;padding: 3px;">
				<div>密码不修改请留空。</div>
			</div>
			<table class="grid">
				<tr>
					<td>登录名称</td>
					<td><input name="id" type="hidden"  value="${expert.id}">
					<input name="loginName" type="text" placeholder="请输入登录名称" class="easyui-validatebox" data-options="required:true" value="${expert.loginName}"></td>
				</tr>
				<tr>
					<td>密码</td>
					<td><input name="password" type="text"></td>
				</tr>
				<tr>
					<td>真实姓名</td>
					<td><input name="realName" type="text" value="${expert.realName}"></td>
				</tr>
				<tr>
					<td>单位</td>
					<td><input name="department" type="text" value="${expert.department}"></td>
				</tr>
				<tr>
					<td>联系电话</td>
					<td><input name="phoneNumber" type="text" value="${expert.phoneNumber}"></td>
				</tr>
				<tr>
					<td>技术类型</td>
					<td><input name="techType" type="text" value="${expert.techType}"></td>
				</tr>
				<tr>
					<td>状态</td>
					<td><select name="status" class="easyui-combobox" data-options="width:140,height:29,editable:false,panelHeight:'auto'">
						<c:forEach items="${statusList}" var="statusList">
							<option value="${statusList.key}" <c:if test="${statusList.key == expert.status}">selected="selected"</c:if>>${statusList.value}</option>
						</c:forEach>
					</select></td>
				</tr>
			</table>
		</form>
	</div>
</div>