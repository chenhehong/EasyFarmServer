<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<script type="text/javascript">
	$(function() {
		
		$('#resourceDetailForm').form({
			url : '${ctx}/xxresource/detail'
		});
	});
</script>
<div class="easyui-layout" data-options="fit:true,border:false">
	<div data-options="region:'center',border:false" title="" style="overflow: hidden;padding: 3px;">
		<form id="resourceDetailForm" method="post">
			<table class="grid">
				<tr>
					<td>下乡事由</td>
					<td colspan="4"><textarea name="dataBusinessMatter" rows="" cols="" readonly="readonly">${resource.dataBusinessMatter}</textarea></td>
				</tr>
				<tr>
					<td>上传时间</td>
					<td><input name="creatTime" type="text" value="${resource.creatTime}" readonly="readonly"></td>
				</tr>
				<tr>
					<td>拍摄地址</td>
					<td><input name="address" type="text" value="${resource.address}" readonly="readonly"></td>
				</tr>
				<tr>
					<td>资源描述</td>
					<td colspan="4"><textarea name="description" rows="" cols="" readonly="readonly">${resource.description}</textarea></td>
				</tr>
				<tr>
					<td>查看资源</td>
					<td><a href="${ctx}${resource.filepath}" target="_blank"><img src="${ctx}${resource.thumbFilepath}" width="45"/></a></td>
				</tr>
			</table>
		</form>
	</div>
</div>