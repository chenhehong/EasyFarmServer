<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <jsp:include page="../inc.jsp"></jsp:include>
    <base href="<%=basePath%>">
    <jsp:include page="./inc-head.jsp"></jsp:include>
	<link href="${ctx}/style/index/commpany.css" rel="stylesheet" media="screen">
  </head>
  
<body class="home blog">
 <!--start 登录注册浮动栏 -->
    <jsp:include page="./wpthemedemobar.jsp"></jsp:include>
 <!-- end 登录注册浮动栏 -->

   <!-- start:头部导航 -->
  <jsp:include page="./header.jsp"></jsp:include>
  <!-- end:头部导航-->

<!--head:所在位置-->
<div id="page_muen_nav">  <b>您现在所在的位置：</b><a href="${ctx}/front/web/base/index">首页</a> <a> &gt; </a><a href="${ctx}/front/web/base/getCommpanyList"> 农企信息列表</a></div>
<!--end:所在位置-->

<!--head:内容项目-->
<div id="content">

<!--head:左侧内容-->
<div class="left_mian"> 
<!--head:查询农企-->
<div class="widget">
<ul>
<li>
	<div class="searchbox">
		<input type="text" id="input_search" autocomplete="off" value="" placeholder="搜索你感兴趣的公司...">
		<button id="btn_search"><i class="search_icon"></i></button>
	</div>
</li>
<li>
	<a href="${ctx}/front/web/expert/getApplyCommpany" target="_blank"><button class="applycommpany" id="btn_applycommpany">发布农企信息</button></a>
</li>
</ul>
</div>
<!--end:查询农企-->

<!-- head:农企信息 -->
<div class="widget" id="widget_company">
   <div class="widge_hd">
   <span>
       <b>待审核农企信息</b>
   </span>
   </div>
<ul>
	<li><a href="./">常德恒凤渔业有限公司</a><p>地址： 湖南省常德市鼎城区蒿子港镇长安村八组<br/>成立日期： 2016/5/31<br/>状态：<span style="color: red;">未审核</span></p></li>
	<li><a href="./">建德市大同镇锦惠农庄</a><p>主营： 热食类食品制售（小型餐饮）（依法须经批准的项目，经相关部门批准后方可开展经营活动）<br/>成立日期： 2016/6/16<br/>状态：<span style="color: red;">未审核</span></p></li>
</ul>
</div>
<!-- end:农企信息 -->

<!--head:扁平图标导航-->
 <jsp:include page="./navigation.jsp"></jsp:include>
<!--head:扁平图标导航-->

</div>
<!--end:左侧内容-->


<!--head:右侧内容-->
<div class="right_mian">

<!-- head:农企信息 -->
  <div class="widget companylist" id="widget_companylist">
<ul>
	<li><a href="./">常德恒凤渔业有限公司</a><br/><p>地址： 湖南省常德市鼎城区蒿子港镇长安村八组<br/>网址： www.2333.com</p></li>
	<li><a href="./">建德市大同镇锦惠农庄</a><br/><p>主营： 热食类食品制售（小型餐饮）（依法须经批准的项目，经相关部门批准后方可开展经营活动）<br/>网址： www.23112.com</p></li>
	<li><a href="./">兰溪市野狐山寨休闲农庄</a><br/><p>主营： 饭馆；中餐类制售；农家乐；不含凉菜；不含裱花蛋糕；不含生食海产品<br/>网址： www.atkd.com.cn</p></li>
	<li><a href="./">诸暨市枫桥良禹农庄</a><br/><p>主营： 食品经营（具体经营项目以许可证或批准文件核定的为准） 养殖销售：淡水产 <br/>网址： www.rerr3.com</p></li>
	<li><a href="./">诸暨市岭北晋坤农庄</a><br/><p>主营： 食品经营（具体经营项目以许可证或批准文件核定的为准） 种植：蔬菜、水果<br/>网址： www.2877.com</p></li>
</ul>
<!--head:列表分页-->
<div class="list_paginator">
	<a data-page="1" href="javascript:void(0);" class="paginator_current">1</a> 
	<a data-page="2" href="javascript:void(0);">2</a>
</div>
<!--end:列表分页-->
</div>
<!-- end:农企信息 -->

</div>
<!--end:右侧内容-->
</div>  
<!--end:内容项目-->

<!--head:底部信息-->
  <jsp:include page="./footer.jsp"></jsp:include>
<!--end:底部信息-->
<script type="text/javascript" src="${ctx}/jslib/index/thickbox.js"></script>
<script type="text/javascript" src="${ctx}/jslib/index/themepark.js"></script>
</body>
</html>
