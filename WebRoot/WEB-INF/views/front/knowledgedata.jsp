<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <jsp:include page="../inc.jsp"></jsp:include>
    <base href="<%=basePath%>">
    <jsp:include page="./inc-head.jsp"></jsp:include>
 <link href="${ctx}/style/index/knowledgedata.css" rel="stylesheet" media="screen">

  </head>
  
<body class="home blog">
 <!--start 登录注册浮动栏 -->
    <jsp:include page="./wpthemedemobar.jsp"></jsp:include>
 <!-- end 登录注册浮动栏 -->

   <!-- start:头部导航 -->
  <jsp:include page="./header.jsp"></jsp:include>
  <!-- end:头部导航-->

<!--head:所在位置-->
<div id="page_muen_nav">  <b>您现在所在的位置：</b><a href="${ctx}/front/web/base/index">首页</a> <a> &gt; </a><a href="${ctx}/front/web/base/getKnowledgeData"> 农技知识库</a></div>
<!--end:所在位置-->

<!--head:内容项目-->
<div class="knowledgecontent">

<div class="widget">
<ul>
<li>
	<div class="searchbox">
		<input type="text" id="input_search" autocomplete="off" value="" placeholder="搜索目录关键字...">
		<button id="btn_search"><i class="search_icon"></i></button>
	</div>
</li>
</ul>
</div> 

<!--head:农技目录-->
<div class="tree">
 <ul>
  <li>
  <a href="#" tree="root">知识库目录</a>
  
   <ul>
    <li>
     <a href="#">测土配方</a>
     <ul>
	   <li>
       <a href="#">施肥种类</a>
	   
			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
      <li>
       <a href="#">施肥技术</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">施肥原则</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">建议</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
     </ul>
    </li>
	
	
	    <li>
     <a href="#">四季农事</a>
     <ul>
	   <li>
       <a href="#">春季</a>
	   
			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
      <li>
       <a href="#">夏季</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">秋季</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">冬季</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
     </ul>
    </li>
	
	
		    <li>
     <a href="#">病虫诊断</a>
     <ul>
	   <li>
       <a href="#">常见病虫</a>
	   
			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
      <li>
       <a href="#">病虫防治</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">农药种类</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">农药用法</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
     </ul>
    </li>
	
	
		    <li>
     <a href="#">作物栽培</a>
     <ul>
	   <li>
       <a href="#">技术</a>
	   
			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
      <li>
       <a href="#">建议</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">原则</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">类别</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
     </ul>
    </li>
	
	
		    <li>
     <a href="#">天气灾害</a>
     <ul>
	   <li>
       <a href="#">暴雨</a>
	   
			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
      <li>
       <a href="#">霜冻</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">干旱</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">台风</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
     </ul>
    </li>
	
	
		    <li>
     <a href="#">肥药喷施</a>
     <ul>
	   <li>
       <a href="#">施肥的重要性</a>
	   
			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
      <li>
       <a href="#">施肥技术</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">农药喷施技巧</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
	  <li>
       <a href="#">化肥品牌</a>
	   
	   			<ul>
			   <li>
			   <a href="#">水稻</a>
			  </li>
			  <li>
			   <a href="#">蔬菜</a>
			  </li>
			  <li>
			   <a href="#">水果</a>
			  </li>
			  <li>
			   <a href="#">玉米</a>
			  </li>
			 </ul>
	   
      </li>
     </ul>
    </li>
  
   </ul>
  </li> 
 </ul>
</div>
<!--end:农技目录-->
<script>
	$(document).ready(function(){
		opentreeul($(".tree ul ul"), false);
		$(".tree a").click(function(){
			var nextul = $(this).next("ul");
			var otherchild1 = $(this).parent("li").prevAll("li");
			var otherchild2 = $(this).parent("li").nextAll("li");
			var newstatus = $(nextul).css("display") == "none" ? true : false;
			opentreeul(nextul, newstatus);
			opentreeli(otherchild1, newstatus ? false : true);
			opentreeli(otherchild2, newstatus ? false : true);
			$(".tree a").each(function(){
				$(this).removeClass("selectedchild");
			});
			if($(this).attr("tree") != "root"){
				$(this).addClass("selectedchild");
			}
			return false;
		});
	});
	
	function opentreeul(nodeul, flag){
		if($(nodeul).length > 0){
			if(flag){
				$(nodeul).css("display", "");
				$(nodeul).children("li").each(function(){
					opentreeli(this, true);
				});
			}
			else{
				$(nodeul).children("li").each(function(){
					opentreeli(this, false);
				});
				$(nodeul).css("display", "none");
			}
		}
	}
	
	function opentreeli(nodeli, flag){
		if($(nodeli).length > 0){
			if(flag) {
				$(nodeli).css("display", "");
			}
			else{
				$(nodeli).children("ul").each(function(){
					opentreeul(this, false);
				});
				$(nodeli).css("display", "none");
			}
		}
	}
</script>

<!-- head:知识库内容 -->
  <div class="widget knowledgelist">
<ul>
	<!--head:一条内容-->
	<li>
		<a class="knowledge_title" href="./">水稻种子药剂处理技术意见</a>
		<div class="knowledge_expandable">
			播种前进行种子处理是有效控制种传病害及苗期病虫危害的重要措施，也是推进农药减量控害的有效途径。水稻通过种子带菌传播的主要病害有恶苗病等。为做好今年水稻种子药剂处理，现提出以下技术意见。各地要高度重视水稻种子处理工作，狠抓宣传发动和技术指导…
			<a class="toggle-expand">显示全部</a>
		</div>
		<div class="knowledge_content" style="display: none;">
播种前进行种子处理是有效控制种传病害及苗期病虫危害的重要措施，也是推进农药减量控害的有效途径。水稻通过种子带菌传播的主要病害有恶苗病等。为做好今年水稻种子药剂处理，现提出以下技术意见。各地要高度重视水稻种子处理工作，狠抓宣传发动和技术指导。

一、药剂浸种：

用25%氰烯菌酯悬浮剂3毫升加水6公斤（2000倍液），可浸种子4-5公斤。具体操作方法为：将药剂倒入已称好的清水中搅拌均匀，然后倒入种子拌匀，去除空秕谷，浸足60小时后捞起，即可催芽。

二、药剂拌种：

种子经浸种催芽后，每4-5公斤稻种用35%丁硫克百威15-20克加10%吡虫啉30克（或70%吡虫啉5克）拌种，闷30分钟后即可播种。可有效控制苗期灰飞虱、稻蓟马等为害。

三、注意事项：

1、浸种时，操作顺序上应先配准药液，再放入种子，并严格掌握浸种时间。浸种容器要放在室内，避免雨淋日晒。

2、拌种时，芽谷应为湿润状态，过湿稍后再拌，过干应喷水湿润后再拌。

3、催芽播种后，一定要保持秧板潮湿。如遇连续高温干旱天气，应及时灌跑马水（夜灌日排），防止高温干旱造成灼芽或失水回根。

4、浸种后药液要妥善处理，防止随意倾倒残液污染水源。
		</div>
		<div class="knowledge_actions">
			<button class="knowledge_collapse" style="display: none;"><i></i>收起</button>
		</div>
	</li>
	<!--end:一条内容-->
	<!--head:一条内容-->
	<li>
		<a class="knowledge_title" href="./">当前高温季节蔬菜生产管理建议</a>
		<div class="knowledge_expandable">
			当前正值夏秋高温伏旱季节，天气以晴热高温少雨为主，且台风等灾害性天气频发，给蔬菜生产带来不利影响。当前，设施蔬菜正处于换茬、休耕、育苗及秋蔬菜适时定植阶段，而露地蔬菜如豇豆、苋菜、空心菜...
			<a class="toggle-expand">显示全部</a>
		</div>
		<div class="knowledge_content" style="display: none;">
当前正值夏秋高温伏旱季节，天气以晴热高温少雨为主，且台风等灾害性天气频发，给蔬菜生产带来不利影响。当前，设施蔬菜正处于换茬、休耕、育苗及秋蔬菜适时定植阶段，而露地蔬菜如豇豆、苋菜、空心菜、鲜食玉米、茄子、丝瓜和葫芦等处于生产采收旺季。高温使得在田蔬菜瓜果易受灼伤，西甜瓜、茄果类等秋菜育苗难度大，成苗率低，移栽定植期推迟，速生叶菜尤其是青梗菜生产也受影响，需加强管理，做好以下5条技术措施。

1、选种耐热耐旱蔬菜品种，培育壮苗。针对高温晴热天气，当前季节选择种植快菜、苋菜、空心菜等速生叶菜及四季葱、中国南瓜等抗热性较好的蔬菜品种，以保障市场供应。快菜可播种‘早熟5号’、‘双耐’等品种，抗热青梗菜推荐‘金品4号’、‘尚美205’、‘华冠’、‘夏绿’等品种。近期宜适时采用穴盘基质育苗方法，播种培育松花菜、包心菜、瓠瓜、黄瓜、番茄、辣椒、茄子等秋季蔬菜秧苗，西甜瓜、黄瓜等应适当缩短苗龄，重点是防止秧苗徒长，宜早上浇水。

2、采取“两网一膜”覆盖措施，提高抗灾能力。夏秋高温季节宜采用两网一膜（即遮阳网、防虫网和棚顶薄膜覆盖）。遮阳网具有遮强光、降高温、防暴雨冲击等多种功能，能有效地改善夏秋高温季节作物生长环境，避免植株暴晒、日灼危害。在快菜、苋菜、芹菜、葱等叶菜上使用遮阳网覆盖栽培，可适当提早播种上市，提高蔬菜品质，播种后也可采用浮面覆盖，出苗后晚揭早盖。在秋季松花菜、包心菜、番茄、辣椒、茄子等秧苗上使用遮阳网时，需注意通风降温，防止秧苗徒长。设施蔬菜生产可覆盖大棚薄膜进行避雨栽培，避免暴雨冲刷，减少病害发生。有条件的采取全网或四周覆盖防虫网，可实现物理措施隔离害虫，减少病虫危害。

3、应用微蓄微灌及水肥一体化技术 节水节肥。菜田早晚灌跑马水，推荐在高温干旱的早晨、夜间应用微灌、微喷技术，能起到增湿、降温作用，改善田间小气候，调节土壤水、肥、气、热状况，有效减轻干旱对蔬菜生产的危害，结合肥水一体化技术，兼具省工、省力、节水、节肥、增效等优点，可大幅改善蔬菜商品性。

4、中耕除草、畦面覆盖、及时追肥。勤中耕除草松土，保持菜地土壤疏松，防止杂草，减少土壤水分蒸发，防止杂草争肥争水，促进发苗。按照“薄肥勤施”原则，一般在早上或傍晚时施用，有条件的采用滴灌追肥，喷施叶面肥。

5、综合防治病虫害，确保产品安全。夏季跳甲、烟粉虱、潜叶蝇、螟虫和蛾类等蔬菜害虫高发，近年来红蜘蛛、蓟马等在豇豆、葱等作物上危害加重。要坚持防重于治原则，在蔬菜落令后及时清理残株，高温闷棚，翻耕土壤，筑田埂灌水，撒施生石灰、石灰氮、棉隆、荣宝等消毒处理；也可采取菜稻轮作或与葱蒜类轮作等方式，改良土壤，杀灭土传病虫害。综合运用杀虫灯、昆虫性诱剂、防虫网、色板等生物物理措施，防治小菜蛾、斜纹夜蛾、黄条跳甲等害虫危害。要合理用药，注意用药安全，做好安全间隔期，确保蔬菜产品质量安全。
		</div>
		<div class="knowledge_actions">
			<button class="knowledge_collapse" style="display: none;"><i></i>收起</button>
		</div>
	</li>
	<!--end:一条内容-->
	<!--head:一条内容-->
	<li>
		<a class="knowledge_title" href="./">梅雨季节水果产业抗灾防灾技术措施</a>
		<div class="knowledge_expandable">
			<img src="${ctx}/uploadfile/p3.png">
			1.抓紧采收。抓紧采摘已成熟的早熟桃、杨梅等水果并及时销售，来不及销售的要进冷库保存。

2.开沟排水、清理污泥杂物。暴雨后应及时清理疏通排水沟渠，尽早排出果园积水，使土壤恢复干爽，避免果园积水造成死根烂根。对水淹较重、短期内又不能排除的...
        <a class="toggle-expand">显示全部</a>
		</div>
		<div class="knowledge_content" style="display: none;">
1.抓紧采收。抓紧采摘已成熟的早熟桃、杨梅等水果并及时销售，来不及销售的要进冷库保存。

2.开沟排水、清理污泥杂物。暴雨后应及时清理疏通排水沟渠，尽早排出果园积水，使土壤恢复干爽，避免果园积水造成死根烂根。对水淹较重、短期内又不能排除的果园，可用抽水机排除，确保果树根系对氧气的正常需要。对被风吹斜、被水冲倒的果树，应及时扶正培土，并把枝条和叶面上的杂物和污泥清除，最好用高压喷雾器喷清水清洗枝叶，以确保树体正常生理活动的进行。

3.及时清园。对已结果的果园应全面清除果园内的落果、落叶，拉到果园外挖坑深埋；剪截断枝残枝，集中焚烧，从而减少病菌传染。对果袋有破损而果实无伤的，将破损的袋子去除，喷一遍杀虫杀菌剂，待药液干后再重新套袋。

4.加强灾后修剪。暴雨过后应根据树体萎蔫、落叶情况，从基部剪除受害严重的枝条，或进行回缩、短截，促发新的枝梢。

5.及时恢复设施。应及时整修被毁坏的葡萄避雨棚等果园基础设施，保证果树的正常生产。

6.根外追肥。树体受涝后根系受损，吸收肥水的能力较弱，不宜立即根施肥料，可选用0.1-0.2%的磷酸二氢钾、0.3%尿素，以及叶面肥等进行根外追肥。每隔5天左右一次，连喷2次。

7.适时松土。水淹后果园土壤板结，容易引起根系缺氧，待果园表土基本干燥时，及时进行松土。

8.病害防治。加强病虫害防治。暴雨过后，可能会给果树造成一些伤口，易遭病菌侵染，并且受灾后树势减弱，抗病力降低，各种病害容易大量发生和蔓延。因此要对全园果树和土壤进行一次全面的消毒处理，避免病害蔓延。对于近期即将采收的水果，不能使用安全间隔期较长的农药，以保证果品质量安全。
		</div>
		<div class="knowledge_actions">
			<button class="knowledge_collapse" style="display: none;"><i></i>收起</button>
		</div>
	</li>
	<!--end:一条内容-->
	<!--head:一条内容-->
	<li>
		<a class="knowledge_title" href="./">玉米合理追肥促大穗</a>
		<div class="knowledge_expandable">
			<img src="${ctx}/uploadfile/p4.png">
			　玉米植株从拔节孕穗到抽穗开花期，生长速度快，吸收的养分多，是吸肥的关键时期；开花授粉后，吸收的养分多，但吸收速度减缓。就玉米而言，苗期吸氮量占总量的9.7%，中期占78.4%，后期占11.9%，因此，夏玉米高产必须抓好后期施肥，早孕穗，促大穗，夺高产…
			<a class="toggle-expand">显示全部</a>
		</div>
		<div class="knowledge_content" style="display: none;">
　玉米植株从拔节孕穗到抽穗开花期，生长速度快，吸收的养分多，是吸肥的关键时期；开花授粉后，吸收的养分多，但吸收速度减缓。就玉米而言，苗期吸氮量占总量的9.7%，中期占78.4%，后期占11.9%，因此，夏玉米高产必须抓好后期施肥，早孕穗，促大穗，夺高产。

　　施好拔节肥

　　夏玉米全展叶7叶期，正是施用拔节肥的最适期，这时正值玉米叶片大量展开，茎秆迅速伸长的时期，需肥量较大，一般到全展叶9叶期时，土壤养分就难以满足玉米生长的需要了。此时应抓住玉米7～9叶期拔节前后开穴追肥，苗壮的适当迟施、少施，地瘦苗弱的早施、重施，一般每亩施碳铵15～20公斤，人畜粪肥500～750公斤。

　　足水孕穗

　　孕穗期是需水最多的时期，一般玉米孕穗期植株生长的需水量约占全生育期总需水量的27%~38%，此期土壤水分应保持在田间持水量的70%~75%，以水调肥，满足玉米生长发育的肥水需要。

　　重施穗肥

　　追施穗肥不但可以提高光合作用效率，促进穗位叶片及其上下几片叶的生长，而且能使叶片延长寿命，促进籽粒饱满。穗肥施用时间与玉米品种及地力情况有关。一般来说，早熟品种在可见叶15~16片或展开叶8~10片时；中晚熟品种在可见叶15~17片或展开叶11~12片时施用为宜，基肥充足、生长旺盛的地块，可适当迟施穗肥，以全田有少量植株开始抽雄时为宜；基肥不足的瘦地生长势差，穗肥则宜早施。穗肥应选用速效性氮肥，一般每亩穴施或条施碳铵25~35公斤，若选用尿素，每亩施7.5~10公斤就可以了。
		</div>
		<div class="knowledge_actions">
			<button class="knowledge_collapse" style="display: none;"><i></i>收起</button>
		</div>
	</li>
	<!--end:一条内容-->
</ul>
<script>
//展开问题
$(".knowledge_expandable").click(function(){
	$(this).hide();
	$(this).nextAll(".knowledge_content").show();
	$(this).nextAll(".knowledge_actions").find(".knowledge_collapse").show();
});

//折叠问题
$(".knowledge_collapse").click(function(){
	$(this).hide();
	$(this).parent(".knowledge_actions").prevAll(".knowledge_content").hide();
	$(this).parent(".knowledge_actions").prevAll(".knowledge_expandable").show();
});
</script>
<!--head:列表分页-->
<div class="list_paginator">
	<a data-page="1" href="javascript:void(0);" class="paginator_current">1</a> 
	<a data-page="2" href="javascript:void(0);">2</a>
</div>
<!--end:列表分页-->
</div>
<!-- end:知识库内容 -->

</div>  
<!--end:内容项目-->

<!--head:底部信息-->
 <jsp:include page="./footer.jsp"></jsp:include>
<!--end:底部信息-->

<script type="text/javascript" src="${ctx}/jslib/index/thickbox.js"></script>
<script type="text/javascript" src="${ctx}/jslib/index/themepark.js"></script>
</body>
</html>
