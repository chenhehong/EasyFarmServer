<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <jsp:include page="../inc.jsp"></jsp:include>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>农技推广综合管理平台</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=9" >
	<link rel="shortcut icon" href="${ctx}/style/images/ico.ico" type="image/x-icon" />
	<link href="${ctx}/style/index/themepark.css" rel="stylesheet" media="screen">
	<link href="${ctx}/style/index/default.css" rel="stylesheet" media="screen">
	<link href="${ctx}/style/index/detailinputunit.css" rel="stylesheet" media="screen">
	<link href="${ctx}/style/index/countryside.css" rel="stylesheet" media="screen">
	<link href="${ctx}/jslib/xheditor/xheditor_skin/nostyle/ui.css" rel="stylesheet" media="screen">
  
 	<style>
 		.question_toolbar{position: relative; width: 100%; box-shadow: 0 1px 0 rgba(255,255,255,0.6);}
		.question_gradient{position: relative; margin-right: 100px; height: 30px; border: 1px solid #ccc; border-right: none; border-bottom-color: #aaa; border-bottom-left-radius: 3px; -webkit-border-bottom-left-radius: 3px;}
		.question_button{outline:none;cursor: pointer; font-family: "Helvetica Neue",Helvetica,Arial,sans-serif; position: absolute; right: 0; top: 0; height: 32px; width: 100px; text-align: center; text-shadow: 0 1px 0 #fff; color: #555; font-size: 18px; font-weight: bold; border: 1px solid #ccc; border-bottom-color: #aaa; border-bottom-right-radius: 3px;  -webkit-border-bottom-right-radius: 3px; background-color: #e6e6e6; background-repeat: no-repeat; background-image: linear-gradient(#fcfcfc, #fcfcfc 25%, #e6e6e6); transition: all .15s linear; box-shadow: inset 0 0 1px #fff;}
		.question_title{border: 1px solid #045bb2; border-radius: 4px; box-shadow: inset 0 1px 3px rgba(0,0,0,.2),0 1px 0 rgba(255,255,255,.1); outline: 0;padding: 5px; width: 300px; font-size: 15px;}
 	</style>

</head>
<body class="page page-id-2 page-parent page-template page-template-aboutus page-template-aboutus-php">

  <!--start 登录注册浮动栏 -->
    <jsp:include page="./wpthemedemobar.jsp"></jsp:include>
 <!-- end 登录注册浮动栏 -->

   <!-- start:头部导航 -->
  <jsp:include page="./header.jsp"></jsp:include>
  <!-- end:头部导航-->

<!--head:所在位置-->
<div id="page_muen_nav">  <b>您现在所在的位置：</b><a href="${ctx}/front/web/base/index">首页</a> <a> &gt; </a><a href="${ctx}/front/web/base/getQuestionList"> 在线交流</a><a> &gt; </a><a href="#"> 提问</a></div>
<!--end:所在位置-->

<!--head:内容项目-->
<div id="content">

<!--head:左侧内容-->
<div class="left_mian"> 
<!--head:扁平图标导航-->
 <jsp:include page="./navigation.jsp"></jsp:include>
<!--head:扁平图标导航-->

</div>
<!--end:左侧内容-->


<!--head:右侧内容-->
<div class="right_mian">

<div class="widget" id="widget_question">
	<div class="question_unit">
		<div class="question_detail">
			<div class="tag_pro question_detail_div">
		 		<span style="font-size:20px; float:left;line-height:25px; margin-right: 10px; width: 50px;font-weight: bold;">问题分类</span>
		 		<div id="select_category">
					<a rel="tag">粮油</a>
					<a rel="tag">桑蚕</a>
					<a rel="tag">中药材</a>
					<a rel="tag">畜牧业</a>
					<a rel="tag">蔬菜</a>
					<a rel="tag">水果</a>
					<a rel="tag">花卉</a>
					<a rel="tag">水产渔业</a>
					<a rel="tag">茶叶</a>
					<a rel="tag">饲料</a>
					<a rel="tag">花卉</a>
					<a rel="tag">农村能源</a>
					<a rel="tag">农产品销售</a>
					<a rel="tag">农业政策</a>
					<a rel="tag">农业法律</a>
				</div>
			</div>
			<div class="tag_pro question_detail_div">
		 		<span style="font-size:20px; float:left;line-height:25px; margin-right: 10px; width: 50px;font-weight: bold;">指定专家</span>
		 		<div id="select_expert">
					<a rel="tag">鲁兴萌</a>
					<a rel="tag">金佩华</a>
					<a rel="tag">中药材</a>
					<a rel="tag">余荣峰</a>
					<a rel="tag">王立如</a>
					<a rel="tag">许年林</a>
				</div>
			</div>
			<div class="question_detail_div">
				<span style="font-size:20px; float:left;line-height:25px; margin-right: 10px; width: 50px;font-weight: bold;">标题</span>
				<input class="question_title" type="text" name="questiontitle" placeholder="请写下你的问题..." />
			</div>
			<div class="question_detail_div">
				<p>请对问题进行补充或者上传问题中涉及的图片 </p>
				<textarea name="content"  class="xheditor-mini {skin:'nostyle',layerShadow:0,upBtnText:'本地图片',upImgUrl:'upload.asp'}" style="width: 100%; height: 150px;" placeholder="问题说明（可选）..."></textarea>
				<div class="question_toolbar">
					<div class="question_gradient"></div>
					<button class="question_button" type="submit">发布</button>
				</div>
			</div>
		</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".tag_pro a").click(function(){
			$(this).addClass("selecttag");
			$(this).nextAll("a").removeClass("selecttag");
			$(this).prevAll("a").removeClass("selecttag");
		});
	});
</script>
	</div>
</div>

</div>
<!--end:右侧内容-->

</div>  
<!--end:内容项目-->

<!--head:底部信息-->
  <jsp:include page="./footer.jsp"></jsp:include>
<!--end:底部信息-->
<script type="text/javascript" src="${ctx}/jslib/index/thickbox.js"></script>
<script type="text/javascript" src="${ctx}/jslib/index/themepark.js"></script>


</body>
</html>