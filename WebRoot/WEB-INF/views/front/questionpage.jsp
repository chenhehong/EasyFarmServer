<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	 <jsp:include page="../inc.jsp"></jsp:include>
    <base href="<%=basePath%>">
    <jsp:include page="./inc-head.jsp"></jsp:include>
	<link href="${ctx}/style/index/comment.css" rel="stylesheet" media="screen">
</head>
<body class="page page-id-2 page-parent page-template page-template-aboutus page-template-aboutus-php">

 <!--start 登录注册浮动栏 -->
    <jsp:include page="./wpthemedemobar.jsp"></jsp:include>
 <!-- end 登录注册浮动栏 -->

   <!-- start:头部导航 -->
  <jsp:include page="./header.jsp"></jsp:include>
  <!-- end:头部导航-->

<!--head:所在位置-->
<div id="page_muen_nav">  <b>您现在所在的位置：</b><a href="${ctx}/front/web/base/index">首页</a> <a> &gt; </a><a href="${ctx}/front/web/base/getQuestionList"> 在线交流</a></div>
<!--end:所在位置-->

<!--head:内容项目-->
<div id="content">

<!--head:左侧内容-->
<div class="left_mian"> 
<!--head:查询提问-->
<div class="widget">
<ul>
<li>
	<div class="searchbox">
		<input type="text" id="input_search" autocomplete="off" value="" placeholder="搜索你感兴趣的内容...">
		<button id="btn_search"><i class="search_icon"></i></button>
	</div>
</li>
<li>
	<a href="${ctx}/front/web/expert/getAskQuestionPage" target="_blank"><button class="askquestion" id="btn_askquestion">提问</button></a>
</li>
</ul>
</div>
<!--end:查询提问-->

<!--head:扁平图标导航-->
  <jsp:include page="./navigation.jsp"></jsp:include>
<!--head:扁平图标导航-->

</div>
<!--end:左侧内容-->


<!--head:右侧内容-->
<div class="right_mian">

<div class="widget" id="widget_question">
		<a class="question_title" href="./">【养猪】求救，家里的猪最近总是没食欲</a>
		<div class="question_content">
朱元璋御膳菜单：
早膳：羊肉炒、猪肉炒黄菜、蒸猪蹄肚、两熟煎鲜鱼、香米饭、豆汤、泡茶。（话说早晨吃这么多荤的真没问题？）
午膳：胡椒醋鲜虾、烧鹅、燌羊头蹄、鹅肉巴子、咸鼓芥末羊肚盘、蒜醋白血汤、五味蒸鸡、元汁羊骨头、糊辣醋腰子、蒸鲜鱼、羊肉水晶角儿、椒末羊肉、香米饭、蒜酪、三鲜汤、豆汤、泡茶。

万历皇帝御膳菜单：
米面食：八宝馒头、攒馅馒头、蒸卷、海清卷子、蝴蝶卷子；大蒸饼、椒盐饼、夹糖饼、芝麻烧饼、奶皮烧饼、薄脆饼、灵芝饼；枣糕、白馓子、糖馓子、芝麻象眼减煠；鸡蛋面、白切面、水晶饭。
肉食：烧天鹅（天鹅……）、烧鹅、清蒸鸡、暴腌鸡、川炒鸡、烧肉、白煮肉、清蒸肉、猪屑骨、荔枝猪肉、鲟鳇鲊、蒸鱼、猪耳脆、煮鲜肫肝、玉丝肚肺、蒸羊、燌羊。
汤品：牡丹头汤、鸡脆饼汤、猪肉龙松汤、玛瑙糕子汤、锦丝糕子汤、木樨糕子汤、酸甜汤、葡萄汤、蜜汤、牛奶。
【谢评论区 @杨海晨 提醒，煮鲜肫肝并不是猪内脏，我一个天天吃鸭肫的竟然忘了没有猪肫这种说法……】

永乐二年郊祀结束后的庆成宴菜单：
上卓：按酒五般。果子五般。茶食五般。烧煠五般。汤三品。双下馒头。马肉饭。酒五钟。
中卓：按酒四般。果子四般。汤三品。双下馒头。马猪羊肉饭（神一般的荤菜三拼？）。酒五钟。
随驾将军：按酒一般。粉汤。双下馒头猪肉饭。酒一钟。
		
		</div>
		<div class="question_actions">
			<a class="comments_count"><i class="icon_comment"></i><span class="highlight">24</span>条评论</a>
			<a class="like_count"><i class="icon_like"></i><span class="highlight">6</span>个喜欢</a>
			<button class="question_collapse" style="display: none;"><i></i>收起</button>
		</div>
			<!--head:评论区域-->
		 <div class="comment_list">
			<!--head:评论列表-->
			<ul>
			<!--head:一条评论主体-->
			<li class="commentitem">
				<div class="comment_post">
					<div class="avatar">
						<a target="_blank" href="#" title="几尼">
							<img src="./styles/images/noavatar_default.png" alt="几尼">
						</a>
					</div>
					<div class="comment_body">
						<div class="comment_header"><a class="user_name" href="#" target="_blank">几尼</a></div>
						<!--head:评论内容-->
						<p>为什么没有图片的文章，主页有乱码？</p>
						<!--end:评论内容-->
						<div class="comment_footer">
							<span class="comment_time" title="2016年7月10日 19:08:52">7月10日</span>
							<a class="comment_reply" href="javascript:void(0);"><span class="comment_icon comment_icon_reply"></span>回复</a>
							<a class="comment_likes" href="javascript:void(0);"><span class="comment_icon comment_icon_like"></span>顶</a>
							<span class="comment_likes_count">5赞</span>
						</div>
					</div>
				</div>
			</li>
			<!--end:一条评论主体-->
			<!--head:一条评论主体-->
			<li class="commentitem">
				<div class="comment_post">
					<div class="avatar">
						<a target="_blank" href="#" title="几尼">
							<img src="./styles/images/noavatar_default.png" alt="几尼">
						</a>
					</div>
					<div class="comment_body">
						<div class="comment_header"><a class="user_name" href="#" target="_blank">几尼</a></div>
						<!--head:评论内容-->
						<p>楼主你好，非常赞同你的观点。</p>
						<!--end:评论内容-->
						<div class="comment_footer">
							<span class="comment_time" title="2016年7月10日 19:08:52">7月10日</span>
							<a class="comment_reply" href="javascript:void(0);"><span class="comment_icon comment_icon_reply"></span>回复</a>
							<a class="comment_likes" href="javascript:void(0);"><span class="comment_icon comment_icon_like"></span>顶</a>
							<span class="comment_likes_count">5赞</span>
						</div>
					</div>
				</div>
			</li>
			<!--end:一条评论主体-->
			</ul>
			<!--end:评论列表-->
			<!--head:评论分页-->
			<div class="comment_paginator">
					<a data-page="1" href="javascript:void(0);" class="paginator_current">1</a> 
					<a data-page="2" href="javascript:void(0);">2</a>
			</div>
			<!--end:评论分页-->
			<div class="comment_replybox">
				<a class="replybox_avatar" href="javascript:void(0);" onclick="return false"><img src="./styles/images/noavatar_default.png" alt=""></a>
				<div class="replybox_textarea"><textarea name="message" placeholder="写下你的评论…"></textarea></div>
				<div class="replybox_toolbar">
					<div class="replybox_gradient"></div>
					<button class="replybox_button" type="submit">评论</button>
				</div>
			</div>
		</div>
		<!--end:评论区域-->
<script>
//点击已有评论的回复
$(".comment_reply").click(function(){
	var littlebox = $(this).parent(".comment_footer").next(".comment_replylittlebox");
	if($(littlebox).length == 0){
		var replyboxhtml = '<div class="comment_replylittlebox"><div class="replylittlebox_textarea"><textarea name="message" placeholder="写下你的评论…"></textarea></div><div class="replylittlebox_toolbar"><div class="replylittlebox_gradient"></div><button class="replylittlebox_button" type="submit">评论</button></div></div>';
		$(this).parents(".comment_body").append(replyboxhtml);
	}
	else{
		$(littlebox).show();
	}
});
</script>
</div>

</div>
<!--end:右侧内容-->

</div>  
<!--end:内容项目-->


<!--head:底部信息-->
  <jsp:include page="./footer.jsp"></jsp:include>
<!--end:底部信息-->

<script type="text/javascript" src="${ctx}/jslib/index/thickbox.js"></script>
<script type="text/javascript" src="${ctx}/jslib/index/themepark.js"></script>
</body>
</html>