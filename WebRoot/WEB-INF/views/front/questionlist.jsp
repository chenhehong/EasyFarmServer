<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	 <jsp:include page="../inc.jsp"></jsp:include>
    <base href="<%=basePath%>">
    <jsp:include page="./inc-head.jsp"></jsp:include>
	<link href="${ctx}/style/index/comment.css" rel="stylesheet" media="screen">
</head>
<body class="page page-id-2 page-parent page-template page-template-aboutus page-template-aboutus-php">

 <!--start 登录注册浮动栏 -->
    <jsp:include page="./wpthemedemobar.jsp"></jsp:include>
 <!-- end 登录注册浮动栏 -->

   <!-- start:头部导航 -->
  <jsp:include page="./header.jsp"></jsp:include>
  <!-- end:头部导航-->

<!--head:所在位置-->
<div id="page_muen_nav">  <b>您现在所在的位置：</b><a href="${ctx}/front/web/base/index">首页</a> <a> &gt; </a><a href="${ctx}/front/web/base/getQuestionList"> 在线交流</a></div>
<!--end:所在位置-->

<!--head:内容项目-->
<div id="content">

<!--head:左侧内容-->
<div class="left_mian"> 
<!--head:查询提问-->
<div class="widget">
<ul>
<li>
	<div class="searchbox">
		<input type="text" id="input_search" autocomplete="off" value="" placeholder="搜索你感兴趣的内容...">
		<button id="btn_search"><i class="search_icon"></i></button>
	</div>
</li>
<li>
	<a href="${ctx}/front/web/expert/getAskQuestionPage" target="_blank"><button class="askquestion" id="btn_askquestion">提问</button></a>
</li>
</ul>
</div>
<!--end:查询提问-->

<!--head:扁平图标导航-->
  <jsp:include page="./navigation.jsp"></jsp:include>
<!--head:扁平图标导航-->

</div>
<!--end:左侧内容-->


<!--head:右侧内容-->
<div class="right_mian">

<div class="widget" id="widget_question">
<ul>
	<!--head:一条问题-->
	<li>
		<a class="question_title" href="${ctx}/front/web/base/getAskQuestionPage">【养猪】求救，家里的猪最近总是没食欲</a>
		<div class="question_expandable">
			<img src="${ctx}/uploadfile/p1.png">
			朱元璋御膳菜单:早膳:羊肉炒、猪肉炒黄菜、蒸猪蹄肚、两熟煎鲜鱼、香米饭、豆汤、泡茶.(话说早晨吃这么多荤的真没问题?)午膳:胡椒醋鲜虾、烧鹅、燌羊头蹄、鹅肉巴子、咸鼓芥末羊肚盘、蒜醋白血汤、五味蒸鸡、元汁羊骨头、糊辣醋腰子、蒸鲜鱼、羊肉水晶角儿…
			<a class="toggle-expand">显示全部</a>
		</div>
		<div class="question_content" style="display: none;">
朱元璋御膳菜单：
早膳：羊肉炒、猪肉炒黄菜、蒸猪蹄肚、两熟煎鲜鱼、香米饭、豆汤、泡茶。（话说早晨吃这么多荤的真没问题？）
午膳：胡椒醋鲜虾、烧鹅、燌羊头蹄、鹅肉巴子、咸鼓芥末羊肚盘、蒜醋白血汤、五味蒸鸡、元汁羊骨头、糊辣醋腰子、蒸鲜鱼、羊肉水晶角儿、椒末羊肉、香米饭、蒜酪、三鲜汤、豆汤、泡茶。

万历皇帝御膳菜单：
米面食：八宝馒头、攒馅馒头、蒸卷、海清卷子、蝴蝶卷子；大蒸饼、椒盐饼、夹糖饼、芝麻烧饼、奶皮烧饼、薄脆饼、灵芝饼；枣糕、白馓子、糖馓子、芝麻象眼减煠；鸡蛋面、白切面、水晶饭。
肉食：烧天鹅（天鹅……）、烧鹅、清蒸鸡、暴腌鸡、川炒鸡、烧肉、白煮肉、清蒸肉、猪屑骨、荔枝猪肉、鲟鳇鲊、蒸鱼、猪耳脆、煮鲜肫肝、玉丝肚肺、蒸羊、燌羊。
汤品：牡丹头汤、鸡脆饼汤、猪肉龙松汤、玛瑙糕子汤、锦丝糕子汤、木樨糕子汤、酸甜汤、葡萄汤、蜜汤、牛奶。
【谢评论区 @杨海晨 提醒，煮鲜肫肝并不是猪内脏，我一个天天吃鸭肫的竟然忘了没有猪肫这种说法……】

永乐二年郊祀结束后的庆成宴菜单：
上卓：按酒五般。果子五般。茶食五般。烧煠五般。汤三品。双下馒头。马肉饭。酒五钟。
中卓：按酒四般。果子四般。汤三品。双下馒头。马猪羊肉饭（神一般的荤菜三拼？）。酒五钟。
随驾将军：按酒一般。粉汤。双下馒头猪肉饭。酒一钟。
		
		</div>
		<div class="question_actions">
			<a class="comments_count"><i class="icon_comment"></i><span class="highlight">24</span>条评论</a>
			<a class="like_count"><i class="icon_like"></i><span class="highlight">6</span>个喜欢</a>
			<button class="question_collapse" style="display: none;"><i></i>收起</button>
		</div>
			<!--head:评论区域-->
		 <div class="comment_list" style="display: none;" >
			<!--head:评论列表-->
			<ul>
			<!--head:一条评论主体-->
			<li class="commentitem">
				<div class="comment_post">
					<div class="avatar">
						<a target="_blank" href="#" title="几尼">
							<img src="${ctx}/style/images/noavatar_default.png" alt="几尼">
						</a>
					</div>
					<div class="comment_body">
						<div class="comment_header"><a class="user_name" href="#" target="_blank">几尼</a></div>
						<!--head:评论内容-->
						<p>为什么没有图片的文章，主页有乱码？</p>
						<!--end:评论内容-->
						<div class="comment_footer">
							<span class="comment_time" title="2016年7月10日 19:08:52">7月10日</span>
							<a class="comment_reply" href="javascript:void(0);"><span class="comment_icon comment_icon_reply"></span>回复</a>
							<a class="comment_likes" href="javascript:void(0);"><span class="comment_icon comment_icon_like"></span>顶</a>
							<span class="comment_likes_count">5赞</span>
						</div>
					</div>
				</div>
			</li>
			<!--end:一条评论主体-->
			<!--head:一条评论主体-->
			<li class="commentitem">
				<div class="comment_post">
					<div class="avatar">
						<a target="_blank" href="#" title="几尼">
							<img src="${ctx}/style/images/noavatar_default.png" alt="几尼">
						</a>
					</div>
					<div class="comment_body">
						<div class="comment_header"><a class="user_name" href="#" target="_blank">几尼</a></div>
						<!--head:评论内容-->
						<p>楼主你好，非常赞同你的观点。</p>
						<!--end:评论内容-->
						<div class="comment_footer">
							<span class="comment_time" title="2016年7月10日 19:08:52">7月10日</span>
							<a class="comment_reply" href="javascript:void(0);"><span class="comment_icon comment_icon_reply"></span>回复</a>
							<a class="comment_likes" href="javascript:void(0);"><span class="comment_icon comment_icon_like"></span>顶</a>
							<span class="comment_likes_count">5赞</span>
						</div>
					</div>
				</div>
			</li>
			<!--end:一条评论主体-->
			</ul>
			<!--end:评论列表-->
			<!--head:评论分页-->
			<div class="comment_paginator">
					<a data-page="1" href="javascript:void(0);" class="paginator_current">1</a> 
					<a data-page="2" href="javascript:void(0);">2</a>
			</div>
			<!--end:评论分页-->
		</div>
		<!--end:评论区域-->
	</li>
	<!--end:一条问题-->
	<!--head:一条问题-->
	<li>
		<a class="question_title" href="${ctx}/front/web/base/getAskQuestionPage">【种菜】怎么种番茄</a>
		<div class="question_expandable">
			<img src="${ctx}/uploadfile/p2.png">
			开车久了，其实每个人都有了一些不好的习惯，我虽然说开车算稳，但是在加挡的时候还是会冲一下，这冲一下虽小，驾驶员自己一般感受不到，但是看到副驾驶的人身体会轻微的动一下，后来研究了一下，其实不冲很简单...
			<a class="toggle-expand">显示全部</a>
		</div>
		<div class="question_content" style="display: none;">
开车久了，其实每个人都有了一些不好的习惯，我虽然说开车算稳，但是在加挡的时候还是会冲一下，这冲一下虽小，驾驶员自己一般感受不到，但是看到副驾驶的人身体会轻微的动一下，后来研究了一下，其实不冲很简单。
开车久了为了加速快，我们都是踩一脚油门后在高转速的时候加档，这样能获得较快的加速，如果想要开的平稳，很简单，从1档开始，加油之后不要在最高转速的时候摘挡挂档，脚离开油门后，等2到3秒转速自己降下来后再摘空挡加档，用这种开法的话基本上冲劲就完全没了，但是缺点就是加速比较慢，可能后面的车会哔哔。。。。。。这方法适合追求极致的朋友驾驶。。。。
——————————————更新分割线————————————————
刚拿驾照就上高速很危险！！！

　　一脚油门踩到底的驾驶技巧我 8 岁时开碰碰车的时候就学会了 _(:3 」∠)_ _(:3 」∠)_ _(:3 」∠)_

　　鉴于之前的答案太乱了，整理一下方便阅读。以下均为个人经验，有不对之处请指正。

　　上长途高速驾驶之前建议在环城高速上有一定的驾驶经验，比如北京的五环路，不堵车的情况下能开到 100，而且上面车多，能练习好超车跟车经验，这样不会到了跨省市高速上看到旁边的车 120 甚至 150 开过去的时候自己心慌。

　　一个司机想要安全驾驶，有两点很重要，一个是预判，另一个是让。

		</div>
		<div class="question_actions">
			<a class="comments_count"><i class="icon_comment"></i><span class="highlight">19</span>条评论</a>
			<a class="like_count"><i class="icon_like"></i><span class="highlight">10</span>个喜欢</a>
			<button class="question_collapse" style="display: none;" onclick="fnCollapseQuestion()"><i></i>收起</button>
		</div>
		<!--head:评论区域-->
		<div class="comment_list" style="display: none;" >
			<!--head:评论列表-->
			<ul>
			</ul>
			<!--end:评论列表-->
			<!--head:评论分页-->
			<div class="comment_paginator">
					<a data-page="1" href="javascript:void(0);" class="paginator_current">1</a> 
			</div>
			<!--end:评论分页-->
		</div>
		<!--end:评论区域-->
	</li>
	<!--end:一条问题-->
	<!--head:一条问题-->
	<li>
		<a class="question_title" href="${ctx}/front/web/base/getAskQuestionPage">【种菜】花生喷药技巧</a>
		<div class="question_expandable">
			
			上周，据美国科技网站 VentureBeat 报道，苹果公司准备招聘4位体育相关的 Siri 软件工程师。此举是为了解决各种球赛后，Siri 听到大量的 「F*ck, SH*t, Bitch」而无法还嘴的窘境（这句是吐槽）。总之一句话，苹果公司要在体育语音搜索中发力了。当然你不要...
			<a class="toggle-expand">显示全部</a>
		</div>
		<div class="question_content" style="display: none;">
			上周，据美国科技网站 VentureBeat 报道，苹果公司准备招聘4位体育相关的 Siri 软件工程师。此举是为了解决各种球赛后，Siri 听到大量的 「F*ck, SH*t, Bitch」而无法还嘴的窘境（这句是吐槽）。总之一句话，苹果公司要在体育语音搜索中发力了。

当然你不要理解错，语音发力不是弄个篮球/足球解说员搞个语音包。

「霍霍，你车开得挺合理呀！」
「你这是邦邦邦乱开。」
「前方50米有一收费站，请勿力劈华山。」
「嘿嘿嘿。」
		</div>
		<div class="question_actions">
			<a class="comments_count"><i class="icon_comment"></i><span class="highlight">22</span>条评论</a>
			<a class="like_count"><i class="icon_like"></i><span class="highlight">5</span>个喜欢</a>
			<button class="question_collapse" style="display: none;" onclick="fnCollapseQuestion()"><i></i>收起</button>
		</div>
		<!--head:评论区域-->
		<div class="comment_list" style="display: none;" >
			<!--head:评论列表-->
			<ul>
			</ul>
			<!--end:评论列表-->
			<!--head:评论分页-->
			<div class="comment_paginator">
					<a data-page="1" href="javascript:void(0);" class="paginator_current">1</a> 
			</div>
			<!--end:评论分页-->
		</div>
		<!--end:评论区域-->
	</li>
	<!--end:一条问题-->
	<!--head:一条问题-->
	<li>
		<a class="question_title" href="${ctx}/front/web/base/getAskQuestionPage">【养鱼】草鱼育苗</a>
		<div class="question_expandable">
			<img src="${ctx}/uploadfile/p3.png">
			这个问题在《国家新型城镇化规划纲要》中就已经回答了。推行农业现代化的条件简单来看，一是土地自然条件，二是技术资金支持。这看起来很容易实现，但却还有一个问题，我国的农业人口过多，真正的农村人均耕地很少，远达不到推行集体规模经营的条件，平均每…
			<a class="toggle-expand">显示全部</a>
		</div>
		<div class="question_content" style="display: none;">
这个问题在《国家新型城镇化规划纲要》中就已经回答了。
推行农业现代化的条件简单来看，一是土地自然条件，二是技术资金支持。
这看起来很容易实现，但却还有一个问题，我国的农业人口过多，真正的农村人均耕地很少，远达不到推行集体规模经营的条件，平均每人只有这么点地，如果理想化地现在推行规模化和机械化，那就根本不需要这么多农村劳动力，这些农业人口去哪里讨生活呢？
为什么要推行城镇化，尤其是"新型城镇化"？
新型城镇化的内涵是"人"的城镇化，而不是地的城镇化，让更多的农业人口转变生产方式，从第一产业到二三产业中去，这样不仅增加了收入，而且释放了农村走机械化规模化的潜力。
有兴趣你可以读读这个规划的文本，写的还是很有水平的。
		</div>
		<div class="question_actions">
			<a class="comments_count"><i class="icon_comment"></i><span class="highlight">33</span>条评论</a>
			<a class="like_count"><i class="icon_like"></i><span class="highlight">25</span>个喜欢</a>
			<button class="question_collapse" style="display: none;" onclick="fnCollapseQuestion()"><i></i>收起</button>
		</div>
		<!--head:评论区域-->
		<div class="comment_list" style="display: none;" >
			<!--head:评论列表-->
			<ul>
			</ul>
			<!--end:评论列表-->
			<!--head:评论分页-->
			<div class="comment_paginator">
					<a data-page="1" href="javascript:void(0);" class="paginator_current">1</a> 
			</div>
			<!--end:评论分页-->
		</div>
		<!--end:评论区域-->
	</li>
	<!--end:一条问题-->
	<!--head:一条问题-->
	<li>
		<a class="question_title" href="${ctx}/front/web/base/getAskQuestionPage">【种菜】花生喷药技巧</a>
		<div class="question_expandable">
			<img src="${ctx}/uploadfile/p4.png">
			这年底多出来的一秒对于我们人类来说几乎就是个概念，但是对于认死理的计算机来说有可能就是灾难。2012年6月底也多出了一秒，这多出来的一秒直接把Reddit和一些其他网站搞挂了。这是为什么呢？因为Unix time（POSIX time）是不支持leap second（即闰秒）的…
			<a class="toggle-expand">显示全部</a>
		</div>
		<div class="question_content" style="display: none;">
2012年6月底也多出了一秒，这多出来的一秒直接把Reddit和一些其他网站搞挂了。

这是为什么呢？

因为Unix time（POSIX time）是不支持leap second（即闰秒）的。我们都知道在C里的time()函数会返回一个秒数，一般是从1970年1月1号0点到现在为止的秒数。然而这几十年来出现了大概20多个leap second，POSIX是怎么处理的呢？方法就是——无视他们。POSIX规定，每天是严格的86400秒，一秒都不多。

为什么要这么规定？好算呗！

我们都知道，C里还有一个函数叫做gmtime()，可以把秒数（time_t）还原成哪年哪天几点几分几秒。如果把leap second无视掉，这个函数就很好做，可是如果考虑到leap second，这个函数就直接崩盘了。首先你需要逐一考虑之前没有明显规律的leap second，更可怕的是，之后不知道什么时候出现的leap second你怎么处理？这给编译器带来了一个比较严峻的考验，于是乎POSIX大笔一挥，我们不管啦！
		</div>
		<div class="question_actions">
			<a class="comments_count"><i class="icon_comment"></i><span class="highlight">33</span>条评论</a>
			<a class="like_count"><i class="icon_like"></i><span class="highlight">25</span>个喜欢</a>
			<button class="question_collapse" style="display: none;" onclick="fnCollapseQuestion()"><i></i>收起</button>
		</div>
		<!--head:评论区域-->
		<div class="comment_list" style="display: none;" >
			<!--head:评论列表-->
			<ul>
			</ul>
			<!--end:评论列表-->
			<!--head:评论分页-->
			<div class="comment_paginator">
					<a data-page="1" href="javascript:void(0);" class="paginator_current">1</a> 
			</div>
			<!--end:评论分页-->
		</div>
		<!--end:评论区域-->
	</li>
	<!--end:一条问题-->
</ul>
<!--head:列表分页-->
<div class="list_paginator">
	<a data-page="1" href="javascript:void(0);" class="paginator_current">1</a> 
	<a data-page="2" href="javascript:void(0);">2</a>
</div>
<!--end:列表分页-->
<script>
//展开问题
$(".question_expandable").click(function(){
	$(this).hide();
	$(this).nextAll(".question_content").show();
	$(this).nextAll(".question_actions").find(".question_collapse").show();
	$(this).nextAll(".comment_list").show();
	
	//创建评论区
	var replybox = $(this).nextAll(".comment_list").find(".comment_replybox");
	if($(replybox).length == 0){
		var replyboxhtml ='<div class="comment_replybox"><a class="replybox_avatar" href="javascript:void(0);" onclick="return false"><img src="${ctx}/style/images/noavatar_default.png" alt=""></a><div class="replybox_textarea"><textarea name="message" placeholder="写下你的评论…"></textarea></div><div class="replybox_toolbar"><div class="replybox_gradient"></div><button class="replybox_button" type="submit">评论</button></div></div>';
		$(this).nextAll(".comment_list").append(replyboxhtml);
	}
});

//折叠问题
$(".question_collapse").click(function(){
	$(this).hide();
	$(this).parent(".question_actions").prevAll(".question_content").hide();
	$(this).parent(".question_actions").nextAll(".comment_list").hide();
	$(this).parent(".question_actions").prevAll(".question_expandable").show();
});

//点击已有评论的回复
$(".comment_reply").click(function(){
	var littlebox = $(this).parent(".comment_footer").next(".comment_replylittlebox");
	if($(littlebox).length == 0){
		var replyboxhtml = '<div class="comment_replylittlebox"><div class="replylittlebox_textarea"><textarea name="message" placeholder="写下你的评论…"></textarea></div><div class="replylittlebox_toolbar"><div class="replylittlebox_gradient"></div><button class="replylittlebox_button" type="submit">评论</button></div></div>';
		$(this).parents(".comment_body").append(replyboxhtml);
	}
	else{
		$(littlebox).show();
	}
});
</script>
</div>

</div>
<!--end:右侧内容-->

</div>  
<!--end:内容项目-->


</div>  
<!--end:内容项目-->

<!--head:底部信息-->
  <jsp:include page="./footer.jsp"></jsp:include>
<!--end:底部信息-->

<script type="text/javascript" src="${ctx}/jslib/index/thickbox.js"></script>
<script type="text/javascript" src="${ctx}/jslib/index/themepark.js"></script>
</body>
</html>