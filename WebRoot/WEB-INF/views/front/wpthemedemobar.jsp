<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!--start: not login-->
<c:if test="${sessionInfoExpert==null}">
<div id="wpthemedemobar" style="position:fixed!important;_position:absolute; _top:expression(eval(document.documentElement.scrollTop+0)); top:0px; z-index:99999;">
<div class="wpthemedemobar_wrapper">
<div class="wptdb_right">
<a id="btnlogin" href="${ctx}/front/web/base/getLoginPage">立即登录</a>
<a id="btnregister" href="${ctx}/front/web/base/getRegister">注册用户</a>
</div>
<div class="wptdb_current">当前登录用户：<span class="wptdb_themename">未登录</span></div></div>
</div>
</c:if>
<!--end: not login-->
<!--start: have login-->

<c:if test="${sessionInfoExpert!=null}">
<div id="wpthemedemobar" style="position:fixed!important;_position:absolute; _top:expression(eval(document.documentElement.scrollTop+0)); top:0px; z-index:99999;">
<div class="wpthemedemobar_wrapper">
<div class="wptdb_right">
<a id="btnlogout" href="${ctx}/front/web/expert/logout">退出</a>
</div>
<div class="wptdb_current">当前登录用户：<span class="wptdb_themename">专家：${sessionInfoExpert.name}</span></div></div>
</div>
</c:if>
<!--end: not login-->
