﻿var webservicepath = 'RegisterForm.aspx';

$(document).ready(function () {
    //密码控件
    //$("input:pwd").chromaHash({ bars: 3 });
    //省市控件     
    $("#provincecity").ProvinceCity("label");
    //detectAddress();
    $("#provincecity label").css({ "margin-top": "-3px" });
    initcheck();
    $("#logincode").focus();
});

function detectAddress() {
    $.ajax
	({
	    type: "POST",
	    async: true,
	    cache: false,
	    dataType: 'json',
	    contentType: 'application/json; charset=utf-8',
	    url: "/account/detectAddress",
	    success: function (data) {
	        if (data.status) {
	            $("select#province option:contains('" + data.province + "')").attr('selected', true);
	            $('#province').trigger('change');
	            $("select#city option:contains('" + data.city + "')").attr('selected', true);
	        }
	    }
	});
}

//完成注册，上传数据
function register() {
    if (check()) {
        var addressStr = $("#province")[0].value + "省" + $("#city")[0].value + "市" + $("#detailaddr").val();
        var data = { 
            LoginCode: $('#logincode').val(), 
            Password: $('#pwd').val(), 
            Email: $('#email').val(), 
            ShopAddress: addressStr,
            ContactPhone: $('#contactphone').val(),
            CheckCode: $('#validvalue').val(),
        }; 

        $("#btn_register").attr("disabled", "disabled");

        //提交数据给registerhandle.ashx页面处理 
        $.post("api/authority/registerhandle.ashx",data,function(result){ 
            if(result == "1"){
                alert("注册成功");
                //SendWelcomeEmailAndMsg();
                window.location.href='LoginForm.aspx?LoginCode=' + $('#logincode').val();
            }
            else if(result == "-1" || result == "-2"){
                alert("验证码错误");
                f_refreshtype();
                $('#validvalue').val('');
                $("#validvalue").focus();
                $("#btn_register").removeAttr("disabled");
            }
            else if(result == "-3"){
                alert("账号名格式不正确");
                f_refreshtype();
                $('#validvalue').val('');
                $("#logincode").focus();
                $("#btn_register").removeAttr("disabled");
            }
            else if(result == "-4"){
                alert("账号名已存在，请重新输入");
                f_refreshtype();
                $('#validvalue').val('');
                $("#logincode").focus();
                $("#btn_register").removeAttr("disabled");
            }
            else{
                alert("注册失败");
                f_refreshtype();
                $('#validvalue').val('');
                $("#btn_register").removeAttr("disabled");
            }
        });
    }
}

//发送注册成功邮件和信息
function SendWelcomeEmailAndMsg() {
    $.ajax
	({
	    beforeSend: function () {},
	    type: "POST",
	    async: true,
	    dataType: "json",
	    url: "/Account/SendWelcomeEmailAndMsg",
	    data: {},
	    error: function (XmlHttpRequest, textStatus, errorThrown) {},
	    success: function (msg) {},
	    complete: function () {
	    }
	});
}

function checkEmail() {
    var txt = $('#email').val();
    if (txt.length == 0) {
        $('#speemail').html('邮箱不能为空');
        $('#speemail').show();
        return false;
    }
    else {
        if (chkemail(txt) == false) {
            $('#speemail').html('邮箱格式不正确，请重新输入');
            $('#speemail').show();
            return false;
        }
        else {
            $('#speemail').hide();
            return true;
        }
    }
}

function chkemail(stremail) {
    if (!/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(stremail)) {
        return false;
    }
    else {
        return true;
    }
}

function checklogincodevalue() {
    var txt = $("#logincode").val();
    if (txt.length == 0) {
        $('#spelogincode').html('账号名不能为空');
        $('#spelogincode').show();
        return false;
    }
    if (txt.length < 5) {
        $('#spelogincode').html('账号名长度不能少于5位');
        $('#spelogincode').show();
        return false;
    }
    if (txt.length > 30) {
        $('#spelogincode').html('账号名长度不能超过30位');
        $('#spelogincode').show();
        return false;
    }
    if (chklogincode(txt) == false) {
        $('#spelogincode').html('账号名格式不符');
        $('#spelogincode').show();
        return false;
    }
    else {
        $('#spelogincode').hide();
        //异步调用，检验账号是否已存在
        $.ajax({
            type: "POST",
            contentType: "application/json",
            url: webservicepath + "/CheckLoginCode",
            data: "{'logincode':'" + txt + "'}",
            dataType: "json",
            success: function (json) {
                var data = JSON.parse(json.d);
                if (data == true) {
                    $('#spelogincode').html('账号名已存在');
                    $('#spelogincode').show();
                }
                else {
                    $('#spelogincode').hide();
                }
            },
            error: function (xhr, error, thrown) {

            }
        });
        return true;
    }
}

function chklogincode(strlogincode) {
    if (!/^[a-zA-Z]{1}[a-zA-Z0-9]{4,29}$/.test(strlogincode))
        return false;
    else
        return true;
}

function checkPassword() {
    var txt = $("#pwd").val();
    if (txt.length == 0) {
        $('#spepwd').html('密码不能为空');
        $('#spepwd').show();
        return false;
    } else if (txt.length < 6) {
        $('#spepwd').html('密码长度不能小于6位');
        $('#spepwd').show();
        return false;
    } else if (txt.length > 20) {
        $('#spepwd').html('密码长度不能超过20位');
        $('#spepwd').show();
        return false;
    }
    else {
        $('#spepwd').hide();
        return true;
    }
}

function checkConfirmpassword() {
    var txt = $("#pwdconfirm").val();
    if (txt.length == 0) {
        $('#spepwdconfirm').html('确认密码不能为空');
        $('#spepwdconfirm').show();
        return false;
    } else if (txt != $("#pwd").val()) {
        $('#spepwdconfirm').html('两次输入的密码不一致！');
        $('#spepwdconfirm').show();
        return false;
    }
    else {
        $('#spepwdconfirm').hide();
        return true;
    }
}

function checkcontactphone() {
    var txt = $("#contactphone").val();
    if (txt.length == 0) {
        $('#specontactphone').html('电话号码不能为空');
        $('#specontactphone').show();
        return false;
    }
    else {
        var reg = /((\d{11})|^((\d{7,8})|(\d{4}|\d{3})-(\d{7,8})|(\d{4}|\d{3})-(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1})|(\d{7,8})-(\d{4}|\d{3}|\d{2}|\d{1}))$)/;
        if (!reg.test(txt)) {
            $('#specontactphone').html('输入的号码不正确');
            $('#specontactphone').show();
            return false;
        } else {
            $('#specontactphone').hide();
            return true;
        }
    }
}

//点击切换验证码
function f_refreshtype() {
    if ($("#validImg").length > 0) {
        var imgsrc = $("#validImg").attr("src");
        $("#validImg").attr("src", "api/authority/createvalidateimage.ashx?" + Math.random());
    }
}

function checkvalidvalue() {
    var validvalue = $("#validvalue").val();
    if (validvalue != "") {
        var data = {};
        $.post(webservicepath + "?ValidateCode=" + validvalue, data, function (result) {
            if (result == "1") {
                $('#spevalidvalue').hide();
            }
            else {
                $('#spevalidvalue').text("填写的验证码错误");
                $('#spevalidvalue').show();
            }
        });
        return true;
    }
    else {
        $('#spevalidvalue').text("验证码不能为空");
        $('#spevalidvalue').show();
        return false;
    }
}

function checkAddress() {
    if ($("#province").val() == "请选择省份") {
        $('#spedetailaddr').html('请选择具体省份');
        $('#spedetailaddr').show();
        return false;
    }
    else if ($("#detailaddr").val().length == 0) {
        $('#spedetailaddr').html('请输入详细地址');
        $('#spedetailaddr').show();
        return false;
    }
    else {
        $('#spedetailaddr').hide();
        return true;
    }
}

function initcheck() {
    //电子邮件检验
    $('#email').blur(function () {
        checkEmail();
    })
    //用户名检验
    $('#logincode').blur(function () {
        checklogincodevalue();
    })
    //密码检验
    $('#pwd').blur(function () {
        checkPassword();
    })
    //确认密码检验
    $('#pwdconfirm').blur(function () {
        checkConfirmpassword();
    })
    //电话检验
    $('#contactphone').blur(function () {
        checkcontactphone();
    })   
    //地址检验
    $('#detailaddr').blur(function () {
        checkAddress();
    })
    //注册码检验
    $('#validvalue').blur(function () {
        checkvalidvalue();
    })
}

function check() {
    var emailCheck = checkEmail();
    var logincodeCheck = checklogincodevalue();
    var passwordCheck = checkPassword();
    var confirmpasswordCheck = checkConfirmpassword();
    var contactphoneCheck = checkcontactphone();
    var addressCheck = checkAddress();
    var validvalueCheck = checkvalidvalue();

    if (emailCheck == false || logincodeCheck == false || passwordCheck == false
        || confirmpasswordCheck == false || contactphoneCheck == false
        || addressCheck == false || validvalueCheck == false)
        return false;
    else
        return true;
}
