﻿var webservicepath = 'ForgetPasswordForm.aspx';

$(document).ready(function () {
    initcheck();
    $("#logincode").focus();
});

//完成注册，上传数据
function finish() {
    if (check()) {
        var data = { 
            LoginCode: $('#logincode').val(), 
            Email: $('#email').val(), 
            CheckCode: $('#validvalue').val(),
        }; 

        $("#btn_finish").attr("disabled", "disabled");

        //提交数据给ForgetPasswordHandle.ashx页面处理 
        $.post("api/authority/forgetpasswordhandle.ashx",data,function(result){ 
            if(result == "1"){
                alert("申请成功，请查阅邮件进行密码重设");
                window.location.href='LoginForm.aspx';
            }
            else if(result == "-1" || result == "-2"){
                alert("验证码错误");
                f_refreshtype();
                $('#validvalue').val('');
                $("#validvalue").focus();
                $("#btn_finish").removeAttr("disabled");
            }
            else{
                alert("账号名与邮箱不匹配");
                f_refreshtype();
                $('#validvalue').val('');
                $("#logincode").focus();
                $("#btn_finish").removeAttr("disabled");
            }
        });
    }
}

function checkEmail() {
    var txt = $('#email').val();
    if (txt.length == 0) {
        $('#speemail').html('邮箱不能为空');
        $('#speemail').show();
        return false;
    }
    else {
        if (chkemail(txt) == false) {
            $('#speemail').html('邮箱格式不正确，请重新输入');
            $('#speemail').show();
            return false;
        }
        else {
            $('#speemail').hide();
            return true;
        }
    }
}

function chkemail(stremail) {
    if (!/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(stremail)) {
        return false;
    }
    else {
        return true;
    }
}

function checklogincodevalue() {
    var txt = $("#logincode").val();
    if (txt.length == 0) {
        $('#spelogincode').html('账号名不能为空');
        $('#spelogincode').show();
        return false;
    }
    if (txt.length < 5) {
        $('#spelogincode').html('账号名长度不能少于5位');
        $('#spelogincode').show();
        return false;
    }
    if (txt.length > 30) {
        $('#spelogincode').html('账号名长度不能超过30位');
        $('#spelogincode').show();
        return false;
    }
    if (chklogincode(txt) == false) {
        $('#spelogincode').html('账号名格式不符');
        $('#spelogincode').show();
        return false;
    }
    else {
        $('#spelogincode').hide();
        return true;
    }
}

function chklogincode(strlogincode) {
    if (!/^[a-zA-Z]{1}[a-zA-Z0-9]{4,29}$/.test(strlogincode))
        return false;
    else
        return true;
}

//点击切换验证码
function f_refreshtype() {
    if ($("#validImg").length > 0) {
        var imgsrc = $("#validImg").attr("src");
        $("#validImg").attr("src", "api/authority/createvalidateimage.ashx?" + Math.random());
    }
}

function checkvalidvalue() {
    var validvalue = $("#validvalue").val();
    if (validvalue != "") {
        var data = {};
        $.post(webservicepath + "?ValidateCode=" + validvalue, data, function (result) {
            if (result == "1") {
                $('#spevalidvalue').hide();
            }
            else {
                $('#spevalidvalue').text("填写的验证码错误");
                $('#spevalidvalue').show();
            }
        });
        return true;
    }
    else {
        $('#spevalidvalue').text("验证码不能为空");
        $('#spevalidvalue').show();
        return false;
    }
}

function initcheck() {
    //电子邮件检验
    $('#email').blur(function () {
        checkEmail();
    })
    //用户名检验
    $('#logincode').blur(function () {
        checklogincodevalue();
    })
    //注册码检验
    $('#validvalue').blur(function () {
        checkvalidvalue();
    })
}

function check() {
    var emailCheck = checkEmail();
    var logincodeCheck = checklogincodevalue();
    var validvalueCheck = checkvalidvalue();

    if (emailCheck == false || logincodeCheck == false || validvalueCheck == false)
        return false;
    else
        return true;
}
